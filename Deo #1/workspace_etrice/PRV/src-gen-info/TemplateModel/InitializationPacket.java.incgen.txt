package TemplateModel;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;
import java.io.Serializable;




public class InitializationPacket implements Serializable {

	private static final long serialVersionUID = 812509847L;


	/*--------------------- attributes ---------------------*/
	public  long systemStartTime;
	public  int controllerT;
	public  int sensorID;
	public  float sensorPError;
	public  int sensorTimeToMeasure;
	public  float sensorThreshold;
	public  float sensorLowerBound;
	public  float sensorUpperBound;
	public  String sensorName;
	public  float pumpErrorRate;
	public  int pumpWaitTime;
	public  int activationsBeforePump;
	public  long maxTimeToAlarmAfterCH4;
	public  long maxTimeToClosePumpAfterCH4;
	public  float COThreshold;
	public  float CH4Threshold;
	public  float airThreshold;

	/* --------------------- attribute setters and getters */
	public void setSystemStartTime(long systemStartTime) {
		 this.systemStartTime = systemStartTime;
	}
	public long getSystemStartTime() {
		return this.systemStartTime;
	}
	public void setControllerT(int controllerT) {
		 this.controllerT = controllerT;
	}
	public int getControllerT() {
		return this.controllerT;
	}
	public void setSensorID(int sensorID) {
		 this.sensorID = sensorID;
	}
	public int getSensorID() {
		return this.sensorID;
	}
	public void setSensorPError(float sensorPError) {
		 this.sensorPError = sensorPError;
	}
	public float getSensorPError() {
		return this.sensorPError;
	}
	public void setSensorTimeToMeasure(int sensorTimeToMeasure) {
		 this.sensorTimeToMeasure = sensorTimeToMeasure;
	}
	public int getSensorTimeToMeasure() {
		return this.sensorTimeToMeasure;
	}
	public void setSensorThreshold(float sensorThreshold) {
		 this.sensorThreshold = sensorThreshold;
	}
	public float getSensorThreshold() {
		return this.sensorThreshold;
	}
	public void setSensorLowerBound(float sensorLowerBound) {
		 this.sensorLowerBound = sensorLowerBound;
	}
	public float getSensorLowerBound() {
		return this.sensorLowerBound;
	}
	public void setSensorUpperBound(float sensorUpperBound) {
		 this.sensorUpperBound = sensorUpperBound;
	}
	public float getSensorUpperBound() {
		return this.sensorUpperBound;
	}
	public void setSensorName(String sensorName) {
		 this.sensorName = sensorName;
	}
	public String getSensorName() {
		return this.sensorName;
	}
	public void setPumpErrorRate(float pumpErrorRate) {
		 this.pumpErrorRate = pumpErrorRate;
	}
	public float getPumpErrorRate() {
		return this.pumpErrorRate;
	}
	public void setPumpWaitTime(int pumpWaitTime) {
		 this.pumpWaitTime = pumpWaitTime;
	}
	public int getPumpWaitTime() {
		return this.pumpWaitTime;
	}
	public void setActivationsBeforePump(int activationsBeforePump) {
		 this.activationsBeforePump = activationsBeforePump;
	}
	public int getActivationsBeforePump() {
		return this.activationsBeforePump;
	}
	public void setMaxTimeToAlarmAfterCH4(long maxTimeToAlarmAfterCH4) {
		 this.maxTimeToAlarmAfterCH4 = maxTimeToAlarmAfterCH4;
	}
	public long getMaxTimeToAlarmAfterCH4() {
		return this.maxTimeToAlarmAfterCH4;
	}
	public void setMaxTimeToClosePumpAfterCH4(long maxTimeToClosePumpAfterCH4) {
		 this.maxTimeToClosePumpAfterCH4 = maxTimeToClosePumpAfterCH4;
	}
	public long getMaxTimeToClosePumpAfterCH4() {
		return this.maxTimeToClosePumpAfterCH4;
	}
	public void setCOThreshold(float COThreshold) {
		 this.COThreshold = COThreshold;
	}
	public float getCOThreshold() {
		return this.COThreshold;
	}
	public void setCH4Threshold(float CH4Threshold) {
		 this.CH4Threshold = CH4Threshold;
	}
	public float getCH4Threshold() {
		return this.CH4Threshold;
	}
	public void setAirThreshold(float airThreshold) {
		 this.airThreshold = airThreshold;
	}
	public float getAirThreshold() {
		return this.airThreshold;
	}

	/*--------------------- operations ---------------------*/

	// default constructor
	public InitializationPacket() {
		super();

		// initialize attributes
		this.setSensorName("");

		/* user defined constructor body */
	}

	// constructor using fields
	public InitializationPacket(long systemStartTime, int controllerT, int sensorID, float sensorPError, int sensorTimeToMeasure, float sensorThreshold, float sensorLowerBound, float sensorUpperBound, String sensorName, float pumpErrorRate, int pumpWaitTime, int activationsBeforePump, long maxTimeToAlarmAfterCH4, long maxTimeToClosePumpAfterCH4, float COThreshold, float CH4Threshold, float airThreshold) {
		super();

		this.systemStartTime = systemStartTime;
		this.controllerT = controllerT;
		this.sensorID = sensorID;
		this.sensorPError = sensorPError;
		this.sensorTimeToMeasure = sensorTimeToMeasure;
		this.sensorThreshold = sensorThreshold;
		this.sensorLowerBound = sensorLowerBound;
		this.sensorUpperBound = sensorUpperBound;
		this.sensorName = sensorName;
		this.pumpErrorRate = pumpErrorRate;
		this.pumpWaitTime = pumpWaitTime;
		this.activationsBeforePump = activationsBeforePump;
		this.maxTimeToAlarmAfterCH4 = maxTimeToAlarmAfterCH4;
		this.maxTimeToClosePumpAfterCH4 = maxTimeToClosePumpAfterCH4;
		this.COThreshold = COThreshold;
		this.CH4Threshold = CH4Threshold;
		this.airThreshold = airThreshold;

		/* user defined constructor body */
	}

	// deep copy
	public InitializationPacket deepCopy() {
		InitializationPacket copy = new InitializationPacket();
		copy.systemStartTime = systemStartTime;
		copy.controllerT = controllerT;
		copy.sensorID = sensorID;
		copy.sensorPError = sensorPError;
		copy.sensorTimeToMeasure = sensorTimeToMeasure;
		copy.sensorThreshold = sensorThreshold;
		copy.sensorLowerBound = sensorLowerBound;
		copy.sensorUpperBound = sensorUpperBound;
		copy.sensorName = sensorName;
		copy.pumpErrorRate = pumpErrorRate;
		copy.pumpWaitTime = pumpWaitTime;
		copy.activationsBeforePump = activationsBeforePump;
		copy.maxTimeToAlarmAfterCH4 = maxTimeToAlarmAfterCH4;
		copy.maxTimeToClosePumpAfterCH4 = maxTimeToClosePumpAfterCH4;
		copy.COThreshold = COThreshold;
		copy.CH4Threshold = CH4Threshold;
		copy.airThreshold = airThreshold;
		return copy;
	}
};
