package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.*;
import org.eclipse.etrice.runtime.java.modelbase.*;
import org.eclipse.etrice.runtime.java.debugging.*;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;

import room.basic.service.logging.*;
import room.basic.service.timing.*;
import room.basic.service.logging.Log.*;
import room.basic.service.timing.PTimer.*;



public abstract class ActivableActor extends ActorClassBase {


	//--------------------- ports

	//--------------------- saps
	protected PTimerConjPort timer = null;
	protected LogConjPort logger = null;

	//--------------------- services

	//--------------------- optional actors

	//--------------------- interface item IDs
	public static final int IFITEM_timer = 1;
	public static final int IFITEM_logger = 2;

	/*--------------------- attributes ---------------------*/
	public  long systemStartTime;
	public  long activationStartTime;
	public  String actorName;

	/*--------------------- operations ---------------------*/
	public  void Log(String str) {
		LogLevel(str, 1);
	}
	public  void LogLevel(String str, int pri) {
		long currentTime = System.currentTimeMillis() - systemStartTime;
		logger.log(pri, String.format("\n[%s ms][%s] - %s", currentTime, actorName, str));
	}


	//--------------------- construction
	public ActivableActor(IRTObject parent, String name) {
		super(parent, name);
		setClassName("ActivableActor");

		// initialize attributes
		this.setSystemStartTime(0L);
		this.setActivationStartTime(0L);
		this.setActorName("");

		// own ports

		// own saps
		timer = new PTimerConjPort(this, "timer", IFITEM_timer, 0);
		logger = new LogConjPort(this, "logger", IFITEM_logger, 0);

		// own service implementations

		// sub actors

		// wiring


		/* user defined constructor body */

	}

	/* --------------------- attribute setters and getters */
	public void setSystemStartTime(long systemStartTime) {
		 this.systemStartTime = systemStartTime;
	}
	public long getSystemStartTime() {
		return this.systemStartTime;
	}
	public void setActivationStartTime(long activationStartTime) {
		 this.activationStartTime = activationStartTime;
	}
	public long getActivationStartTime() {
		return this.activationStartTime;
	}
	public void setActorName(String actorName) {
		 this.actorName = actorName;
	}
	public String getActorName() {
		return this.actorName;
	}


	//--------------------- port getters
	public PTimerConjPort getTimer (){
		return this.timer;
	}
	public LogConjPort getLogger (){
		return this.logger;
	}

	//--------------------- lifecycle functions
	public void stop(){
		super.stop();
	}

	public void destroy(){
		/* user defined destructor body */
		DebuggingService.getInstance().addMessageActorDestroy(this);
		super.destroy();
	}

	//--------------------- no state machine
	public void receiveEvent(InterfaceItemBase ifitem, int evt, Object data) {
		handleSystemEvent(ifitem, evt, data);
	}

	public void executeInitTransition() {}

};
