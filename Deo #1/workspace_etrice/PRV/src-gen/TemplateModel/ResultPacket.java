package TemplateModel;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;
import java.io.Serializable;




public class ResultPacket implements Serializable {

	private static final long serialVersionUID = 122136404L;


	/*--------------------- attributes ---------------------*/
	public  float result;
	public  int sensorID;
	public  String sensorName;
	public  int waterFlowStatus;

	/* --------------------- attribute setters and getters */
	public void setResult(float result) {
		 this.result = result;
	}
	public float getResult() {
		return this.result;
	}
	public void setSensorID(int sensorID) {
		 this.sensorID = sensorID;
	}
	public int getSensorID() {
		return this.sensorID;
	}
	public void setSensorName(String sensorName) {
		 this.sensorName = sensorName;
	}
	public String getSensorName() {
		return this.sensorName;
	}
	public void setWaterFlowStatus(int waterFlowStatus) {
		 this.waterFlowStatus = waterFlowStatus;
	}
	public int getWaterFlowStatus() {
		return this.waterFlowStatus;
	}

	/*--------------------- operations ---------------------*/

	// default constructor
	public ResultPacket() {
		super();

		// initialize attributes
		this.setSensorName("");

		/* user defined constructor body */
	}

	// constructor using fields
	public ResultPacket(float result, int sensorID, String sensorName, int waterFlowStatus) {
		super();

		this.result = result;
		this.sensorID = sensorID;
		this.sensorName = sensorName;
		this.waterFlowStatus = waterFlowStatus;

		/* user defined constructor body */
	}

	// deep copy
	public ResultPacket deepCopy() {
		ResultPacket copy = new ResultPacket();
		copy.result = result;
		copy.sensorID = sensorID;
		copy.sensorName = sensorName;
		copy.waterFlowStatus = waterFlowStatus;
		return copy;
	}
};
