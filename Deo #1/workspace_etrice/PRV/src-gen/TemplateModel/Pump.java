package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.*;
import org.eclipse.etrice.runtime.java.modelbase.*;
import org.eclipse.etrice.runtime.java.debugging.*;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;

import room.basic.service.logging.*;
import room.basic.service.timing.*;
import TemplateModel.InitOrUpdateProtocol.*;
import room.basic.service.logging.Log.*;
import room.basic.service.timing.PTimer.*;
import TemplateModel.PumpProtocol.*;



public class Pump extends ActivableActor {


	//--------------------- ports
	protected InitOrUpdateProtocolConjPort InitPort = null;
	protected PumpProtocolPort PumpPort = null;

	//--------------------- saps

	//--------------------- services

	//--------------------- optional actors

	//--------------------- interface item IDs
	public static final int IFITEM_InitPort = 3;
	public static final int IFITEM_PumpPort = 4;

	/*--------------------- attributes ---------------------*/
	public  float pumpErrorRate;
	public  int pumpWaitTime;

	/*--------------------- operations ---------------------*/


	//--------------------- construction
	public Pump(IRTObject parent, String name) {
		super(parent, name);
		setClassName("Pump");

		// initialize attributes
		this.setPumpErrorRate(0f);
		this.setPumpWaitTime(0);

		// own ports
		InitPort = new InitOrUpdateProtocolConjPort(this, "InitPort", IFITEM_InitPort);
		PumpPort = new PumpProtocolPort(this, "PumpPort", IFITEM_PumpPort);

		// own saps

		// own service implementations

		// sub actors

		// wiring


		/* user defined constructor body */

	}

	/* --------------------- attribute setters and getters */
	public void setPumpErrorRate(float pumpErrorRate) {
		 this.pumpErrorRate = pumpErrorRate;
	}
	public float getPumpErrorRate() {
		return this.pumpErrorRate;
	}
	public void setPumpWaitTime(int pumpWaitTime) {
		 this.pumpWaitTime = pumpWaitTime;
	}
	public int getPumpWaitTime() {
		return this.pumpWaitTime;
	}


	//--------------------- port getters
	public InitOrUpdateProtocolConjPort getInitPort (){
		return this.InitPort;
	}
	public PumpProtocolPort getPumpPort (){
		return this.PumpPort;
	}

	//--------------------- lifecycle functions
	public void stop(){
		super.stop();
	}

	public void destroy(){
		/* user defined destructor body */
		DebuggingService.getInstance().addMessageActorDestroy(this);
		super.destroy();
	}

	/* state IDs */
	public static final int STATE_UNINITIALIZED = 2;
	public static final int STATE_INACTIVE = 3;
	public static final int STATE_ACTIVE = 4;
	public static final int STATE_WAIT_FOR_ACTIVE = 5;
	public static final int STATE_WAIT_FOR_INACTIVE = 6;
	public static final int STATE_MAX = 7;
	
	/* transition chains */
	public static final int CHAIN_TRANS_INITIAL_TO__UNINITIALIZED = 1;
	public static final int CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_INACTIVE_BY_InitializeInitPort = 2;
	public static final int CHAIN_TRANS_tr1_FROM_INACTIVE_TO_ActivateChoice_BY_ActivatePumpPumpPort = 3;
	public static final int CHAIN_TRANS_tr2_FROM_INACTIVE_TO_INACTIVE_BY_DeactivatePumpPumpPort_tr2 = 4;
	public static final int CHAIN_TRANS_tr3_FROM_ACTIVE_TO_ACTIVE_BY_ActivatePumpPumpPort_tr3 = 5;
	public static final int CHAIN_TRANS_tr6_FROM_WAIT_FOR_ACTIVE_TO_ACTIVE_BY_timeouttimer = 6;
	public static final int CHAIN_TRANS_tr7_FROM_ACTIVE_TO_DeactivateChoice_BY_DeactivatePumpPumpPort = 7;
	public static final int CHAIN_TRANS_tr10_FROM_WAIT_FOR_INACTIVE_TO_INACTIVE_BY_timeouttimer = 8;
	public static final int CHAIN_TRANS_tr11_FROM_INACTIVE_TO_INACTIVE_BY_PollPumpPumpPort_tr11 = 9;
	public static final int CHAIN_TRANS_tr12_FROM_WAIT_FOR_ACTIVE_TO_WAIT_FOR_ACTIVE_BY_PollPumpPumpPort_tr12 = 10;
	public static final int CHAIN_TRANS_tr13_FROM_WAIT_FOR_INACTIVE_TO_WAIT_FOR_INACTIVE_BY_PollPumpPumpPort_tr13 = 11;
	public static final int CHAIN_TRANS_tr14_FROM_ACTIVE_TO_ACTIVE_BY_PollPumpPumpPort_tr14 = 12;
	
	/* triggers */
	public static final int POLLING = 0;
	public static final int TRIG_InitPort__Initialize = IFITEM_InitPort + EVT_SHIFT*InitOrUpdateProtocol.OUT_Initialize;
	public static final int TRIG_PumpPort__ActivatePump = IFITEM_PumpPort + EVT_SHIFT*PumpProtocol.IN_ActivatePump;
	public static final int TRIG_PumpPort__DeactivatePump = IFITEM_PumpPort + EVT_SHIFT*PumpProtocol.IN_DeactivatePump;
	public static final int TRIG_PumpPort__PollPump = IFITEM_PumpPort + EVT_SHIFT*PumpProtocol.IN_PollPump;
	public static final int TRIG_timer__timeout = IFITEM_timer + EVT_SHIFT*PTimer.OUT_timeout;
	public static final int TRIG_timer__internalTimer = IFITEM_timer + EVT_SHIFT*PTimer.OUT_internalTimer;
	public static final int TRIG_timer__internalTimeout = IFITEM_timer + EVT_SHIFT*PTimer.OUT_internalTimeout;
	
	// state names
	protected static final String stateStrings[] = {
		"<no state>",
		"<top>",
		"UNINITIALIZED",
		"INACTIVE",
		"ACTIVE",
		"WAIT_FOR_ACTIVE",
		"WAIT_FOR_INACTIVE"
	};
	
	// history
	protected int history[] = {NO_STATE, NO_STATE, NO_STATE, NO_STATE, NO_STATE, NO_STATE, NO_STATE};
	
	private void setState(int new_state) {
		DebuggingService.getInstance().addActorState(this,stateStrings[new_state]);
		this.state = new_state;
	}
	
	/* Entry and Exit Codes */
	
	/* Action Codes */
	protected void action_TRANS_tr0_FROM_UNINITIALIZED_TO_INACTIVE_BY_InitializeInitPort(InterfaceItemBase ifitem, InitializationPacket ip) {
	    this.actorName = "P";
	    this.systemStartTime = ip.systemStartTime;
	    this.pumpErrorRate = ip.pumpErrorRate;
	    this.pumpWaitTime = ip.pumpWaitTime;
	    
	    Log("Pump initialized!");
	}
	protected void action_TRANS_tr1_FROM_INACTIVE_TO_ActivateChoice_BY_ActivatePumpPumpPort(InterfaceItemBase ifitem) {
	    Log("Got activate message while inactive!");
	}
	protected void action_TRANS_tr2_FROM_INACTIVE_TO_INACTIVE_BY_DeactivatePumpPumpPort_tr2(InterfaceItemBase ifitem) {
	    Log("Inactive pump wants to be deactivated");
	}
	protected void action_TRANS_tr3_FROM_ACTIVE_TO_ACTIVE_BY_ActivatePumpPumpPort_tr3(InterfaceItemBase ifitem) {
	    Log("Activated pump wants to be activated");
	}
	protected void action_TRANS_tr4_FROM_ActivateChoice_TO_INACTIVE(InterfaceItemBase ifitem) {
	    Log("Pump failed to get activated!");
	}
	protected void action_TRANS_tr5_FROM_ActivateChoice_TO_WAIT_FOR_ACTIVE_COND_tr5(InterfaceItemBase ifitem) {
	    Log(String.format("Pump waiting to be activated for %d ms!", pumpWaitTime));
	    timer.startTimeout(pumpWaitTime);
	}
	protected void action_TRANS_tr6_FROM_WAIT_FOR_ACTIVE_TO_ACTIVE_BY_timeouttimer(InterfaceItemBase ifitem) {
	    Log("Pump fully activated!");
	    water.Water.instance().decreaseWaterLevel();
	    water.Water.instance().decreaseWaterLevel();
	}
	protected void action_TRANS_tr7_FROM_ACTIVE_TO_DeactivateChoice_BY_DeactivatePumpPumpPort(InterfaceItemBase ifitem) {
	    Log("Got deactivate message while active!");
	}
	protected void action_TRANS_tr8_FROM_DeactivateChoice_TO_ACTIVE(InterfaceItemBase ifitem) {
	    Log("Pump failed to get deactivated!");
	}
	protected void action_TRANS_tr9_FROM_DeactivateChoice_TO_WAIT_FOR_INACTIVE_COND_tr9(InterfaceItemBase ifitem) {
	    Log(String.format("Pump waiting to be deactivated for %d ms!", pumpWaitTime));
	    timer.startTimeout(pumpWaitTime);
	}
	protected void action_TRANS_tr10_FROM_WAIT_FOR_INACTIVE_TO_INACTIVE_BY_timeouttimer(InterfaceItemBase ifitem) {
	    Log("Pump fully deactivated!");
	    water.Water.instance().increaseWaterLevel();
	    water.Water.instance().increaseWaterLevel();
	}
	protected void action_TRANS_tr14_FROM_ACTIVE_TO_ACTIVE_BY_PollPumpPumpPort_tr14(InterfaceItemBase ifitem) {
	    water.Water.instance().decreaseWaterLevel();
	}
	
	/* State Switch Methods */
	/**
	 * calls exit codes while exiting from the current state to one of its
	 * parent states while remembering the history
	 * @param current__et - the current state
	 * @param to - the final parent state
	 */
	private void exitTo(int current__et, int to) {
		while (current__et!=to) {
			switch (current__et) {
				case STATE_UNINITIALIZED:
					this.history[STATE_TOP] = STATE_UNINITIALIZED;
					current__et = STATE_TOP;
					break;
				case STATE_INACTIVE:
					this.history[STATE_TOP] = STATE_INACTIVE;
					current__et = STATE_TOP;
					break;
				case STATE_ACTIVE:
					this.history[STATE_TOP] = STATE_ACTIVE;
					current__et = STATE_TOP;
					break;
				case STATE_WAIT_FOR_ACTIVE:
					this.history[STATE_TOP] = STATE_WAIT_FOR_ACTIVE;
					current__et = STATE_TOP;
					break;
				case STATE_WAIT_FOR_INACTIVE:
					this.history[STATE_TOP] = STATE_WAIT_FOR_INACTIVE;
					current__et = STATE_TOP;
					break;
				default:
					/* should not occur */
					break;
			}
		}
	}
	
	/**
	 * calls action, entry and exit codes along a transition chain. The generic data are cast to typed data
	 * matching the trigger of this chain. The ID of the final state is returned
	 * @param chain__et - the chain ID
	 * @param generic_data__et - the generic data pointer
	 * @return the +/- ID of the final state either with a positive sign, that indicates to execute the state's entry code, or a negative sign vice versa
	 */
	private int executeTransitionChain(int chain__et, InterfaceItemBase ifitem, Object generic_data__et) {
		switch (chain__et) {
			case Pump.CHAIN_TRANS_INITIAL_TO__UNINITIALIZED:
			{
				return STATE_UNINITIALIZED;
			}
			case Pump.CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_INACTIVE_BY_InitializeInitPort:
			{
				InitializationPacket ip = (InitializationPacket) generic_data__et;
				action_TRANS_tr0_FROM_UNINITIALIZED_TO_INACTIVE_BY_InitializeInitPort(ifitem, ip);
				return STATE_INACTIVE;
			}
			case Pump.CHAIN_TRANS_tr1_FROM_INACTIVE_TO_ActivateChoice_BY_ActivatePumpPumpPort:
			{
				action_TRANS_tr1_FROM_INACTIVE_TO_ActivateChoice_BY_ActivatePumpPumpPort(ifitem);
				if (pumpErrorRate <= Math.random()) {
				action_TRANS_tr5_FROM_ActivateChoice_TO_WAIT_FOR_ACTIVE_COND_tr5(ifitem);
				return STATE_WAIT_FOR_ACTIVE;}
				else {
				action_TRANS_tr4_FROM_ActivateChoice_TO_INACTIVE(ifitem);
				return STATE_INACTIVE;}
			}
			case Pump.CHAIN_TRANS_tr2_FROM_INACTIVE_TO_INACTIVE_BY_DeactivatePumpPumpPort_tr2:
			{
				action_TRANS_tr2_FROM_INACTIVE_TO_INACTIVE_BY_DeactivatePumpPumpPort_tr2(ifitem);
				return STATE_INACTIVE;
			}
			case Pump.CHAIN_TRANS_tr3_FROM_ACTIVE_TO_ACTIVE_BY_ActivatePumpPumpPort_tr3:
			{
				action_TRANS_tr3_FROM_ACTIVE_TO_ACTIVE_BY_ActivatePumpPumpPort_tr3(ifitem);
				return STATE_ACTIVE;
			}
			case Pump.CHAIN_TRANS_tr6_FROM_WAIT_FOR_ACTIVE_TO_ACTIVE_BY_timeouttimer:
			{
				action_TRANS_tr6_FROM_WAIT_FOR_ACTIVE_TO_ACTIVE_BY_timeouttimer(ifitem);
				return STATE_ACTIVE;
			}
			case Pump.CHAIN_TRANS_tr7_FROM_ACTIVE_TO_DeactivateChoice_BY_DeactivatePumpPumpPort:
			{
				action_TRANS_tr7_FROM_ACTIVE_TO_DeactivateChoice_BY_DeactivatePumpPumpPort(ifitem);
				if (pumpErrorRate <= Math.random()) {
				action_TRANS_tr9_FROM_DeactivateChoice_TO_WAIT_FOR_INACTIVE_COND_tr9(ifitem);
				return STATE_WAIT_FOR_INACTIVE;}
				else {
				action_TRANS_tr8_FROM_DeactivateChoice_TO_ACTIVE(ifitem);
				return STATE_ACTIVE;}
			}
			case Pump.CHAIN_TRANS_tr10_FROM_WAIT_FOR_INACTIVE_TO_INACTIVE_BY_timeouttimer:
			{
				action_TRANS_tr10_FROM_WAIT_FOR_INACTIVE_TO_INACTIVE_BY_timeouttimer(ifitem);
				return STATE_INACTIVE;
			}
			case Pump.CHAIN_TRANS_tr11_FROM_INACTIVE_TO_INACTIVE_BY_PollPumpPumpPort_tr11:
			{
				return STATE_INACTIVE;
			}
			case Pump.CHAIN_TRANS_tr12_FROM_WAIT_FOR_ACTIVE_TO_WAIT_FOR_ACTIVE_BY_PollPumpPumpPort_tr12:
			{
				return STATE_WAIT_FOR_ACTIVE;
			}
			case Pump.CHAIN_TRANS_tr13_FROM_WAIT_FOR_INACTIVE_TO_WAIT_FOR_INACTIVE_BY_PollPumpPumpPort_tr13:
			{
				return STATE_WAIT_FOR_INACTIVE;
			}
			case Pump.CHAIN_TRANS_tr14_FROM_ACTIVE_TO_ACTIVE_BY_PollPumpPumpPort_tr14:
			{
				action_TRANS_tr14_FROM_ACTIVE_TO_ACTIVE_BY_PollPumpPumpPort_tr14(ifitem);
				return STATE_ACTIVE;
			}
				default:
					/* should not occur */
					break;
		}
		return NO_STATE;
	}
	
	/**
	 * calls entry codes while entering a state's history. The ID of the final leaf state is returned
	 * @param state__et - the state which is entered
	 * @return - the ID of the final leaf state
	 */
	private int enterHistory(int state__et) {
		if (state__et >= STATE_MAX) {
			state__et =  (state__et - STATE_MAX);
		}
		while (true) {
			switch (state__et) {
				case STATE_UNINITIALIZED:
					/* in leaf state: return state id */
					return STATE_UNINITIALIZED;
				case STATE_INACTIVE:
					/* in leaf state: return state id */
					return STATE_INACTIVE;
				case STATE_ACTIVE:
					/* in leaf state: return state id */
					return STATE_ACTIVE;
				case STATE_WAIT_FOR_ACTIVE:
					/* in leaf state: return state id */
					return STATE_WAIT_FOR_ACTIVE;
				case STATE_WAIT_FOR_INACTIVE:
					/* in leaf state: return state id */
					return STATE_WAIT_FOR_INACTIVE;
				case STATE_TOP:
					state__et = this.history[STATE_TOP];
					break;
				default:
					/* should not occur */
					break;
			}
		}
		/* return NO_STATE; // required by CDT but detected as unreachable by JDT because of while (true) */
	}
	
	public void executeInitTransition() {
		int chain__et = Pump.CHAIN_TRANS_INITIAL_TO__UNINITIALIZED;
		int next__et = executeTransitionChain(chain__et, null, null);
		next__et = enterHistory(next__et);
		setState(next__et);
	}
	
	/* receiveEvent contains the main implementation of the FSM */
	public void receiveEventInternal(InterfaceItemBase ifitem, int localId, int evt, Object generic_data__et) {
		int trigger__et = localId + EVT_SHIFT*evt;
		int chain__et = NOT_CAUGHT;
		int catching_state__et = NO_STATE;
	
		if (!handleSystemEvent(ifitem, evt, generic_data__et)) {
			switch (getState()) {
			    case STATE_UNINITIALIZED:
			        switch(trigger__et) {
			                case TRIG_InitPort__Initialize:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_INACTIVE_BY_InitializeInitPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_INACTIVE:
			        switch(trigger__et) {
			                case TRIG_PumpPort__ActivatePump:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr1_FROM_INACTIVE_TO_ActivateChoice_BY_ActivatePumpPumpPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_PumpPort__DeactivatePump:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr2_FROM_INACTIVE_TO_INACTIVE_BY_DeactivatePumpPumpPort_tr2;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_PumpPort__PollPump:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr11_FROM_INACTIVE_TO_INACTIVE_BY_PollPumpPumpPort_tr11;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_ACTIVE:
			        switch(trigger__et) {
			                case TRIG_PumpPort__ActivatePump:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr3_FROM_ACTIVE_TO_ACTIVE_BY_ActivatePumpPumpPort_tr3;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_PumpPort__DeactivatePump:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr7_FROM_ACTIVE_TO_DeactivateChoice_BY_DeactivatePumpPumpPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_PumpPort__PollPump:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr14_FROM_ACTIVE_TO_ACTIVE_BY_PollPumpPumpPort_tr14;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_WAIT_FOR_ACTIVE:
			        switch(trigger__et) {
			                case TRIG_timer__timeout:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr6_FROM_WAIT_FOR_ACTIVE_TO_ACTIVE_BY_timeouttimer;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_PumpPort__PollPump:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr12_FROM_WAIT_FOR_ACTIVE_TO_WAIT_FOR_ACTIVE_BY_PollPumpPumpPort_tr12;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_WAIT_FOR_INACTIVE:
			        switch(trigger__et) {
			                case TRIG_timer__timeout:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr10_FROM_WAIT_FOR_INACTIVE_TO_INACTIVE_BY_timeouttimer;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_PumpPort__PollPump:
			                    {
			                        chain__et = Pump.CHAIN_TRANS_tr13_FROM_WAIT_FOR_INACTIVE_TO_WAIT_FOR_INACTIVE_BY_PollPumpPumpPort_tr13;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    default:
			        /* should not occur */
			        break;
			}
		}
		if (chain__et != NOT_CAUGHT) {
			exitTo(getState(), catching_state__et);
			{
				int next__et = executeTransitionChain(chain__et, ifitem, generic_data__et);
				next__et = enterHistory(next__et);
				setState(next__et);
			}
		}
	}
	public void receiveEvent(InterfaceItemBase ifitem, int evt, Object generic_data__et) {
		int localId = (ifitem==null)? 0 : ifitem.getLocalId();
		receiveEventInternal(ifitem, localId, evt, generic_data__et);
	}

};
