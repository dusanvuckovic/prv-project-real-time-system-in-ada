package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.*;
import org.eclipse.etrice.runtime.java.modelbase.*;
import org.eclipse.etrice.runtime.java.debugging.*;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;

import room.basic.service.logging.*;
import room.basic.service.timing.*;
import TemplateModel.InitOrUpdateProtocol.*;
import room.basic.service.logging.Log.*;
import room.basic.service.timing.PTimer.*;
import TemplateModel.PumpProtocol.*;
import TemplateModel.SensorProtocol.*;



public class Controller extends ActivableActor {


	//--------------------- ports
	protected InitOrUpdateProtocolConjPort Init = null;
	protected SensorProtocolConjPort COPort = null;
	protected SensorProtocolConjPort CH4Port = null;
	protected SensorProtocolConjPort AirFlowPort = null;
	protected SensorProtocolConjPort WaterFlowPort = null;
	protected SensorProtocolConjPort WaterHighPort = null;
	protected SensorProtocolConjPort WaterLowPort = null;
	protected PumpProtocolConjPort PumpPort = null;

	//--------------------- saps

	//--------------------- services

	//--------------------- optional actors

	//--------------------- interface item IDs
	public static final int IFITEM_Init = 3;
	public static final int IFITEM_COPort = 4;
	public static final int IFITEM_CH4Port = 5;
	public static final int IFITEM_AirFlowPort = 6;
	public static final int IFITEM_WaterFlowPort = 7;
	public static final int IFITEM_WaterHighPort = 8;
	public static final int IFITEM_WaterLowPort = 9;
	public static final int IFITEM_PumpPort = 10;

	/*--------------------- attributes ---------------------*/
	public  boolean alarmRaised;
	public  boolean lowInterrupt;
	public  boolean highInterrupt;
	public  int remaining;
	public  boolean hadError[];
	public  boolean triedActivatingPump;
	public  boolean triedDeactivatingPump;
	public  boolean pumpStatusKnown;
	public  int activationsBeforePump;
	public  int currentActivationsBeforePump;
	public  int controllerT;
	public  int waterFlowStatus;
	public  float CH4Level;
	public  float COThreshold;
	public  float CH4Threshold;
	public  float airThreshold;
	public  int sensorTimeToMeasure;
	public  long timePumpChangeRequested;
	public  long timeMethaneBecameCritical;
	public  long maxTimeToAlarmAfterCH4;
	public  long maxTimeToClosePumpAfterCH4;

	/*--------------------- operations ---------------------*/
	public  void startNewPeriod() {
		remaining = 4;
		highInterrupt = false;
		lowInterrupt = false;				
		COPort.StartMeasurement();
		CH4Port.StartMeasurement();
		AirFlowPort.StartMeasurement();
		WaterFlowPort.StartMeasurement();
		WaterHighPort.StartMeasurement();
		WaterLowPort.StartMeasurement();
		timer.startTimeout(sensorTimeToMeasure+10);	
		PumpPort.PollPump();				
	}
	public  void raiseAlarm() {
		Log("ALARM ALARM ALARM!");
		alarmRaised = true;
	}


	//--------------------- construction
	public Controller(IRTObject parent, String name) {
		super(parent, name);
		setClassName("Controller");

		// initialize attributes
		this.setAlarmRaised(false);
		this.setLowInterrupt(false);
		this.setHighInterrupt(false);
		this.setRemaining(0);
		{
			boolean[] array = new boolean[6];
			for (int i=0;i<6;i++){
				array[i] = false;
			}
			this.setHadError(array);
		}
		this.setTriedActivatingPump(false);
		this.setTriedDeactivatingPump(false);
		this.setPumpStatusKnown(false);
		this.setActivationsBeforePump(0);
		this.setCurrentActivationsBeforePump(0);
		this.setControllerT(0);
		this.setWaterFlowStatus(0);
		this.setCH4Level(0f);
		this.setCOThreshold(0f);
		this.setCH4Threshold(0f);
		this.setAirThreshold(0f);
		this.setSensorTimeToMeasure(0);
		this.setTimePumpChangeRequested(0L);
		this.setTimeMethaneBecameCritical(0L);
		this.setMaxTimeToAlarmAfterCH4(0L);
		this.setMaxTimeToClosePumpAfterCH4(0L);

		// own ports
		Init = new InitOrUpdateProtocolConjPort(this, "Init", IFITEM_Init);
		COPort = new SensorProtocolConjPort(this, "COPort", IFITEM_COPort);
		CH4Port = new SensorProtocolConjPort(this, "CH4Port", IFITEM_CH4Port);
		AirFlowPort = new SensorProtocolConjPort(this, "AirFlowPort", IFITEM_AirFlowPort);
		WaterFlowPort = new SensorProtocolConjPort(this, "WaterFlowPort", IFITEM_WaterFlowPort);
		WaterHighPort = new SensorProtocolConjPort(this, "WaterHighPort", IFITEM_WaterHighPort);
		WaterLowPort = new SensorProtocolConjPort(this, "WaterLowPort", IFITEM_WaterLowPort);
		PumpPort = new PumpProtocolConjPort(this, "PumpPort", IFITEM_PumpPort);

		// own saps

		// own service implementations

		// sub actors

		// wiring


		/* user defined constructor body */

	}

	/* --------------------- attribute setters and getters */
	public void setAlarmRaised(boolean alarmRaised) {
		 this.alarmRaised = alarmRaised;
	}
	public boolean getAlarmRaised() {
		return this.alarmRaised;
	}
	public void setLowInterrupt(boolean lowInterrupt) {
		 this.lowInterrupt = lowInterrupt;
	}
	public boolean getLowInterrupt() {
		return this.lowInterrupt;
	}
	public void setHighInterrupt(boolean highInterrupt) {
		 this.highInterrupt = highInterrupt;
	}
	public boolean getHighInterrupt() {
		return this.highInterrupt;
	}
	public void setRemaining(int remaining) {
		 this.remaining = remaining;
	}
	public int getRemaining() {
		return this.remaining;
	}
	public void setHadError(boolean[] hadError) {
		 this.hadError = hadError;
	}
	public boolean[] getHadError() {
		return this.hadError;
	}
	public void setTriedActivatingPump(boolean triedActivatingPump) {
		 this.triedActivatingPump = triedActivatingPump;
	}
	public boolean getTriedActivatingPump() {
		return this.triedActivatingPump;
	}
	public void setTriedDeactivatingPump(boolean triedDeactivatingPump) {
		 this.triedDeactivatingPump = triedDeactivatingPump;
	}
	public boolean getTriedDeactivatingPump() {
		return this.triedDeactivatingPump;
	}
	public void setPumpStatusKnown(boolean pumpStatusKnown) {
		 this.pumpStatusKnown = pumpStatusKnown;
	}
	public boolean getPumpStatusKnown() {
		return this.pumpStatusKnown;
	}
	public void setActivationsBeforePump(int activationsBeforePump) {
		 this.activationsBeforePump = activationsBeforePump;
	}
	public int getActivationsBeforePump() {
		return this.activationsBeforePump;
	}
	public void setCurrentActivationsBeforePump(int currentActivationsBeforePump) {
		 this.currentActivationsBeforePump = currentActivationsBeforePump;
	}
	public int getCurrentActivationsBeforePump() {
		return this.currentActivationsBeforePump;
	}
	public void setControllerT(int controllerT) {
		 this.controllerT = controllerT;
	}
	public int getControllerT() {
		return this.controllerT;
	}
	public void setWaterFlowStatus(int waterFlowStatus) {
		 this.waterFlowStatus = waterFlowStatus;
	}
	public int getWaterFlowStatus() {
		return this.waterFlowStatus;
	}
	public void setCH4Level(float CH4Level) {
		 this.CH4Level = CH4Level;
	}
	public float getCH4Level() {
		return this.CH4Level;
	}
	public void setCOThreshold(float COThreshold) {
		 this.COThreshold = COThreshold;
	}
	public float getCOThreshold() {
		return this.COThreshold;
	}
	public void setCH4Threshold(float CH4Threshold) {
		 this.CH4Threshold = CH4Threshold;
	}
	public float getCH4Threshold() {
		return this.CH4Threshold;
	}
	public void setAirThreshold(float airThreshold) {
		 this.airThreshold = airThreshold;
	}
	public float getAirThreshold() {
		return this.airThreshold;
	}
	public void setSensorTimeToMeasure(int sensorTimeToMeasure) {
		 this.sensorTimeToMeasure = sensorTimeToMeasure;
	}
	public int getSensorTimeToMeasure() {
		return this.sensorTimeToMeasure;
	}
	public void setTimePumpChangeRequested(long timePumpChangeRequested) {
		 this.timePumpChangeRequested = timePumpChangeRequested;
	}
	public long getTimePumpChangeRequested() {
		return this.timePumpChangeRequested;
	}
	public void setTimeMethaneBecameCritical(long timeMethaneBecameCritical) {
		 this.timeMethaneBecameCritical = timeMethaneBecameCritical;
	}
	public long getTimeMethaneBecameCritical() {
		return this.timeMethaneBecameCritical;
	}
	public void setMaxTimeToAlarmAfterCH4(long maxTimeToAlarmAfterCH4) {
		 this.maxTimeToAlarmAfterCH4 = maxTimeToAlarmAfterCH4;
	}
	public long getMaxTimeToAlarmAfterCH4() {
		return this.maxTimeToAlarmAfterCH4;
	}
	public void setMaxTimeToClosePumpAfterCH4(long maxTimeToClosePumpAfterCH4) {
		 this.maxTimeToClosePumpAfterCH4 = maxTimeToClosePumpAfterCH4;
	}
	public long getMaxTimeToClosePumpAfterCH4() {
		return this.maxTimeToClosePumpAfterCH4;
	}


	//--------------------- port getters
	public InitOrUpdateProtocolConjPort getInit (){
		return this.Init;
	}
	public SensorProtocolConjPort getCOPort (){
		return this.COPort;
	}
	public SensorProtocolConjPort getCH4Port (){
		return this.CH4Port;
	}
	public SensorProtocolConjPort getAirFlowPort (){
		return this.AirFlowPort;
	}
	public SensorProtocolConjPort getWaterFlowPort (){
		return this.WaterFlowPort;
	}
	public SensorProtocolConjPort getWaterHighPort (){
		return this.WaterHighPort;
	}
	public SensorProtocolConjPort getWaterLowPort (){
		return this.WaterLowPort;
	}
	public PumpProtocolConjPort getPumpPort (){
		return this.PumpPort;
	}

	//--------------------- lifecycle functions
	public void stop(){
		super.stop();
	}

	public void destroy(){
		/* user defined destructor body */
		DebuggingService.getInstance().addMessageActorDestroy(this);
		super.destroy();
	}

	/* state IDs */
	public static final int STATE_UNINITIALIZED = 2;
	public static final int STATE_QUERYING = 3;
	public static final int STATE_PROCESSING = 4;
	public static final int STATE_MAX = 5;
	
	/* transition chains */
	public static final int CHAIN_TRANS_INITIAL_TO__UNINITIALIZED = 1;
	public static final int CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_QUERYING_BY_InitializeInit = 2;
	public static final int CHAIN_TRANS_tr1_FROM_QUERYING_TO_PROCESSING_BY_timeouttimer = 3;
	public static final int CHAIN_TRANS_tr2_FROM_PROCESSING_TO_QUERYING_BY_timeouttimer = 4;
	public static final int CHAIN_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3 = 5;
	public static final int CHAIN_TRANS_tr4_FROM_PROCESSING_TO_PROCESSING_BY_ReturnResultCH4PortReturnResultAirFlowPortReturnResultCOPortReturnResultWaterFlowPort_tr4 = 6;
	public static final int CHAIN_TRANS_tr5_FROM_PROCESSING_TO_PROCESSING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr5 = 7;
	public static final int CHAIN_TRANS_tr6_FROM_QUERYING_TO_QUERYING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr6 = 8;
	
	/* triggers */
	public static final int POLLING = 0;
	public static final int TRIG_Init__Initialize = IFITEM_Init + EVT_SHIFT*InitOrUpdateProtocol.OUT_Initialize;
	public static final int TRIG_COPort__SendInterrupt = IFITEM_COPort + EVT_SHIFT*SensorProtocol.OUT_SendInterrupt;
	public static final int TRIG_COPort__ReturnStatus = IFITEM_COPort + EVT_SHIFT*SensorProtocol.OUT_ReturnStatus;
	public static final int TRIG_COPort__ReturnResult = IFITEM_COPort + EVT_SHIFT*SensorProtocol.OUT_ReturnResult;
	public static final int TRIG_CH4Port__SendInterrupt = IFITEM_CH4Port + EVT_SHIFT*SensorProtocol.OUT_SendInterrupt;
	public static final int TRIG_CH4Port__ReturnStatus = IFITEM_CH4Port + EVT_SHIFT*SensorProtocol.OUT_ReturnStatus;
	public static final int TRIG_CH4Port__ReturnResult = IFITEM_CH4Port + EVT_SHIFT*SensorProtocol.OUT_ReturnResult;
	public static final int TRIG_AirFlowPort__SendInterrupt = IFITEM_AirFlowPort + EVT_SHIFT*SensorProtocol.OUT_SendInterrupt;
	public static final int TRIG_AirFlowPort__ReturnStatus = IFITEM_AirFlowPort + EVT_SHIFT*SensorProtocol.OUT_ReturnStatus;
	public static final int TRIG_AirFlowPort__ReturnResult = IFITEM_AirFlowPort + EVT_SHIFT*SensorProtocol.OUT_ReturnResult;
	public static final int TRIG_WaterFlowPort__SendInterrupt = IFITEM_WaterFlowPort + EVT_SHIFT*SensorProtocol.OUT_SendInterrupt;
	public static final int TRIG_WaterFlowPort__ReturnStatus = IFITEM_WaterFlowPort + EVT_SHIFT*SensorProtocol.OUT_ReturnStatus;
	public static final int TRIG_WaterFlowPort__ReturnResult = IFITEM_WaterFlowPort + EVT_SHIFT*SensorProtocol.OUT_ReturnResult;
	public static final int TRIG_WaterHighPort__SendInterrupt = IFITEM_WaterHighPort + EVT_SHIFT*SensorProtocol.OUT_SendInterrupt;
	public static final int TRIG_WaterHighPort__ReturnStatus = IFITEM_WaterHighPort + EVT_SHIFT*SensorProtocol.OUT_ReturnStatus;
	public static final int TRIG_WaterHighPort__ReturnResult = IFITEM_WaterHighPort + EVT_SHIFT*SensorProtocol.OUT_ReturnResult;
	public static final int TRIG_WaterLowPort__SendInterrupt = IFITEM_WaterLowPort + EVT_SHIFT*SensorProtocol.OUT_SendInterrupt;
	public static final int TRIG_WaterLowPort__ReturnStatus = IFITEM_WaterLowPort + EVT_SHIFT*SensorProtocol.OUT_ReturnStatus;
	public static final int TRIG_WaterLowPort__ReturnResult = IFITEM_WaterLowPort + EVT_SHIFT*SensorProtocol.OUT_ReturnResult;
	public static final int TRIG_timer__timeout = IFITEM_timer + EVT_SHIFT*PTimer.OUT_timeout;
	public static final int TRIG_timer__internalTimer = IFITEM_timer + EVT_SHIFT*PTimer.OUT_internalTimer;
	public static final int TRIG_timer__internalTimeout = IFITEM_timer + EVT_SHIFT*PTimer.OUT_internalTimeout;
	
	// state names
	protected static final String stateStrings[] = {
		"<no state>",
		"<top>",
		"UNINITIALIZED",
		"QUERYING",
		"PROCESSING"
	};
	
	// history
	protected int history[] = {NO_STATE, NO_STATE, NO_STATE, NO_STATE, NO_STATE};
	
	private void setState(int new_state) {
		DebuggingService.getInstance().addActorState(this,stateStrings[new_state]);
		this.state = new_state;
	}
	
	/* Entry and Exit Codes */
	
	/* Action Codes */
	protected void action_TRANS_tr0_FROM_UNINITIALIZED_TO_QUERYING_BY_InitializeInit(InterfaceItemBase ifitem, InitializationPacket ip) {
	    this.systemStartTime = ip.systemStartTime;
	    this.COThreshold = ip.COThreshold;
	    this.CH4Threshold = ip.CH4Threshold;
	    this.airThreshold = ip.airThreshold;
	    this.actorName = "C";
	    this.controllerT = ip.controllerT;
	    this.sensorTimeToMeasure = ip.sensorTimeToMeasure;
	    this.activationsBeforePump = ip.activationsBeforePump;
	    this.maxTimeToAlarmAfterCH4 = ip.maxTimeToAlarmAfterCH4;
	    this.maxTimeToClosePumpAfterCH4 = ip.maxTimeToClosePumpAfterCH4;
	    this.pumpStatusKnown = true;
	    
	    Log("Controller initialized!");
	    
	    if (!alarmRaised) {
	    	startNewPeriod();
	    }
	}
	protected void action_TRANS_tr1_FROM_QUERYING_TO_PROCESSING_BY_timeouttimer(InterfaceItemBase ifitem) {
	    if (!alarmRaised) {
	    	timer.startTimeout(controllerT-sensorTimeToMeasure-10);
	    	COPort.RequestStatus();
	    	CH4Port.RequestStatus();
	    	AirFlowPort.RequestStatus();	
	    	WaterFlowPort.RequestStatus();
	    	if (highInterrupt) {
	    		highInterrupt = false;
	    		Log("Got high sensor!");
	    		WaterHighPort.RequestStatus();
	    	}
	    	if (lowInterrupt) {
	    		lowInterrupt = false;
	         	Log("Got low sensor!");
	    		WaterLowPort.RequestStatus();
	    	}
	    }
	    water.Water.instance().increaseWaterLevel();
	    float wLevel = water.Water.instance().getWaterLevel();
	    Log(String.format("Water level is %.2f", wLevel));
	}
	protected void action_TRANS_tr2_FROM_PROCESSING_TO_QUERYING_BY_timeouttimer(InterfaceItemBase ifitem) {
	    if (!alarmRaised) {
	    	if (remaining == 0) {
	    		//Log("All sensors' values received!");
	    	}
	    	else {
	    		Log(String.format("Timing error: sensors remaining: %d!!!", remaining));
	    	}
	    }
	    
	    if (triedDeactivatingPump) {
	    	++currentActivationsBeforePump;
	    	if (currentActivationsBeforePump == activationsBeforePump+1) {
	    	    currentActivationsBeforePump = 0;								    			
	    	    if (waterFlowStatus == WaterFlowStatus.INCREASING) {
	    	    	Log("Pump checked - safely deactivated!");
	    	    	Log(String.format("It took %d ms!", System.currentTimeMillis()-timePumpChangeRequested));
	       			pumpStatusKnown = true;
	    	    	triedDeactivatingPump = false;							    				
	    	    }									
	    	    else {
	    	    	Log("Pump checked - not deactivated!");
	    	    	raiseAlarm();
	    	    }	
	    	}
	    }
	    
	    if (triedActivatingPump) {
	    	++currentActivationsBeforePump;
	    	if (currentActivationsBeforePump == activationsBeforePump+1) {
	    		currentActivationsBeforePump = 0;	
	    		if (waterFlowStatus == WaterFlowStatus.DECREASING) {		    		
	    	    	Log("Pump checked - safely activated!");
	    	    	Log(String.format("It took %d ms!", System.currentTimeMillis()-timePumpChangeRequested));
	    	    	pumpStatusKnown = true;	    	
	    	    	triedActivatingPump = false;
	    	    }									
	    	    else {
	    	    	Log("Pump checked - not activated!");
	    	    	raiseAlarm();
	    		}											
	    	}
	    }
	    
	    if (!alarmRaised) {
	    	startNewPeriod();
	    }
	}
	protected void action_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3(InterfaceItemBase ifitem, StatusPacket st) {
	    if (st.status == SensorStatus.OK) {
	    	hadError[st.sensorID] = false;
	    	switch(st.sensorID) {
	    	    case SensorID.CO_Sensor: COPort.RequestResult(); break;
	    	    case SensorID.CH4_Sensor: CH4Port.RequestResult(); break;
	    	    case SensorID.Air_Flow_Sensor: AirFlowPort.RequestResult(); break;
	    	    case SensorID.Water_Flow_Sensor: WaterFlowPort.RequestResult(); break;
	      		case SensorID.Water_Low_Sensor: 
	    	    	if (!triedDeactivatingPump) {
	    	    	  	timePumpChangeRequested = System.currentTimeMillis();  			    
	    	   			PumpPort.DeactivatePump();
	    	   			pumpStatusKnown = false;
	    	    		triedDeactivatingPump = true;
	    	    		currentActivationsBeforePump = 1;	    		
	    	    	}
	    	    break;
	    	    case SensorID.Water_High_Sensor: 
	    
	    	    	if (!triedActivatingPump) {
	    	    		if (CH4Level >= CH4Threshold) {
	    	    			Log("Cannot activate pump, methane level too high!");
	    	    			raiseAlarm();
	    	    		}
	    	    		timePumpChangeRequested = System.currentTimeMillis();
	    	   			pumpStatusKnown = false;
	    	   			PumpPort.ActivatePump();
	    	    		triedActivatingPump = true;
	    	    		currentActivationsBeforePump = 1;	    		
	    	    	}	    	
	    	    break;
	    	}
	    }
	    else {
	    	--remaining; //not checking result since it's bad
	    	if (hadError[st.sensorID]) {
	    		raiseAlarm();
	    	}
	    	else {
	    		hadError[st.sensorID] = true;
	    	}		
	    }
	}
	protected void action_TRANS_tr4_FROM_PROCESSING_TO_PROCESSING_BY_ReturnResultCH4PortReturnResultAirFlowPortReturnResultCOPortReturnResultWaterFlowPort_tr4(InterfaceItemBase ifitem, ResultPacket res) {
	    --remaining;
	    switch(res.sensorID) {
	    	case SensorID.CH4_Sensor: 
	    		CH4Level = res.result;
	    		if (CH4Level >= CH4Threshold && pumpStatusKnown && waterFlowStatus == WaterFlowStatus.DECREASING) {
	    			Log("CH4 value critical and pump active, shutting down!");
	    			raiseAlarm();
	    		}	
	    		break;
	    	case SensorID.CO_Sensor: 
	    		if (res.result >= COThreshold) {
	    			Log("CO value critical, shutting down!");
	    			raiseAlarm();
	    		}	
	    		break;	    
	    	case SensorID.Air_Flow_Sensor: 
	    		if (res.result >= airThreshold) {
	    			Log("Air value critical, shutting down!");
	    			raiseAlarm();
	    		}	
	    		break;	    
	    	case SensorID.Water_Flow_Sensor:
	    		waterFlowStatus = res.waterFlowStatus;
	    		break;
	    }
	}
	protected void action_TRANS_tr5_FROM_PROCESSING_TO_PROCESSING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr5(InterfaceItemBase ifitem, int sensorID) {
	    switch(sensorID) {
	    	case SensorID.Water_Low_Sensor: 
	    		Log("Low sensor in PROCESSING!");
	    		WaterLowPort.RequestStatus(); 
	    		break;
	    	case SensorID.Water_High_Sensor: 
	    		Log("High sensor in PROCESSING!");
	    		WaterHighPort.RequestStatus(); 
	    		break;
	    }
	}
	protected void action_TRANS_tr6_FROM_QUERYING_TO_QUERYING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr6(InterfaceItemBase ifitem, int sensorID) {
	    switch(sensorID) {
	    	case SensorID.Water_Low_Sensor: 
	    		lowInterrupt = true; 
	    		break;
	    	case SensorID.Water_High_Sensor: 
	    		highInterrupt = true; 
	    		break;
	    }
	}
	
	/* State Switch Methods */
	/**
	 * calls exit codes while exiting from the current state to one of its
	 * parent states while remembering the history
	 * @param current__et - the current state
	 * @param to - the final parent state
	 */
	private void exitTo(int current__et, int to) {
		while (current__et!=to) {
			switch (current__et) {
				case STATE_UNINITIALIZED:
					this.history[STATE_TOP] = STATE_UNINITIALIZED;
					current__et = STATE_TOP;
					break;
				case STATE_QUERYING:
					this.history[STATE_TOP] = STATE_QUERYING;
					current__et = STATE_TOP;
					break;
				case STATE_PROCESSING:
					this.history[STATE_TOP] = STATE_PROCESSING;
					current__et = STATE_TOP;
					break;
				default:
					/* should not occur */
					break;
			}
		}
	}
	
	/**
	 * calls action, entry and exit codes along a transition chain. The generic data are cast to typed data
	 * matching the trigger of this chain. The ID of the final state is returned
	 * @param chain__et - the chain ID
	 * @param generic_data__et - the generic data pointer
	 * @return the +/- ID of the final state either with a positive sign, that indicates to execute the state's entry code, or a negative sign vice versa
	 */
	private int executeTransitionChain(int chain__et, InterfaceItemBase ifitem, Object generic_data__et) {
		switch (chain__et) {
			case Controller.CHAIN_TRANS_INITIAL_TO__UNINITIALIZED:
			{
				return STATE_UNINITIALIZED;
			}
			case Controller.CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_QUERYING_BY_InitializeInit:
			{
				InitializationPacket ip = (InitializationPacket) generic_data__et;
				action_TRANS_tr0_FROM_UNINITIALIZED_TO_QUERYING_BY_InitializeInit(ifitem, ip);
				return STATE_QUERYING;
			}
			case Controller.CHAIN_TRANS_tr1_FROM_QUERYING_TO_PROCESSING_BY_timeouttimer:
			{
				action_TRANS_tr1_FROM_QUERYING_TO_PROCESSING_BY_timeouttimer(ifitem);
				return STATE_PROCESSING;
			}
			case Controller.CHAIN_TRANS_tr2_FROM_PROCESSING_TO_QUERYING_BY_timeouttimer:
			{
				action_TRANS_tr2_FROM_PROCESSING_TO_QUERYING_BY_timeouttimer(ifitem);
				return STATE_QUERYING;
			}
			case Controller.CHAIN_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3:
			{
				StatusPacket st = (StatusPacket) generic_data__et;
				action_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3(ifitem, st);
				return STATE_PROCESSING;
			}
			case Controller.CHAIN_TRANS_tr4_FROM_PROCESSING_TO_PROCESSING_BY_ReturnResultCH4PortReturnResultAirFlowPortReturnResultCOPortReturnResultWaterFlowPort_tr4:
			{
				ResultPacket res = (ResultPacket) generic_data__et;
				action_TRANS_tr4_FROM_PROCESSING_TO_PROCESSING_BY_ReturnResultCH4PortReturnResultAirFlowPortReturnResultCOPortReturnResultWaterFlowPort_tr4(ifitem, res);
				return STATE_PROCESSING;
			}
			case Controller.CHAIN_TRANS_tr5_FROM_PROCESSING_TO_PROCESSING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr5:
			{
				int sensorID = (int) generic_data__et;
				action_TRANS_tr5_FROM_PROCESSING_TO_PROCESSING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr5(ifitem, sensorID);
				return STATE_PROCESSING;
			}
			case Controller.CHAIN_TRANS_tr6_FROM_QUERYING_TO_QUERYING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr6:
			{
				int sensorID = (int) generic_data__et;
				action_TRANS_tr6_FROM_QUERYING_TO_QUERYING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr6(ifitem, sensorID);
				return STATE_QUERYING;
			}
				default:
					/* should not occur */
					break;
		}
		return NO_STATE;
	}
	
	/**
	 * calls entry codes while entering a state's history. The ID of the final leaf state is returned
	 * @param state__et - the state which is entered
	 * @return - the ID of the final leaf state
	 */
	private int enterHistory(int state__et) {
		if (state__et >= STATE_MAX) {
			state__et =  (state__et - STATE_MAX);
		}
		while (true) {
			switch (state__et) {
				case STATE_UNINITIALIZED:
					/* in leaf state: return state id */
					return STATE_UNINITIALIZED;
				case STATE_QUERYING:
					/* in leaf state: return state id */
					return STATE_QUERYING;
				case STATE_PROCESSING:
					/* in leaf state: return state id */
					return STATE_PROCESSING;
				case STATE_TOP:
					state__et = this.history[STATE_TOP];
					break;
				default:
					/* should not occur */
					break;
			}
		}
		/* return NO_STATE; // required by CDT but detected as unreachable by JDT because of while (true) */
	}
	
	public void executeInitTransition() {
		int chain__et = Controller.CHAIN_TRANS_INITIAL_TO__UNINITIALIZED;
		int next__et = executeTransitionChain(chain__et, null, null);
		next__et = enterHistory(next__et);
		setState(next__et);
	}
	
	/* receiveEvent contains the main implementation of the FSM */
	public void receiveEventInternal(InterfaceItemBase ifitem, int localId, int evt, Object generic_data__et) {
		int trigger__et = localId + EVT_SHIFT*evt;
		int chain__et = NOT_CAUGHT;
		int catching_state__et = NO_STATE;
	
		if (!handleSystemEvent(ifitem, evt, generic_data__et)) {
			switch (getState()) {
			    case STATE_UNINITIALIZED:
			        switch(trigger__et) {
			                case TRIG_Init__Initialize:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_QUERYING_BY_InitializeInit;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_QUERYING:
			        switch(trigger__et) {
			                case TRIG_timer__timeout:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr1_FROM_QUERYING_TO_PROCESSING_BY_timeouttimer;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_WaterHighPort__SendInterrupt:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr6_FROM_QUERYING_TO_QUERYING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr6;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_WaterLowPort__SendInterrupt:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr6_FROM_QUERYING_TO_QUERYING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr6;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_PROCESSING:
			        switch(trigger__et) {
			                case TRIG_timer__timeout:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr2_FROM_PROCESSING_TO_QUERYING_BY_timeouttimer;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_COPort__ReturnStatus:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_CH4Port__ReturnStatus:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_AirFlowPort__ReturnStatus:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_WaterFlowPort__ReturnStatus:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_WaterHighPort__ReturnStatus:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_WaterLowPort__ReturnStatus:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr3_FROM_PROCESSING_TO_PROCESSING_BY_ReturnStatusCOPortReturnStatusCH4PortReturnStatusAirFlowPortReturnStatusWaterFlowPortReturnStatusWaterHighPortReturnStatusWaterLowPort_tr3;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_CH4Port__ReturnResult:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr4_FROM_PROCESSING_TO_PROCESSING_BY_ReturnResultCH4PortReturnResultAirFlowPortReturnResultCOPortReturnResultWaterFlowPort_tr4;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_AirFlowPort__ReturnResult:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr4_FROM_PROCESSING_TO_PROCESSING_BY_ReturnResultCH4PortReturnResultAirFlowPortReturnResultCOPortReturnResultWaterFlowPort_tr4;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_COPort__ReturnResult:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr4_FROM_PROCESSING_TO_PROCESSING_BY_ReturnResultCH4PortReturnResultAirFlowPortReturnResultCOPortReturnResultWaterFlowPort_tr4;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_WaterFlowPort__ReturnResult:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr4_FROM_PROCESSING_TO_PROCESSING_BY_ReturnResultCH4PortReturnResultAirFlowPortReturnResultCOPortReturnResultWaterFlowPort_tr4;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_WaterHighPort__SendInterrupt:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr5_FROM_PROCESSING_TO_PROCESSING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr5;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_WaterLowPort__SendInterrupt:
			                    {
			                        chain__et = Controller.CHAIN_TRANS_tr5_FROM_PROCESSING_TO_PROCESSING_BY_SendInterruptWaterHighPortSendInterruptWaterLowPort_tr5;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    default:
			        /* should not occur */
			        break;
			}
		}
		if (chain__et != NOT_CAUGHT) {
			exitTo(getState(), catching_state__et);
			{
				int next__et = executeTransitionChain(chain__et, ifitem, generic_data__et);
				next__et = enterHistory(next__et);
				setState(next__et);
			}
		}
	}
	public void receiveEvent(InterfaceItemBase ifitem, int evt, Object generic_data__et) {
		int localId = (ifitem==null)? 0 : ifitem.getLocalId();
		receiveEventInternal(ifitem, localId, evt, generic_data__et);
	}

};
