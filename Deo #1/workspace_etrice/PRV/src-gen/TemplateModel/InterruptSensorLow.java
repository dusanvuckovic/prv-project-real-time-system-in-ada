package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.*;
import org.eclipse.etrice.runtime.java.modelbase.*;
import org.eclipse.etrice.runtime.java.debugging.*;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;

import room.basic.service.logging.*;
import room.basic.service.timing.*;
import TemplateModel.InitOrUpdateProtocol.*;
import room.basic.service.logging.Log.*;
import room.basic.service.timing.PTimer.*;
import TemplateModel.SensorProtocol.*;



public class InterruptSensorLow extends InterruptSensor {


	//--------------------- ports

	//--------------------- saps

	//--------------------- services

	//--------------------- optional actors

	//--------------------- interface item IDs

	/*--------------------- attributes ---------------------*/

	/*--------------------- operations ---------------------*/
	public  boolean isValueCritical() {
		return (water.Water.instance().getWaterLevel() <= sensorThreshold);			
	}


	//--------------------- construction
	public InterruptSensorLow(IRTObject parent, String name) {
		super(parent, name);
		setClassName("InterruptSensorLow");

		// initialize attributes

		// own ports

		// own saps

		// own service implementations

		// sub actors

		// wiring


		/* user defined constructor body */

	}

	/* --------------------- attribute setters and getters */


	//--------------------- port getters

	//--------------------- lifecycle functions
	public void stop(){
		super.stop();
	}

	public void destroy(){
		/* user defined destructor body */
		DebuggingService.getInstance().addMessageActorDestroy(this);
		super.destroy();
	}


};
