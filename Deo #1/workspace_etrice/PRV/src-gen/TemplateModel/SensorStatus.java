package TemplateModel;

public interface SensorStatus {
	static final int TOO_SOON = 0;
	static final int MISMEASURE = 1;
	static final int OK = 2;
}
