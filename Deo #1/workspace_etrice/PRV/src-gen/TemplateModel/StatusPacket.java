package TemplateModel;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;
import java.io.Serializable;




public class StatusPacket implements Serializable {

	private static final long serialVersionUID = 2029370217L;


	/*--------------------- attributes ---------------------*/
	public  int status;
	public  String statusDescription;
	public  int sensorID;
	public  String sensorName;

	/* --------------------- attribute setters and getters */
	public void setStatus(int status) {
		 this.status = status;
	}
	public int getStatus() {
		return this.status;
	}
	public void setStatusDescription(String statusDescription) {
		 this.statusDescription = statusDescription;
	}
	public String getStatusDescription() {
		return this.statusDescription;
	}
	public void setSensorID(int sensorID) {
		 this.sensorID = sensorID;
	}
	public int getSensorID() {
		return this.sensorID;
	}
	public void setSensorName(String sensorName) {
		 this.sensorName = sensorName;
	}
	public String getSensorName() {
		return this.sensorName;
	}

	/*--------------------- operations ---------------------*/

	// default constructor
	public StatusPacket() {
		super();

		// initialize attributes
		this.setStatusDescription("");
		this.setSensorName("");

		/* user defined constructor body */
	}

	// constructor using fields
	public StatusPacket(int status, String statusDescription, int sensorID, String sensorName) {
		super();

		this.status = status;
		this.statusDescription = statusDescription;
		this.sensorID = sensorID;
		this.sensorName = sensorName;

		/* user defined constructor body */
	}

	// deep copy
	public StatusPacket deepCopy() {
		StatusPacket copy = new StatusPacket();
		copy.status = status;
		copy.statusDescription = statusDescription;
		copy.sensorID = sensorID;
		copy.sensorName = sensorName;
		return copy;
	}
};
