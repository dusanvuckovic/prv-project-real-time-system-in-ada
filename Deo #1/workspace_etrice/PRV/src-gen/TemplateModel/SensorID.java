package TemplateModel;

public interface SensorID {
	static final int CO_Sensor = 0;
	static final int CH4_Sensor = 1;
	static final int Air_Flow_Sensor = 2;
	static final int Water_High_Sensor = 3;
	static final int Water_Low_Sensor = 4;
	static final int Water_Flow_Sensor = 5;
}
