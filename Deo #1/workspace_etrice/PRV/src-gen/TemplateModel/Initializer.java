package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.*;
import org.eclipse.etrice.runtime.java.modelbase.*;
import org.eclipse.etrice.runtime.java.debugging.*;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;

import TemplateModel.InitOrUpdateProtocol.*;



public class Initializer extends ActorClassBase {


	//--------------------- ports
	protected InitOrUpdateProtocolPort COInit = null;
	protected InitOrUpdateProtocolPort CH4Init = null;
	protected InitOrUpdateProtocolPort AirFlowInit = null;
	protected InitOrUpdateProtocolPort WaterLowInit = null;
	protected InitOrUpdateProtocolPort WaterHighInit = null;
	protected InitOrUpdateProtocolPort WaterFlowInit = null;
	protected InitOrUpdateProtocolPort ControllerInit = null;
	protected InitOrUpdateProtocolPort PumpInit = null;

	//--------------------- saps

	//--------------------- services

	//--------------------- optional actors

	//--------------------- interface item IDs
	public static final int IFITEM_COInit = 1;
	public static final int IFITEM_CH4Init = 2;
	public static final int IFITEM_AirFlowInit = 3;
	public static final int IFITEM_WaterLowInit = 4;
	public static final int IFITEM_WaterHighInit = 5;
	public static final int IFITEM_WaterFlowInit = 6;
	public static final int IFITEM_ControllerInit = 7;
	public static final int IFITEM_PumpInit = 8;

	/*--------------------- attributes ---------------------*/

	/*--------------------- operations ---------------------*/


	//--------------------- construction
	public Initializer(IRTObject parent, String name) {
		super(parent, name);
		setClassName("Initializer");

		// initialize attributes

		// own ports
		COInit = new InitOrUpdateProtocolPort(this, "COInit", IFITEM_COInit);
		CH4Init = new InitOrUpdateProtocolPort(this, "CH4Init", IFITEM_CH4Init);
		AirFlowInit = new InitOrUpdateProtocolPort(this, "AirFlowInit", IFITEM_AirFlowInit);
		WaterLowInit = new InitOrUpdateProtocolPort(this, "WaterLowInit", IFITEM_WaterLowInit);
		WaterHighInit = new InitOrUpdateProtocolPort(this, "WaterHighInit", IFITEM_WaterHighInit);
		WaterFlowInit = new InitOrUpdateProtocolPort(this, "WaterFlowInit", IFITEM_WaterFlowInit);
		ControllerInit = new InitOrUpdateProtocolPort(this, "ControllerInit", IFITEM_ControllerInit);
		PumpInit = new InitOrUpdateProtocolPort(this, "PumpInit", IFITEM_PumpInit);

		// own saps

		// own service implementations

		// sub actors

		// wiring


		/* user defined constructor body */

	}

	/* --------------------- attribute setters and getters */


	//--------------------- port getters
	public InitOrUpdateProtocolPort getCOInit (){
		return this.COInit;
	}
	public InitOrUpdateProtocolPort getCH4Init (){
		return this.CH4Init;
	}
	public InitOrUpdateProtocolPort getAirFlowInit (){
		return this.AirFlowInit;
	}
	public InitOrUpdateProtocolPort getWaterLowInit (){
		return this.WaterLowInit;
	}
	public InitOrUpdateProtocolPort getWaterHighInit (){
		return this.WaterHighInit;
	}
	public InitOrUpdateProtocolPort getWaterFlowInit (){
		return this.WaterFlowInit;
	}
	public InitOrUpdateProtocolPort getControllerInit (){
		return this.ControllerInit;
	}
	public InitOrUpdateProtocolPort getPumpInit (){
		return this.PumpInit;
	}

	//--------------------- lifecycle functions
	public void stop(){
		super.stop();
	}

	public void destroy(){
		/* user defined destructor body */
		DebuggingService.getInstance().addMessageActorDestroy(this);
		super.destroy();
	}

	/* state IDs */
	public static final int STATE_DONE = 2;
	public static final int STATE_MAX = 3;
	
	/* transition chains */
	public static final int CHAIN_TRANS_INITIAL_TO__DONE = 1;
	
	/* triggers */
	public static final int POLLING = 0;
	
	// state names
	protected static final String stateStrings[] = {
		"<no state>",
		"<top>",
		"DONE"
	};
	
	// history
	protected int history[] = {NO_STATE, NO_STATE, NO_STATE};
	
	private void setState(int new_state) {
		DebuggingService.getInstance().addActorState(this,stateStrings[new_state]);
		this.state = new_state;
	}
	
	/* Entry and Exit Codes */
	
	/* Action Codes */
	protected void action_TRANS_INITIAL_TO__DONE() {
	    InitializationPacket ip = new InitializationPacket();
	    
	    //AQI
	    float airSensorLow = 0;
	    float airSensorHigh = 140;
	    
	    //ppm
	    float coSensorLow = 0;
	    float coSensorHigh = 50;
	    
	    //ppm
	    float ch4SensorLow = 0;
	    float ch4SensorHigh = 900;
	    
	    ip.COThreshold = 70; //RL: 70
	    ip.CH4Threshold = 1000; //RL: 1000, 50000 za eksplozije
	    ip.airThreshold = 150; //RL: 150
	    ip.controllerT = 240;
	    ip.sensorTimeToMeasure = 150;
	    ip.sensorPError = 0; //0.02f
	    ip.pumpErrorRate = 0; //0.5f;
	    ip.pumpWaitTime = 900;
	    ip.activationsBeforePump = 5;
	    ip.maxTimeToClosePumpAfterCH4 = 400;
	    ip.maxTimeToAlarmAfterCH4 = 900 + 300; //td + tc
	    ip.systemStartTime = System.currentTimeMillis();	    
	    
	    ControllerInit.Initialize(ip);
	    
	    ip.sensorID = SensorID.CO_Sensor;
	    ip.sensorName = "CO";
	    ip.sensorLowerBound = coSensorLow;
	    ip.sensorUpperBound = coSensorHigh;
	    COInit.Initialize(ip);
	    	    
	    ip.sensorID = SensorID.CH4_Sensor;
	    ip.sensorName = "CH4";
	    ip.sensorLowerBound = ch4SensorLow;
	    ip.sensorUpperBound = ch4SensorHigh;
	    CH4Init.Initialize(ip);
	    	    
	    ip.sensorID = SensorID.Air_Flow_Sensor;
	    ip.sensorName = "AirFlow";
	    ip.sensorLowerBound = airSensorLow;
	    ip.sensorUpperBound = airSensorHigh;
	    AirFlowInit.Initialize(ip);
	    
	    ip.sensorID = SensorID.Water_Flow_Sensor;
	    ip.sensorName = "WaterFlow";
	    WaterFlowInit.Initialize(ip);
	    
	    ip.sensorID = SensorID.Water_High_Sensor;
	    ip.sensorThreshold = water.Water.WATER_HIGH_THRESHOLD;
	    ip.sensorName = "WaterHigh";
	    WaterHighInit.Initialize(ip);
	    
	    ip.sensorID = SensorID.Water_Low_Sensor;
	    ip.sensorThreshold = water.Water.WATER_LOW_THRESHOLD;
	    ip.sensorName = "WaterLow";
	    WaterLowInit.Initialize(ip);
	    
	    PumpInit.Initialize(ip);
	}
	
	/* State Switch Methods */
	/**
	 * calls exit codes while exiting from the current state to one of its
	 * parent states while remembering the history
	 * @param current__et - the current state
	 * @param to - the final parent state
	 */
	private void exitTo(int current__et, int to) {
		while (current__et!=to) {
			switch (current__et) {
				case STATE_DONE:
					this.history[STATE_TOP] = STATE_DONE;
					current__et = STATE_TOP;
					break;
				default:
					/* should not occur */
					break;
			}
		}
	}
	
	/**
	 * calls action, entry and exit codes along a transition chain. The generic data are cast to typed data
	 * matching the trigger of this chain. The ID of the final state is returned
	 * @param chain__et - the chain ID
	 * @param generic_data__et - the generic data pointer
	 * @return the +/- ID of the final state either with a positive sign, that indicates to execute the state's entry code, or a negative sign vice versa
	 */
	private int executeTransitionChain(int chain__et, InterfaceItemBase ifitem, Object generic_data__et) {
		switch (chain__et) {
			case Initializer.CHAIN_TRANS_INITIAL_TO__DONE:
			{
				action_TRANS_INITIAL_TO__DONE();
				return STATE_DONE;
			}
				default:
					/* should not occur */
					break;
		}
		return NO_STATE;
	}
	
	/**
	 * calls entry codes while entering a state's history. The ID of the final leaf state is returned
	 * @param state__et - the state which is entered
	 * @return - the ID of the final leaf state
	 */
	private int enterHistory(int state__et) {
		if (state__et >= STATE_MAX) {
			state__et =  (state__et - STATE_MAX);
		}
		while (true) {
			switch (state__et) {
				case STATE_DONE:
					/* in leaf state: return state id */
					return STATE_DONE;
				case STATE_TOP:
					state__et = this.history[STATE_TOP];
					break;
				default:
					/* should not occur */
					break;
			}
		}
		/* return NO_STATE; // required by CDT but detected as unreachable by JDT because of while (true) */
	}
	
	public void executeInitTransition() {
		int chain__et = Initializer.CHAIN_TRANS_INITIAL_TO__DONE;
		int next__et = executeTransitionChain(chain__et, null, null);
		next__et = enterHistory(next__et);
		setState(next__et);
	}
	
	/* receiveEvent contains the main implementation of the FSM */
	public void receiveEventInternal(InterfaceItemBase ifitem, int localId, int evt, Object generic_data__et) {
		int trigger__et = localId + EVT_SHIFT*evt;
		int chain__et = NOT_CAUGHT;
		int catching_state__et = NO_STATE;
	
		if (!handleSystemEvent(ifitem, evt, generic_data__et)) {
			switch (getState()) {
			    case STATE_DONE:
			        break;
			    default:
			        /* should not occur */
			        break;
			}
		}
		if (chain__et != NOT_CAUGHT) {
			exitTo(getState(), catching_state__et);
			{
				int next__et = executeTransitionChain(chain__et, ifitem, generic_data__et);
				next__et = enterHistory(next__et);
				setState(next__et);
			}
		}
	}
	public void receiveEvent(InterfaceItemBase ifitem, int evt, Object generic_data__et) {
		int localId = (ifitem==null)? 0 : ifitem.getLocalId();
		receiveEventInternal(ifitem, localId, evt, generic_data__et);
	}

};
