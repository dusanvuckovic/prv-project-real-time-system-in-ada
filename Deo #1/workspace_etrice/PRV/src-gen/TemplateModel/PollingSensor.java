package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.*;
import org.eclipse.etrice.runtime.java.modelbase.*;
import org.eclipse.etrice.runtime.java.debugging.*;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;

import room.basic.service.logging.*;
import room.basic.service.timing.*;
import TemplateModel.InitOrUpdateProtocol.*;
import room.basic.service.logging.Log.*;
import room.basic.service.timing.PTimer.*;
import TemplateModel.SensorProtocol.*;



public class PollingSensor extends Sensor {


	//--------------------- ports

	//--------------------- saps

	//--------------------- services

	//--------------------- optional actors

	//--------------------- interface item IDs

	/*--------------------- attributes ---------------------*/
	public  float sensorLowerBound;
	public  float sensorUpperBound;

	/*--------------------- operations ---------------------*/
	public  void ReturnResultOperation() {
		float value = (float)(sensorLowerBound + Math.random()*(sensorUpperBound-sensorLowerBound));
		ResultPacket res = new ResultPacket(value, sensorID, actorName, WaterFlowStatus.NO_STATUS);
		OutPort.ReturnResult(res);				
	}


	//--------------------- construction
	public PollingSensor(IRTObject parent, String name) {
		super(parent, name);
		setClassName("PollingSensor");

		// initialize attributes
		this.setSensorLowerBound(0f);
		this.setSensorUpperBound(0f);

		// own ports

		// own saps

		// own service implementations

		// sub actors

		// wiring


		/* user defined constructor body */

	}

	/* --------------------- attribute setters and getters */
	public void setSensorLowerBound(float sensorLowerBound) {
		 this.sensorLowerBound = sensorLowerBound;
	}
	public float getSensorLowerBound() {
		return this.sensorLowerBound;
	}
	public void setSensorUpperBound(float sensorUpperBound) {
		 this.sensorUpperBound = sensorUpperBound;
	}
	public float getSensorUpperBound() {
		return this.sensorUpperBound;
	}


	//--------------------- port getters

	//--------------------- lifecycle functions
	public void stop(){
		super.stop();
	}

	public void destroy(){
		/* user defined destructor body */
		DebuggingService.getInstance().addMessageActorDestroy(this);
		super.destroy();
	}

	/* state IDs */
	public static final int STATE_TOO_SOON = 7;
	public static final int STATE_MAX = 8;
	
	/* transition chains */
	public static final int CHAIN_TRANS_tr5_FROM_MEASURING_TO_TOO_SOON_BY_RequestStatusOutPort = 1;
	public static final int CHAIN_TRANS_tr6_FROM_MEASURING_TO_TOO_SOON_BY_RequestResultOutPort = 2;
	public static final int CHAIN_TRANS_tr7_FROM_TOO_SOON_TO_MEASURING_BY_StartMeasurementOutPort = 3;
	public static final int CHAIN_TRANS_tr12_FROM_MEASURED_TO_MEASURED_BY_RequestResultOutPort_tr12 = 4;
	public static final int CHAIN_TRANS_tr13_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestResultOutPort_tr13 = 5;
	public static final int CHAIN_TRANS_tr14_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestStatusOutPort_tr14 = 6;
	public static final int CHAIN_TRANS_tr15_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestResultOutPort_tr15 = 7;
	public static final int CHAIN_TRANS_INITIAL_TO__UNINITIALIZED = 8;
	public static final int CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_IDLE_BY_InitializeInitialize = 9;
	public static final int CHAIN_TRANS_tr1_FROM_IDLE_TO_MEASURING_BY_StartMeasurementOutPort = 10;
	public static final int CHAIN_TRANS_tr2_FROM_MEASURING_TO_cp0_BY_timeouttimer = 11;
	public static final int CHAIN_TRANS_tr8_FROM_MEASURED_TO_MEASURING_BY_StartMeasurementOutPort = 12;
	public static final int CHAIN_TRANS_tr9_FROM_MISMEASURED_TO_MEASURING_BY_StartMeasurementOutPort = 13;
	public static final int CHAIN_TRANS_tr10_FROM_MEASURED_TO_MEASURED_BY_RequestStatusOutPort_tr10 = 14;
	public static final int CHAIN_TRANS_tr11_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestStatusOutPort_tr11 = 15;
	
	/* triggers */
	public static final int POLLING = 0;
	
	// state names
	protected static final String stateStrings[] = {
		"<no state>",
		"<top>",
		"UNINITIALIZED",
		"IDLE",
		"MEASURING",
		"MEASURED",
		"MISMEASURED",
		"TOO_SOON"
	};
	
	// history
	protected int history[] = {NO_STATE, NO_STATE, NO_STATE, NO_STATE, NO_STATE, NO_STATE, NO_STATE, NO_STATE};
	
	private void setState(int new_state) {
		DebuggingService.getInstance().addActorState(this,stateStrings[new_state]);
		this.state = new_state;
	}
	
	/* Entry and Exit Codes */
	
	/* Action Codes */
	protected void action_TRANS_tr5_FROM_MEASURING_TO_TOO_SOON_BY_RequestStatusOutPort(InterfaceItemBase ifitem) {
	    Log("TOO_SOON! - req.status");
	}
	protected void action_TRANS_tr6_FROM_MEASURING_TO_TOO_SOON_BY_RequestResultOutPort(InterfaceItemBase ifitem) {
	    Log("TOO_SOON! - req.result");
	}
	protected void action_TRANS_tr12_FROM_MEASURED_TO_MEASURED_BY_RequestResultOutPort_tr12(InterfaceItemBase ifitem) {
	    ReturnResultOperation();
	}
	protected void action_TRANS_tr13_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestResultOutPort_tr13(InterfaceItemBase ifitem) {
	    ReturnResultOperation();
	}
	protected void action_TRANS_tr14_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestStatusOutPort_tr14(InterfaceItemBase ifitem) {
	    StatusPacket st = new StatusPacket(SensorStatus.TOO_SOON, "TOO SOON", sensorID, actorName);
	    OutPort.ReturnStatus(st);
	}
	protected void action_TRANS_tr15_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestResultOutPort_tr15(InterfaceItemBase ifitem) {
	    ReturnResultOperation();
	}
	protected void action_TRANS_tr0_FROM_UNINITIALIZED_TO_IDLE_BY_InitializeInitialize(InterfaceItemBase ifitem, InitializationPacket ip) {
	    this.actorName = ip.sensorName;
	    this.sensorID = ip.sensorID;
	    this.systemStartTime = ip.systemStartTime;
	    this.sensorPError = ip.sensorPError;
	    this.sensorTimeToMeasure = ip.sensorTimeToMeasure;
	    this.sensorLowerBound = ip.sensorLowerBound;
	    this.sensorUpperBound = ip.sensorUpperBound;
	}
	
	/* State Switch Methods */
	/**
	 * calls exit codes while exiting from the current state to one of its
	 * parent states while remembering the history
	 * @param current__et - the current state
	 * @param to - the final parent state
	 */
	private void exitTo(int current__et, int to) {
		while (current__et!=to) {
			switch (current__et) {
				case STATE_TOO_SOON:
					this.history[STATE_TOP] = STATE_TOO_SOON;
					current__et = STATE_TOP;
					break;
				case STATE_UNINITIALIZED:
					this.history[STATE_TOP] = STATE_UNINITIALIZED;
					current__et = STATE_TOP;
					break;
				case STATE_IDLE:
					this.history[STATE_TOP] = STATE_IDLE;
					current__et = STATE_TOP;
					break;
				case STATE_MEASURING:
					this.history[STATE_TOP] = STATE_MEASURING;
					current__et = STATE_TOP;
					break;
				case STATE_MEASURED:
					this.history[STATE_TOP] = STATE_MEASURED;
					current__et = STATE_TOP;
					break;
				case STATE_MISMEASURED:
					this.history[STATE_TOP] = STATE_MISMEASURED;
					current__et = STATE_TOP;
					break;
				default:
					/* should not occur */
					break;
			}
		}
	}
	
	/**
	 * calls action, entry and exit codes along a transition chain. The generic data are cast to typed data
	 * matching the trigger of this chain. The ID of the final state is returned
	 * @param chain__et - the chain ID
	 * @param generic_data__et - the generic data pointer
	 * @return the +/- ID of the final state either with a positive sign, that indicates to execute the state's entry code, or a negative sign vice versa
	 */
	private int executeTransitionChain(int chain__et, InterfaceItemBase ifitem, Object generic_data__et) {
		switch (chain__et) {
			case PollingSensor.CHAIN_TRANS_tr5_FROM_MEASURING_TO_TOO_SOON_BY_RequestStatusOutPort:
			{
				action_TRANS_tr5_FROM_MEASURING_TO_TOO_SOON_BY_RequestStatusOutPort(ifitem);
				return STATE_TOO_SOON;
			}
			case PollingSensor.CHAIN_TRANS_tr6_FROM_MEASURING_TO_TOO_SOON_BY_RequestResultOutPort:
			{
				action_TRANS_tr6_FROM_MEASURING_TO_TOO_SOON_BY_RequestResultOutPort(ifitem);
				return STATE_TOO_SOON;
			}
			case PollingSensor.CHAIN_TRANS_tr7_FROM_TOO_SOON_TO_MEASURING_BY_StartMeasurementOutPort:
			{
				return STATE_MEASURING;
			}
			case PollingSensor.CHAIN_TRANS_tr12_FROM_MEASURED_TO_MEASURED_BY_RequestResultOutPort_tr12:
			{
				action_TRANS_tr12_FROM_MEASURED_TO_MEASURED_BY_RequestResultOutPort_tr12(ifitem);
				return STATE_MEASURED;
			}
			case PollingSensor.CHAIN_TRANS_tr13_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestResultOutPort_tr13:
			{
				action_TRANS_tr13_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestResultOutPort_tr13(ifitem);
				return STATE_MISMEASURED;
			}
			case PollingSensor.CHAIN_TRANS_tr14_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestStatusOutPort_tr14:
			{
				action_TRANS_tr14_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestStatusOutPort_tr14(ifitem);
				return STATE_TOO_SOON;
			}
			case PollingSensor.CHAIN_TRANS_tr15_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestResultOutPort_tr15:
			{
				action_TRANS_tr15_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestResultOutPort_tr15(ifitem);
				return STATE_TOO_SOON;
			}
			case PollingSensor.CHAIN_TRANS_INITIAL_TO__UNINITIALIZED:
			{
				return STATE_UNINITIALIZED;
			}
			case PollingSensor.CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_IDLE_BY_InitializeInitialize:
			{
				InitializationPacket ip = (InitializationPacket) generic_data__et;
				action_TRANS_tr0_FROM_UNINITIALIZED_TO_IDLE_BY_InitializeInitialize(ifitem, ip);
				return STATE_IDLE;
			}
			case PollingSensor.CHAIN_TRANS_tr1_FROM_IDLE_TO_MEASURING_BY_StartMeasurementOutPort:
			{
				return STATE_MEASURING;
			}
			case PollingSensor.CHAIN_TRANS_tr2_FROM_MEASURING_TO_cp0_BY_timeouttimer:
			{
				if (sensorPError <= Math.random()) {
				return STATE_MEASURED;}
				else {
				action_TRANS_tr3_FROM_cp0_TO_MISMEASURED(ifitem);
				return STATE_MISMEASURED;}
			}
			case PollingSensor.CHAIN_TRANS_tr8_FROM_MEASURED_TO_MEASURING_BY_StartMeasurementOutPort:
			{
				return STATE_MEASURING;
			}
			case PollingSensor.CHAIN_TRANS_tr9_FROM_MISMEASURED_TO_MEASURING_BY_StartMeasurementOutPort:
			{
				return STATE_MEASURING;
			}
			case PollingSensor.CHAIN_TRANS_tr10_FROM_MEASURED_TO_MEASURED_BY_RequestStatusOutPort_tr10:
			{
				action_TRANS_tr10_FROM_MEASURED_TO_MEASURED_BY_RequestStatusOutPort_tr10(ifitem);
				return STATE_MEASURED;
			}
			case PollingSensor.CHAIN_TRANS_tr11_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestStatusOutPort_tr11:
			{
				action_TRANS_tr11_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestStatusOutPort_tr11(ifitem);
				return STATE_MISMEASURED;
			}
				default:
					/* should not occur */
					break;
		}
		return NO_STATE;
	}
	
	/**
	 * calls entry codes while entering a state's history. The ID of the final leaf state is returned
	 * @param state__et - the state which is entered
	 * @return - the ID of the final leaf state
	 */
	private int enterHistory(int state__et) {
		boolean skip_entry__et = false;
		if (state__et >= STATE_MAX) {
			state__et =  (state__et - STATE_MAX);
			skip_entry__et = true;
		}
		while (true) {
			switch (state__et) {
				case STATE_TOO_SOON:
					/* in leaf state: return state id */
					return STATE_TOO_SOON;
				case STATE_UNINITIALIZED:
					/* in leaf state: return state id */
					return STATE_UNINITIALIZED;
				case STATE_IDLE:
					/* in leaf state: return state id */
					return STATE_IDLE;
				case STATE_MEASURING:
					if (!(skip_entry__et)) entry_MEASURING();
					/* in leaf state: return state id */
					return STATE_MEASURING;
				case STATE_MEASURED:
					/* in leaf state: return state id */
					return STATE_MEASURED;
				case STATE_MISMEASURED:
					/* in leaf state: return state id */
					return STATE_MISMEASURED;
				case STATE_TOP:
					state__et = this.history[STATE_TOP];
					break;
				default:
					/* should not occur */
					break;
			}
			skip_entry__et = false;
		}
		/* return NO_STATE; // required by CDT but detected as unreachable by JDT because of while (true) */
	}
	
	public void executeInitTransition() {
		int chain__et = PollingSensor.CHAIN_TRANS_INITIAL_TO__UNINITIALIZED;
		int next__et = executeTransitionChain(chain__et, null, null);
		next__et = enterHistory(next__et);
		setState(next__et);
	}
	
	/* receiveEvent contains the main implementation of the FSM */
	public void receiveEventInternal(InterfaceItemBase ifitem, int localId, int evt, Object generic_data__et) {
		int trigger__et = localId + EVT_SHIFT*evt;
		int chain__et = NOT_CAUGHT;
		int catching_state__et = NO_STATE;
	
		if (!handleSystemEvent(ifitem, evt, generic_data__et)) {
			switch (getState()) {
			    case STATE_TOO_SOON:
			        switch(trigger__et) {
			                case TRIG_OutPort__StartMeasurement:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr7_FROM_TOO_SOON_TO_MEASURING_BY_StartMeasurementOutPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_OutPort__RequestStatus:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr14_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestStatusOutPort_tr14;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_OutPort__RequestResult:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr15_FROM_TOO_SOON_TO_TOO_SOON_BY_RequestResultOutPort_tr15;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_UNINITIALIZED:
			        switch(trigger__et) {
			                case TRIG_Initialize__Initialize:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr0_FROM_UNINITIALIZED_TO_IDLE_BY_InitializeInitialize;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_IDLE:
			        switch(trigger__et) {
			                case TRIG_OutPort__StartMeasurement:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr1_FROM_IDLE_TO_MEASURING_BY_StartMeasurementOutPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_MEASURING:
			        switch(trigger__et) {
			                case TRIG_OutPort__RequestStatus:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr5_FROM_MEASURING_TO_TOO_SOON_BY_RequestStatusOutPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_OutPort__RequestResult:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr6_FROM_MEASURING_TO_TOO_SOON_BY_RequestResultOutPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_timer__timeout:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr2_FROM_MEASURING_TO_cp0_BY_timeouttimer;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_MEASURED:
			        switch(trigger__et) {
			                case TRIG_OutPort__RequestResult:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr12_FROM_MEASURED_TO_MEASURED_BY_RequestResultOutPort_tr12;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_OutPort__StartMeasurement:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr8_FROM_MEASURED_TO_MEASURING_BY_StartMeasurementOutPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_OutPort__RequestStatus:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr10_FROM_MEASURED_TO_MEASURED_BY_RequestStatusOutPort_tr10;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    case STATE_MISMEASURED:
			        switch(trigger__et) {
			                case TRIG_OutPort__RequestResult:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr13_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestResultOutPort_tr13;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_OutPort__StartMeasurement:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr9_FROM_MISMEASURED_TO_MEASURING_BY_StartMeasurementOutPort;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                case TRIG_OutPort__RequestStatus:
			                    {
			                        chain__et = PollingSensor.CHAIN_TRANS_tr11_FROM_MISMEASURED_TO_MISMEASURED_BY_RequestStatusOutPort_tr11;
			                        catching_state__et = STATE_TOP;
			                    }
			                break;
			                default:
			                    /* should not occur */
			                    break;
			        }
			        break;
			    default:
			        /* should not occur */
			        break;
			}
		}
		if (chain__et != NOT_CAUGHT) {
			exitTo(getState(), catching_state__et);
			{
				int next__et = executeTransitionChain(chain__et, ifitem, generic_data__et);
				next__et = enterHistory(next__et);
				setState(next__et);
			}
		}
	}
	public void receiveEvent(InterfaceItemBase ifitem, int evt, Object generic_data__et) {
		int localId = (ifitem==null)? 0 : ifitem.getLocalId();
		receiveEventInternal(ifitem, localId, evt, generic_data__et);
	}

};
