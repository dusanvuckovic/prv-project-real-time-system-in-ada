package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.Message;
import org.eclipse.etrice.runtime.java.modelbase.EventMessage;
import org.eclipse.etrice.runtime.java.modelbase.EventWithDataMessage;
import org.eclipse.etrice.runtime.java.modelbase.IInterfaceItemOwner;
import org.eclipse.etrice.runtime.java.modelbase.InterfaceItemBase;
import org.eclipse.etrice.runtime.java.modelbase.PortBase;
import org.eclipse.etrice.runtime.java.modelbase.ReplicatedPortBase;
import org.eclipse.etrice.runtime.java.debugging.DebuggingService;
import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;



public class SensorProtocol {
	// message IDs
	public static final int MSG_MIN = 0;
	public static final int OUT_SendInterrupt = 1;
	public static final int OUT_ReturnStatus = 2;
	public static final int OUT_ReturnResult = 3;
	public static final int IN_StartMeasurement = 4;
	public static final int IN_RequestStatus = 5;
	public static final int IN_RequestResult = 6;
	public static final int MSG_MAX = 7;


	private static String messageStrings[] = {"MIN", "SendInterrupt","ReturnStatus","ReturnResult", "StartMeasurement","RequestStatus","RequestResult","MAX"};

	public String getMessageString(int msg_id) {
		if (msg_id<MSG_MIN || msg_id>MSG_MAX+1){
			// id out of range
			return "Message ID out of range";
		}
		else{
			return messageStrings[msg_id];
		}
	}

	
	// port class
	static public class SensorProtocolPort extends PortBase {
		// constructors
		public SensorProtocolPort(IInterfaceItemOwner actor, String name, int localId) {
			this(actor, name, localId, 0);
		}
		public SensorProtocolPort(IInterfaceItemOwner actor, String name, int localId, int idx) {
			super(actor, name, localId, idx);
			DebuggingService.getInstance().addPortInstance(this);
		}
	
		public void destroy() {
			DebuggingService.getInstance().removePortInstance(this);
			super.destroy();
		}
	
		@Override
		public void receive(Message m) {
			if (!(m instanceof EventMessage))
				return;
			EventMessage msg = (EventMessage) m;
			if (0 < msg.getEvtId() && msg.getEvtId() < MSG_MAX) {
				DebuggingService.getInstance().addMessageAsyncIn(getPeerAddress(), getAddress(), messageStrings[msg.getEvtId()]);
				if (msg instanceof EventWithDataMessage)
					getActor().receiveEvent(this, msg.getEvtId(), ((EventWithDataMessage)msg).getData());
				else
					getActor().receiveEvent(this, msg.getEvtId(), null);
			}
	}
	
	
		// sent messages
		public void SendInterrupt(int sensorID) {
			DebuggingService.getInstance().addMessageAsyncOut(getAddress(), getPeerAddress(), messageStrings[OUT_SendInterrupt]);
			if (getPeerAddress()!=null)
				getPeerMsgReceiver().receive(new EventWithDataMessage(getPeerAddress(), OUT_SendInterrupt, sensorID));
		}
		public void ReturnStatus(StatusPacket st) {
			DebuggingService.getInstance().addMessageAsyncOut(getAddress(), getPeerAddress(), messageStrings[OUT_ReturnStatus]);
			if (getPeerAddress()!=null)
				getPeerMsgReceiver().receive(new EventWithDataMessage(getPeerAddress(), OUT_ReturnStatus, st.deepCopy()));
		}
		public void ReturnStatus(int status, String statusDescription, int sensorID, String sensorName) {
			ReturnStatus(new StatusPacket(status, statusDescription, sensorID, sensorName));
		}
		public void ReturnResult(ResultPacket res) {
			DebuggingService.getInstance().addMessageAsyncOut(getAddress(), getPeerAddress(), messageStrings[OUT_ReturnResult]);
			if (getPeerAddress()!=null)
				getPeerMsgReceiver().receive(new EventWithDataMessage(getPeerAddress(), OUT_ReturnResult, res.deepCopy()));
		}
		public void ReturnResult(float result, int sensorID, String sensorName, int waterFlowStatus) {
			ReturnResult(new ResultPacket(result, sensorID, sensorName, waterFlowStatus));
		}
	}
	
	// replicated port class
	static public class SensorProtocolReplPort extends ReplicatedPortBase {
	
		public SensorProtocolReplPort(IInterfaceItemOwner actor, String name, int localId) {
			super(actor, name, localId);
		}
	
		public int getReplication() {
			return getNInterfaceItems();
		}
	
		public int getIndexOf(InterfaceItemBase ifitem){
				return ifitem.getIdx();
		}
	
		public SensorProtocolPort get(int idx) {
			return (SensorProtocolPort) getInterfaceItem(idx);
		}
	
		protected InterfaceItemBase createInterfaceItem(IInterfaceItemOwner rcv, String name, int lid, int idx) {
			return new SensorProtocolPort(rcv, name, lid, idx);
		}
	
		// outgoing messages
		public void SendInterrupt(int sensorID){
			for (InterfaceItemBase item : getItems()) {
				((SensorProtocolPort)item).SendInterrupt( sensorID);
			}
		}
		public void ReturnStatus(StatusPacket st){
			for (InterfaceItemBase item : getItems()) {
				((SensorProtocolPort)item).ReturnStatus( st);
			}
		}
		public void ReturnResult(ResultPacket res){
			for (InterfaceItemBase item : getItems()) {
				((SensorProtocolPort)item).ReturnResult( res);
			}
		}
	}
	
	
	// port class
	static public class SensorProtocolConjPort extends PortBase {
		// constructors
		public SensorProtocolConjPort(IInterfaceItemOwner actor, String name, int localId) {
			this(actor, name, localId, 0);
		}
		public SensorProtocolConjPort(IInterfaceItemOwner actor, String name, int localId, int idx) {
			super(actor, name, localId, idx);
			DebuggingService.getInstance().addPortInstance(this);
		}
	
		public void destroy() {
			DebuggingService.getInstance().removePortInstance(this);
			super.destroy();
		}
	
		@Override
		public void receive(Message m) {
			if (!(m instanceof EventMessage))
				return;
			EventMessage msg = (EventMessage) m;
			if (0 < msg.getEvtId() && msg.getEvtId() < MSG_MAX) {
				DebuggingService.getInstance().addMessageAsyncIn(getPeerAddress(), getAddress(), messageStrings[msg.getEvtId()]);
				if (msg instanceof EventWithDataMessage)
					getActor().receiveEvent(this, msg.getEvtId(), ((EventWithDataMessage)msg).getData());
				else
					getActor().receiveEvent(this, msg.getEvtId(), null);
			}
	}
	
	
		// sent messages
		public void StartMeasurement() {
			DebuggingService.getInstance().addMessageAsyncOut(getAddress(), getPeerAddress(), messageStrings[IN_StartMeasurement]);
			if (getPeerAddress()!=null)
				getPeerMsgReceiver().receive(new EventMessage(getPeerAddress(), IN_StartMeasurement));
		}
		public void RequestStatus() {
			DebuggingService.getInstance().addMessageAsyncOut(getAddress(), getPeerAddress(), messageStrings[IN_RequestStatus]);
			if (getPeerAddress()!=null)
				getPeerMsgReceiver().receive(new EventMessage(getPeerAddress(), IN_RequestStatus));
		}
		public void RequestResult() {
			DebuggingService.getInstance().addMessageAsyncOut(getAddress(), getPeerAddress(), messageStrings[IN_RequestResult]);
			if (getPeerAddress()!=null)
				getPeerMsgReceiver().receive(new EventMessage(getPeerAddress(), IN_RequestResult));
		}
	}
	
	// replicated port class
	static public class SensorProtocolConjReplPort extends ReplicatedPortBase {
	
		public SensorProtocolConjReplPort(IInterfaceItemOwner actor, String name, int localId) {
			super(actor, name, localId);
		}
	
		public int getReplication() {
			return getNInterfaceItems();
		}
	
		public int getIndexOf(InterfaceItemBase ifitem){
				return ifitem.getIdx();
		}
	
		public SensorProtocolConjPort get(int idx) {
			return (SensorProtocolConjPort) getInterfaceItem(idx);
		}
	
		protected InterfaceItemBase createInterfaceItem(IInterfaceItemOwner rcv, String name, int lid, int idx) {
			return new SensorProtocolConjPort(rcv, name, lid, idx);
		}
	
		// incoming messages
		public void StartMeasurement(){
			for (InterfaceItemBase item : getItems()) {
				((SensorProtocolConjPort)item).StartMeasurement();
			}
		}
		public void RequestStatus(){
			for (InterfaceItemBase item : getItems()) {
				((SensorProtocolConjPort)item).RequestStatus();
			}
		}
		public void RequestResult(){
			for (InterfaceItemBase item : getItems()) {
				((SensorProtocolConjPort)item).RequestResult();
			}
		}
	}
	
}
