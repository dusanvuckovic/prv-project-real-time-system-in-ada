package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.*;
import org.eclipse.etrice.runtime.java.modelbase.*;
import org.eclipse.etrice.runtime.java.debugging.*;

import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;

import room.basic.service.logging.*;
import room.basic.service.timing.*;
import TemplateModel.InitOrUpdateProtocol.*;
import room.basic.service.logging.Log.*;
import room.basic.service.timing.PTimer.*;
import TemplateModel.PumpProtocol.*;
import TemplateModel.SensorProtocol.*;



public class TopActor extends ActorClassBase {


	//--------------------- ports

	//--------------------- saps
	protected LogConjPort logger = null;

	//--------------------- services

	//--------------------- optional actors

	//--------------------- interface item IDs
	public static final int IFITEM_logger = 1;

	/*--------------------- attributes ---------------------*/

	/*--------------------- operations ---------------------*/


	//--------------------- construction
	public TopActor(IRTObject parent, String name) {
		super(parent, name);
		setClassName("TopActor");

		// initialize attributes

		// own ports

		// own saps
		logger = new LogConjPort(this, "logger", IFITEM_logger, 0);

		// own service implementations

		// sub actors
		DebuggingService.getInstance().addMessageActorCreate(this, "initializer");
		new Initializer(this, "initializer");
		DebuggingService.getInstance().addMessageActorCreate(this, "controller");
		new Controller(this, "controller");
		DebuggingService.getInstance().addMessageActorCreate(this, "co");
		new PollingSensor(this, "co");
		DebuggingService.getInstance().addMessageActorCreate(this, "ch4");
		new PollingSensor(this, "ch4");
		DebuggingService.getInstance().addMessageActorCreate(this, "air");
		new PollingSensor(this, "air");
		DebuggingService.getInstance().addMessageActorCreate(this, "waterFlow");
		new WaterPollingSensor(this, "waterFlow");
		DebuggingService.getInstance().addMessageActorCreate(this, "waterHigh");
		new InterruptSensorHigh(this, "waterHigh");
		DebuggingService.getInstance().addMessageActorCreate(this, "waterLow");
		new InterruptSensorLow(this, "waterLow");
		DebuggingService.getInstance().addMessageActorCreate(this, "pump");
		new Pump(this, "pump");

		// wiring
		InterfaceItemBase.connect(this, "initializer/ControllerInit", "controller/Init");
		InterfaceItemBase.connect(this, "initializer/COInit", "co/Initialize");
		InterfaceItemBase.connect(this, "controller/COPort", "co/OutPort");
		InterfaceItemBase.connect(this, "controller/CH4Port", "ch4/OutPort");
		InterfaceItemBase.connect(this, "controller/AirFlowPort", "air/OutPort");
		InterfaceItemBase.connect(this, "initializer/CH4Init", "ch4/Initialize");
		InterfaceItemBase.connect(this, "initializer/AirFlowInit", "air/Initialize");
		InterfaceItemBase.connect(this, "initializer/WaterFlowInit", "waterFlow/Initialize");
		InterfaceItemBase.connect(this, "waterFlow/OutPort", "controller/WaterFlowPort");
		InterfaceItemBase.connect(this, "waterHigh/Initialize", "initializer/WaterHighInit");
		InterfaceItemBase.connect(this, "waterLow/Initialize", "initializer/WaterLowInit");
		InterfaceItemBase.connect(this, "waterHigh/OutPort", "controller/WaterHighPort");
		InterfaceItemBase.connect(this, "waterLow/OutPort", "controller/WaterLowPort");
		InterfaceItemBase.connect(this, "initializer/PumpInit", "pump/InitPort");
		InterfaceItemBase.connect(this, "pump/PumpPort", "controller/PumpPort");


		/* user defined constructor body */

	}

	/* --------------------- attribute setters and getters */


	//--------------------- port getters
	public LogConjPort getLogger (){
		return this.logger;
	}

	//--------------------- lifecycle functions
	public void stop(){
		super.stop();
	}

	public void destroy(){
		/* user defined destructor body */
		DebuggingService.getInstance().addMessageActorDestroy(this);
		super.destroy();
	}

	/* state IDs */
	public static final int STATE_helloState = 2;
	public static final int STATE_MAX = 3;
	
	/* transition chains */
	public static final int CHAIN_TRANS_INITIAL_TO__helloState = 1;
	
	/* triggers */
	public static final int POLLING = 0;
	
	// state names
	protected static final String stateStrings[] = {
		"<no state>",
		"<top>",
		"helloState"
	};
	
	// history
	protected int history[] = {NO_STATE, NO_STATE, NO_STATE};
	
	private void setState(int new_state) {
		DebuggingService.getInstance().addActorState(this,stateStrings[new_state]);
		this.state = new_state;
	}
	
	/* Entry and Exit Codes */
	protected void entry_helloState() {
		System.out.println("### Hello World! ###");
		logger.open("test.txt");
		logger.log(3, "mylog");
	}
	
	/* Action Codes */
	
	/* State Switch Methods */
	/**
	 * calls exit codes while exiting from the current state to one of its
	 * parent states while remembering the history
	 * @param current__et - the current state
	 * @param to - the final parent state
	 */
	private void exitTo(int current__et, int to) {
		while (current__et!=to) {
			switch (current__et) {
				case STATE_helloState:
					this.history[STATE_TOP] = STATE_helloState;
					current__et = STATE_TOP;
					break;
				default:
					/* should not occur */
					break;
			}
		}
	}
	
	/**
	 * calls action, entry and exit codes along a transition chain. The generic data are cast to typed data
	 * matching the trigger of this chain. The ID of the final state is returned
	 * @param chain__et - the chain ID
	 * @param generic_data__et - the generic data pointer
	 * @return the +/- ID of the final state either with a positive sign, that indicates to execute the state's entry code, or a negative sign vice versa
	 */
	private int executeTransitionChain(int chain__et, InterfaceItemBase ifitem, Object generic_data__et) {
		switch (chain__et) {
			case TopActor.CHAIN_TRANS_INITIAL_TO__helloState:
			{
				return STATE_helloState;
			}
				default:
					/* should not occur */
					break;
		}
		return NO_STATE;
	}
	
	/**
	 * calls entry codes while entering a state's history. The ID of the final leaf state is returned
	 * @param state__et - the state which is entered
	 * @return - the ID of the final leaf state
	 */
	private int enterHistory(int state__et) {
		boolean skip_entry__et = false;
		if (state__et >= STATE_MAX) {
			state__et =  (state__et - STATE_MAX);
			skip_entry__et = true;
		}
		while (true) {
			switch (state__et) {
				case STATE_helloState:
					if (!(skip_entry__et)) entry_helloState();
					/* in leaf state: return state id */
					return STATE_helloState;
				case STATE_TOP:
					state__et = this.history[STATE_TOP];
					break;
				default:
					/* should not occur */
					break;
			}
			skip_entry__et = false;
		}
		/* return NO_STATE; // required by CDT but detected as unreachable by JDT because of while (true) */
	}
	
	public void executeInitTransition() {
		int chain__et = TopActor.CHAIN_TRANS_INITIAL_TO__helloState;
		int next__et = executeTransitionChain(chain__et, null, null);
		next__et = enterHistory(next__et);
		setState(next__et);
	}
	
	/* receiveEvent contains the main implementation of the FSM */
	public void receiveEventInternal(InterfaceItemBase ifitem, int localId, int evt, Object generic_data__et) {
		int trigger__et = localId + EVT_SHIFT*evt;
		int chain__et = NOT_CAUGHT;
		int catching_state__et = NO_STATE;
	
		if (!handleSystemEvent(ifitem, evt, generic_data__et)) {
			switch (getState()) {
			    case STATE_helloState:
			        break;
			    default:
			        /* should not occur */
			        break;
			}
		}
		if (chain__et != NOT_CAUGHT) {
			exitTo(getState(), catching_state__et);
			{
				int next__et = executeTransitionChain(chain__et, ifitem, generic_data__et);
				next__et = enterHistory(next__et);
				setState(next__et);
			}
		}
	}
	public void receiveEvent(InterfaceItemBase ifitem, int evt, Object generic_data__et) {
		int localId = (ifitem==null)? 0 : ifitem.getLocalId();
		receiveEventInternal(ifitem, localId, evt, generic_data__et);
	}

};
