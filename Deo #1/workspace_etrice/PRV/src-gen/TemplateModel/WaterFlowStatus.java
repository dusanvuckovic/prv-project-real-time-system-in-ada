package TemplateModel;

public interface WaterFlowStatus {
	static final int NO_STATUS = 0;
	static final int INCREASING = 1;
	static final int DECREASING = 2;
}
