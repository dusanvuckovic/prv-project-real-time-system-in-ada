package TemplateModel;

import org.eclipse.etrice.runtime.java.messaging.Message;
import org.eclipse.etrice.runtime.java.modelbase.EventMessage;
import org.eclipse.etrice.runtime.java.modelbase.EventWithDataMessage;
import org.eclipse.etrice.runtime.java.modelbase.IInterfaceItemOwner;
import org.eclipse.etrice.runtime.java.modelbase.InterfaceItemBase;
import org.eclipse.etrice.runtime.java.modelbase.PortBase;
import org.eclipse.etrice.runtime.java.modelbase.ReplicatedPortBase;
import org.eclipse.etrice.runtime.java.debugging.DebuggingService;
import static org.eclipse.etrice.runtime.java.etunit.EtUnit.*;



public class InitOrUpdateProtocol {
	// message IDs
	public static final int MSG_MIN = 0;
	public static final int OUT_Initialize = 1;
	public static final int MSG_MAX = 2;


	private static String messageStrings[] = {"MIN", "Initialize", "MAX"};

	public String getMessageString(int msg_id) {
		if (msg_id<MSG_MIN || msg_id>MSG_MAX+1){
			// id out of range
			return "Message ID out of range";
		}
		else{
			return messageStrings[msg_id];
		}
	}

	
	// port class
	static public class InitOrUpdateProtocolPort extends PortBase {
		// constructors
		public InitOrUpdateProtocolPort(IInterfaceItemOwner actor, String name, int localId) {
			this(actor, name, localId, 0);
		}
		public InitOrUpdateProtocolPort(IInterfaceItemOwner actor, String name, int localId, int idx) {
			super(actor, name, localId, idx);
			DebuggingService.getInstance().addPortInstance(this);
		}
	
		public void destroy() {
			DebuggingService.getInstance().removePortInstance(this);
			super.destroy();
		}
	
		@Override
		public void receive(Message m) {
			if (!(m instanceof EventMessage))
				return;
			EventMessage msg = (EventMessage) m;
			if (0 < msg.getEvtId() && msg.getEvtId() < MSG_MAX) {
				DebuggingService.getInstance().addMessageAsyncIn(getPeerAddress(), getAddress(), messageStrings[msg.getEvtId()]);
				if (msg instanceof EventWithDataMessage)
					getActor().receiveEvent(this, msg.getEvtId(), ((EventWithDataMessage)msg).getData());
				else
					getActor().receiveEvent(this, msg.getEvtId(), null);
			}
	}
	
	
		// sent messages
		public void Initialize(InitializationPacket ip) {
			DebuggingService.getInstance().addMessageAsyncOut(getAddress(), getPeerAddress(), messageStrings[OUT_Initialize]);
			if (getPeerAddress()!=null)
				getPeerMsgReceiver().receive(new EventWithDataMessage(getPeerAddress(), OUT_Initialize, ip.deepCopy()));
		}
		public void Initialize(long systemStartTime, int controllerT, int sensorID, float sensorPError, int sensorTimeToMeasure, float sensorThreshold, float sensorLowerBound, float sensorUpperBound, String sensorName, float pumpErrorRate, int pumpWaitTime, int activationsBeforePump, long maxTimeToAlarmAfterCH4, long maxTimeToClosePumpAfterCH4, float COThreshold, float CH4Threshold, float airThreshold) {
			Initialize(new InitializationPacket(systemStartTime, controllerT, sensorID, sensorPError, sensorTimeToMeasure, sensorThreshold, sensorLowerBound, sensorUpperBound, sensorName, pumpErrorRate, pumpWaitTime, activationsBeforePump, maxTimeToAlarmAfterCH4, maxTimeToClosePumpAfterCH4, COThreshold, CH4Threshold, airThreshold));
		}
	}
	
	// replicated port class
	static public class InitOrUpdateProtocolReplPort extends ReplicatedPortBase {
	
		public InitOrUpdateProtocolReplPort(IInterfaceItemOwner actor, String name, int localId) {
			super(actor, name, localId);
		}
	
		public int getReplication() {
			return getNInterfaceItems();
		}
	
		public int getIndexOf(InterfaceItemBase ifitem){
				return ifitem.getIdx();
		}
	
		public InitOrUpdateProtocolPort get(int idx) {
			return (InitOrUpdateProtocolPort) getInterfaceItem(idx);
		}
	
		protected InterfaceItemBase createInterfaceItem(IInterfaceItemOwner rcv, String name, int lid, int idx) {
			return new InitOrUpdateProtocolPort(rcv, name, lid, idx);
		}
	
		// outgoing messages
		public void Initialize(InitializationPacket ip){
			for (InterfaceItemBase item : getItems()) {
				((InitOrUpdateProtocolPort)item).Initialize( ip);
			}
		}
	}
	
	
	// port class
	static public class InitOrUpdateProtocolConjPort extends PortBase {
		// constructors
		public InitOrUpdateProtocolConjPort(IInterfaceItemOwner actor, String name, int localId) {
			this(actor, name, localId, 0);
		}
		public InitOrUpdateProtocolConjPort(IInterfaceItemOwner actor, String name, int localId, int idx) {
			super(actor, name, localId, idx);
			DebuggingService.getInstance().addPortInstance(this);
		}
	
		public void destroy() {
			DebuggingService.getInstance().removePortInstance(this);
			super.destroy();
		}
	
		@Override
		public void receive(Message m) {
			if (!(m instanceof EventMessage))
				return;
			EventMessage msg = (EventMessage) m;
			if (0 < msg.getEvtId() && msg.getEvtId() < MSG_MAX) {
				DebuggingService.getInstance().addMessageAsyncIn(getPeerAddress(), getAddress(), messageStrings[msg.getEvtId()]);
				if (msg instanceof EventWithDataMessage)
					getActor().receiveEvent(this, msg.getEvtId(), ((EventWithDataMessage)msg).getData());
				else
					getActor().receiveEvent(this, msg.getEvtId(), null);
			}
	}
	
	
		// sent messages
	}
	
	// replicated port class
	static public class InitOrUpdateProtocolConjReplPort extends ReplicatedPortBase {
	
		public InitOrUpdateProtocolConjReplPort(IInterfaceItemOwner actor, String name, int localId) {
			super(actor, name, localId);
		}
	
		public int getReplication() {
			return getNInterfaceItems();
		}
	
		public int getIndexOf(InterfaceItemBase ifitem){
				return ifitem.getIdx();
		}
	
		public InitOrUpdateProtocolConjPort get(int idx) {
			return (InitOrUpdateProtocolConjPort) getInterfaceItem(idx);
		}
	
		protected InterfaceItemBase createInterfaceItem(IInterfaceItemOwner rcv, String name, int lid, int idx) {
			return new InitOrUpdateProtocolConjPort(rcv, name, lid, idx);
		}
	
		// incoming messages
	}
	
}
