package water;

public final class Water {
	
	private Water() {}
	
	
	public final static float WATER_HIGH_THRESHOLD = 20.05f;
	public final static float WATER_LOW_THRESHOLD = 19.95f;
	
	private final static float WATER_INCREASE = 0.01f;
	private final static float WATER_DECREASE = 0.01f;
	
	private float waterLevel = 20f;
	private static Water water = null;
	
	public static Water instance() {
		if (water == null)
			water = new Water();
		return water;		
	} 
	
	public synchronized void increaseWaterLevel() {
		waterLevel += WATER_INCREASE;
	}
	public synchronized void decreaseWaterLevel() {
		waterLevel -= WATER_DECREASE + WATER_INCREASE;
	}
	public synchronized float getWaterLevel() {
		return waterLevel;
	}
	

}
