pragma Warnings (Off);
pragma Ada_95;
pragma Source_File_Name (ada_main, Spec_File_Name => "b__prv.ads");
pragma Source_File_Name (ada_main, Body_File_Name => "b__prv.adb");
pragma Suppress (Overflow_Check);

with System.Restrictions;
with Ada.Exceptions;

package body ada_main is

   E067 : Short_Integer; pragma Import (Ada, E067, "system__os_lib_E");
   E010 : Short_Integer; pragma Import (Ada, E010, "system__soft_links_E");
   E008 : Short_Integer; pragma Import (Ada, E008, "system__exception_table_E");
   E063 : Short_Integer; pragma Import (Ada, E063, "ada__io_exceptions_E");
   E047 : Short_Integer; pragma Import (Ada, E047, "ada__strings_E");
   E033 : Short_Integer; pragma Import (Ada, E033, "ada__containers_E");
   E018 : Short_Integer; pragma Import (Ada, E018, "system__exceptions_E");
   E073 : Short_Integer; pragma Import (Ada, E073, "interfaces__c_E");
   E049 : Short_Integer; pragma Import (Ada, E049, "ada__strings__maps_E");
   E053 : Short_Integer; pragma Import (Ada, E053, "ada__strings__maps__constants_E");
   E094 : Short_Integer; pragma Import (Ada, E094, "system__soft_links__initialize_E");
   E075 : Short_Integer; pragma Import (Ada, E075, "system__object_reader_E");
   E042 : Short_Integer; pragma Import (Ada, E042, "system__dwarf_lines_E");
   E032 : Short_Integer; pragma Import (Ada, E032, "system__traceback__symbolic_E");
   E005 : Short_Integer; pragma Import (Ada, E005, "ada__numerics_E");
   E145 : Short_Integer; pragma Import (Ada, E145, "ada__tags_E");
   E162 : Short_Integer; pragma Import (Ada, E162, "ada__streams_E");
   E119 : Short_Integer; pragma Import (Ada, E119, "interfaces__c__strings_E");
   E179 : Short_Integer; pragma Import (Ada, E179, "system__file_control_block_E");
   E164 : Short_Integer; pragma Import (Ada, E164, "system__finalization_root_E");
   E160 : Short_Integer; pragma Import (Ada, E160, "ada__finalization_E");
   E178 : Short_Integer; pragma Import (Ada, E178, "system__file_io_E");
   E529 : Short_Integer; pragma Import (Ada, E529, "ada__streams__stream_io_E");
   E166 : Short_Integer; pragma Import (Ada, E166, "system__storage_pools_E");
   E157 : Short_Integer; pragma Import (Ada, E157, "system__finalization_masters_E");
   E155 : Short_Integer; pragma Import (Ada, E155, "system__storage_pools__subpools_E");
   E141 : Short_Integer; pragma Import (Ada, E141, "ada__strings__unbounded_E");
   E129 : Short_Integer; pragma Import (Ada, E129, "system__task_info_E");
   E104 : Short_Integer; pragma Import (Ada, E104, "ada__calendar_E");
   E113 : Short_Integer; pragma Import (Ada, E113, "ada__real_time_E");
   E174 : Short_Integer; pragma Import (Ada, E174, "ada__text_io_E");
   E513 : Short_Integer; pragma Import (Ada, E513, "system__assertions_E");
   E234 : Short_Integer; pragma Import (Ada, E234, "system__pool_global_E");
   E102 : Short_Integer; pragma Import (Ada, E102, "system__random_seed_E");
   E189 : Short_Integer; pragma Import (Ada, E189, "system__tasking__initialization_E");
   E197 : Short_Integer; pragma Import (Ada, E197, "system__tasking__protected_objects_E");
   E199 : Short_Integer; pragma Import (Ada, E199, "system__tasking__protected_objects__entries_E");
   E203 : Short_Integer; pragma Import (Ada, E203, "system__tasking__queuing_E");
   E209 : Short_Integer; pragma Import (Ada, E209, "system__tasking__stages_E");
   E257 : Short_Integer; pragma Import (Ada, E257, "glib_E");
   E260 : Short_Integer; pragma Import (Ada, E260, "gtkada__types_E");
   E248 : Short_Integer; pragma Import (Ada, E248, "formatter_E");
   E327 : Short_Integer; pragma Import (Ada, E327, "gdk__frame_timings_E");
   E280 : Short_Integer; pragma Import (Ada, E280, "glib__glist_E");
   E309 : Short_Integer; pragma Import (Ada, E309, "gdk__visual_E");
   E282 : Short_Integer; pragma Import (Ada, E282, "glib__gslist_E");
   E274 : Short_Integer; pragma Import (Ada, E274, "gtkada__c_E");
   E264 : Short_Integer; pragma Import (Ada, E264, "glib__object_E");
   E278 : Short_Integer; pragma Import (Ada, E278, "glib__values_E");
   E276 : Short_Integer; pragma Import (Ada, E276, "glib__types_E");
   E266 : Short_Integer; pragma Import (Ada, E266, "glib__type_conversion_hooks_E");
   E268 : Short_Integer; pragma Import (Ada, E268, "gtkada__bindings_E");
   E288 : Short_Integer; pragma Import (Ada, E288, "cairo_E");
   E290 : Short_Integer; pragma Import (Ada, E290, "cairo__region_E");
   E296 : Short_Integer; pragma Import (Ada, E296, "gdk__rectangle_E");
   E294 : Short_Integer; pragma Import (Ada, E294, "glib__generic_properties_E");
   E319 : Short_Integer; pragma Import (Ada, E319, "gdk__color_E");
   E299 : Short_Integer; pragma Import (Ada, E299, "gdk__rgba_E");
   E292 : Short_Integer; pragma Import (Ada, E292, "gdk__event_E");
   E430 : Short_Integer; pragma Import (Ada, E430, "glib__key_file_E");
   E311 : Short_Integer; pragma Import (Ada, E311, "glib__properties_E");
   E392 : Short_Integer; pragma Import (Ada, E392, "glib__string_E");
   E390 : Short_Integer; pragma Import (Ada, E390, "glib__variant_E");
   E388 : Short_Integer; pragma Import (Ada, E388, "glib__g_icon_E");
   E483 : Short_Integer; pragma Import (Ada, E483, "gtk__actionable_E");
   E335 : Short_Integer; pragma Import (Ada, E335, "gtk__builder_E");
   E372 : Short_Integer; pragma Import (Ada, E372, "gtk__buildable_E");
   E404 : Short_Integer; pragma Import (Ada, E404, "gtk__cell_area_context_E");
   E420 : Short_Integer; pragma Import (Ada, E420, "gtk__css_section_E");
   E313 : Short_Integer; pragma Import (Ada, E313, "gtk__enums_E");
   E378 : Short_Integer; pragma Import (Ada, E378, "gtk__orientable_E");
   E432 : Short_Integer; pragma Import (Ada, E432, "gtk__paper_size_E");
   E428 : Short_Integer; pragma Import (Ada, E428, "gtk__page_setup_E");
   E440 : Short_Integer; pragma Import (Ada, E440, "gtk__print_settings_E");
   E343 : Short_Integer; pragma Import (Ada, E343, "gtk__target_entry_E");
   E341 : Short_Integer; pragma Import (Ada, E341, "gtk__target_list_E");
   E454 : Short_Integer; pragma Import (Ada, E454, "gtk__text_mark_E");
   E348 : Short_Integer; pragma Import (Ada, E348, "pango__enums_E");
   E366 : Short_Integer; pragma Import (Ada, E366, "pango__attributes_E");
   E352 : Short_Integer; pragma Import (Ada, E352, "pango__font_metrics_E");
   E354 : Short_Integer; pragma Import (Ada, E354, "pango__language_E");
   E350 : Short_Integer; pragma Import (Ada, E350, "pango__font_E");
   E446 : Short_Integer; pragma Import (Ada, E446, "gtk__text_attributes_E");
   E448 : Short_Integer; pragma Import (Ada, E448, "gtk__text_tag_E");
   E358 : Short_Integer; pragma Import (Ada, E358, "pango__font_face_E");
   E356 : Short_Integer; pragma Import (Ada, E356, "pango__font_family_E");
   E360 : Short_Integer; pragma Import (Ada, E360, "pango__fontset_E");
   E362 : Short_Integer; pragma Import (Ada, E362, "pango__matrix_E");
   E346 : Short_Integer; pragma Import (Ada, E346, "pango__context_E");
   E436 : Short_Integer; pragma Import (Ada, E436, "pango__font_map_E");
   E368 : Short_Integer; pragma Import (Ada, E368, "pango__tabs_E");
   E364 : Short_Integer; pragma Import (Ada, E364, "pango__layout_E");
   E434 : Short_Integer; pragma Import (Ada, E434, "gtk__print_context_E");
   E307 : Short_Integer; pragma Import (Ada, E307, "gdk__display_E");
   E438 : Short_Integer; pragma Import (Ada, E438, "gtk__print_operation_preview_E");
   E410 : Short_Integer; pragma Import (Ada, E410, "gtk__tree_model_E");
   E398 : Short_Integer; pragma Import (Ada, E398, "gtk__entry_buffer_E");
   E396 : Short_Integer; pragma Import (Ada, E396, "gtk__editable_E");
   E394 : Short_Integer; pragma Import (Ada, E394, "gtk__cell_editable_E");
   E376 : Short_Integer; pragma Import (Ada, E376, "gtk__adjustment_E");
   E339 : Short_Integer; pragma Import (Ada, E339, "gtk__style_E");
   E333 : Short_Integer; pragma Import (Ada, E333, "gtk__accel_group_E");
   E325 : Short_Integer; pragma Import (Ada, E325, "gdk__frame_clock_E");
   E329 : Short_Integer; pragma Import (Ada, E329, "gdk__pixbuf_E");
   E416 : Short_Integer; pragma Import (Ada, E416, "gtk__icon_source_E");
   E305 : Short_Integer; pragma Import (Ada, E305, "gdk__screen_E");
   E444 : Short_Integer; pragma Import (Ada, E444, "gtk__text_iter_E");
   E337 : Short_Integer; pragma Import (Ada, E337, "gtk__selection_data_E");
   E321 : Short_Integer; pragma Import (Ada, E321, "gdk__device_E");
   E323 : Short_Integer; pragma Import (Ada, E323, "gdk__drag_contexts_E");
   E317 : Short_Integer; pragma Import (Ada, E317, "gtk__widget_E");
   E422 : Short_Integer; pragma Import (Ada, E422, "gtk__misc_E");
   E315 : Short_Integer; pragma Import (Ada, E315, "gtk__style_provider_E");
   E303 : Short_Integer; pragma Import (Ada, E303, "gtk__settings_E");
   E382 : Short_Integer; pragma Import (Ada, E382, "gdk__window_E");
   E418 : Short_Integer; pragma Import (Ada, E418, "gtk__style_context_E");
   E414 : Short_Integer; pragma Import (Ada, E414, "gtk__icon_set_E");
   E412 : Short_Integer; pragma Import (Ada, E412, "gtk__image_E");
   E408 : Short_Integer; pragma Import (Ada, E408, "gtk__cell_renderer_E");
   E374 : Short_Integer; pragma Import (Ada, E374, "gtk__container_E");
   E384 : Short_Integer; pragma Import (Ada, E384, "gtk__bin_E");
   E370 : Short_Integer; pragma Import (Ada, E370, "gtk__box_E");
   E442 : Short_Integer; pragma Import (Ada, E442, "gtk__status_bar_E");
   E424 : Short_Integer; pragma Import (Ada, E424, "gtk__notebook_E");
   E406 : Short_Integer; pragma Import (Ada, E406, "gtk__cell_layout_E");
   E402 : Short_Integer; pragma Import (Ada, E402, "gtk__cell_area_E");
   E400 : Short_Integer; pragma Import (Ada, E400, "gtk__entry_completion_E");
   E380 : Short_Integer; pragma Import (Ada, E380, "gtk__window_E");
   E301 : Short_Integer; pragma Import (Ada, E301, "gtk__dialog_E");
   E426 : Short_Integer; pragma Import (Ada, E426, "gtk__print_operation_E");
   E386 : Short_Integer; pragma Import (Ada, E386, "gtk__gentry_E");
   E286 : Short_Integer; pragma Import (Ada, E286, "gtk__arguments_E");
   E503 : Short_Integer; pragma Import (Ada, E503, "glib__menu_model_E");
   E481 : Short_Integer; pragma Import (Ada, E481, "gtk__action_E");
   E485 : Short_Integer; pragma Import (Ada, E485, "gtk__activatable_E");
   E479 : Short_Integer; pragma Import (Ada, E479, "gtk__button_E");
   E450 : Short_Integer; pragma Import (Ada, E450, "gtk__clipboard_E");
   E465 : Short_Integer; pragma Import (Ada, E465, "gtk__grange_E");
   E487 : Short_Integer; pragma Import (Ada, E487, "gtk__main_E");
   E515 : Short_Integer; pragma Import (Ada, E515, "gtk__marshallers_E");
   E505 : Short_Integer; pragma Import (Ada, E505, "gtk__menu_item_E");
   E507 : Short_Integer; pragma Import (Ada, E507, "gtk__menu_shell_E");
   E501 : Short_Integer; pragma Import (Ada, E501, "gtk__menu_E");
   E499 : Short_Integer; pragma Import (Ada, E499, "gtk__label_E");
   E463 : Short_Integer; pragma Import (Ada, E463, "gtk__scale_E");
   E460 : Short_Integer; pragma Import (Ada, E460, "gtk__scrollable_E");
   E489 : Short_Integer; pragma Import (Ada, E489, "gtk__switch_E");
   E452 : Short_Integer; pragma Import (Ada, E452, "gtk__text_child_anchor_E");
   E456 : Short_Integer; pragma Import (Ada, E456, "gtk__text_tag_table_E");
   E284 : Short_Integer; pragma Import (Ada, E284, "gtk__text_buffer_E");
   E458 : Short_Integer; pragma Import (Ada, E458, "gtk__text_view_E");
   E517 : Short_Integer; pragma Import (Ada, E517, "gtk__tree_view_column_E");
   E518 : Short_Integer; pragma Import (Ada, E518, "gtkada__handlers_E");
   E509 : Short_Integer; pragma Import (Ada, E509, "gtkada__builder_E");
   E254 : Short_Integer; pragma Import (Ada, E254, "logger_E");
   E211 : Short_Integer; pragma Import (Ada, E211, "parameters_E");
   E491 : Short_Integer; pragma Import (Ada, E491, "guidata_E");
   E240 : Short_Integer; pragma Import (Ada, E240, "actor_package_E");
   E238 : Short_Integer; pragma Import (Ada, E238, "controller_package_E");
   E473 : Short_Integer; pragma Import (Ada, E473, "pump_package_E");
   E469 : Short_Integer; pragma Import (Ada, E469, "sensor_core_package_E");
   E467 : Short_Integer; pragma Import (Ada, E467, "interrupt_sensor_package_E");
   E471 : Short_Integer; pragma Import (Ada, E471, "polling_sensor_package_E");
   E475 : Short_Integer; pragma Import (Ada, E475, "water_E");
   E477 : Short_Integer; pragma Import (Ada, E477, "water_flow_sensor_package_E");
   E228 : Short_Integer; pragma Import (Ada, E228, "processes_E");
   E207 : Short_Integer; pragma Import (Ada, E207, "control_package_E");
   E181 : Short_Integer; pragma Import (Ada, E181, "builder_cb_E");

   Sec_Default_Sized_Stacks : array (1 .. 1) of aliased System.Secondary_Stack.SS_Stack (System.Parameters.Runtime_Default_Sec_Stack_Size);

   Local_Priority_Specific_Dispatching : constant String := "";
   Local_Interrupt_States : constant String := "";

   Is_Elaborated : Boolean := False;

   procedure finalize_library is
   begin
      declare
         procedure F1;
         pragma Import (Ada, F1, "processes__finalize_body");
      begin
         E228 := E228 - 1;
         F1;
      end;
      E477 := E477 - 1;
      declare
         procedure F2;
         pragma Import (Ada, F2, "water_flow_sensor_package__finalize_spec");
      begin
         F2;
      end;
      E475 := E475 - 1;
      declare
         procedure F3;
         pragma Import (Ada, F3, "water__finalize_spec");
      begin
         F3;
      end;
      E471 := E471 - 1;
      declare
         procedure F4;
         pragma Import (Ada, F4, "polling_sensor_package__finalize_spec");
      begin
         F4;
      end;
      E467 := E467 - 1;
      declare
         procedure F5;
         pragma Import (Ada, F5, "interrupt_sensor_package__finalize_spec");
      begin
         F5;
      end;
      E473 := E473 - 1;
      declare
         procedure F6;
         pragma Import (Ada, F6, "pump_package__finalize_spec");
      begin
         F6;
      end;
      E238 := E238 - 1;
      declare
         procedure F7;
         pragma Import (Ada, F7, "controller_package__finalize_spec");
      begin
         F7;
      end;
      E254 := E254 - 1;
      declare
         procedure F8;
         pragma Import (Ada, F8, "logger__finalize_spec");
      begin
         F8;
      end;
      declare
         procedure F9;
         pragma Import (Ada, F9, "gtkada__builder__finalize_body");
      begin
         E509 := E509 - 1;
         F9;
      end;
      declare
         procedure F10;
         pragma Import (Ada, F10, "gtkada__builder__finalize_spec");
      begin
         F10;
      end;
      declare
         procedure F11;
         pragma Import (Ada, F11, "gtkada__handlers__finalize_spec");
      begin
         E518 := E518 - 1;
         F11;
      end;
      E517 := E517 - 1;
      declare
         procedure F12;
         pragma Import (Ada, F12, "gtk__tree_view_column__finalize_spec");
      begin
         F12;
      end;
      E458 := E458 - 1;
      declare
         procedure F13;
         pragma Import (Ada, F13, "gtk__text_view__finalize_spec");
      begin
         F13;
      end;
      E284 := E284 - 1;
      declare
         procedure F14;
         pragma Import (Ada, F14, "gtk__text_buffer__finalize_spec");
      begin
         F14;
      end;
      E456 := E456 - 1;
      declare
         procedure F15;
         pragma Import (Ada, F15, "gtk__text_tag_table__finalize_spec");
      begin
         F15;
      end;
      E452 := E452 - 1;
      declare
         procedure F16;
         pragma Import (Ada, F16, "gtk__text_child_anchor__finalize_spec");
      begin
         F16;
      end;
      E489 := E489 - 1;
      declare
         procedure F17;
         pragma Import (Ada, F17, "gtk__switch__finalize_spec");
      begin
         F17;
      end;
      E463 := E463 - 1;
      declare
         procedure F18;
         pragma Import (Ada, F18, "gtk__scale__finalize_spec");
      begin
         F18;
      end;
      E499 := E499 - 1;
      declare
         procedure F19;
         pragma Import (Ada, F19, "gtk__label__finalize_spec");
      begin
         F19;
      end;
      E501 := E501 - 1;
      declare
         procedure F20;
         pragma Import (Ada, F20, "gtk__menu__finalize_spec");
      begin
         F20;
      end;
      E507 := E507 - 1;
      declare
         procedure F21;
         pragma Import (Ada, F21, "gtk__menu_shell__finalize_spec");
      begin
         F21;
      end;
      E505 := E505 - 1;
      declare
         procedure F22;
         pragma Import (Ada, F22, "gtk__menu_item__finalize_spec");
      begin
         F22;
      end;
      E465 := E465 - 1;
      declare
         procedure F23;
         pragma Import (Ada, F23, "gtk__grange__finalize_spec");
      begin
         F23;
      end;
      E450 := E450 - 1;
      declare
         procedure F24;
         pragma Import (Ada, F24, "gtk__clipboard__finalize_spec");
      begin
         F24;
      end;
      E479 := E479 - 1;
      declare
         procedure F25;
         pragma Import (Ada, F25, "gtk__button__finalize_spec");
      begin
         F25;
      end;
      E481 := E481 - 1;
      declare
         procedure F26;
         pragma Import (Ada, F26, "gtk__action__finalize_spec");
      begin
         F26;
      end;
      E503 := E503 - 1;
      declare
         procedure F27;
         pragma Import (Ada, F27, "glib__menu_model__finalize_spec");
      begin
         F27;
      end;
      E307 := E307 - 1;
      E325 := E325 - 1;
      E333 := E333 - 1;
      E317 := E317 - 1;
      E339 := E339 - 1;
      E374 := E374 - 1;
      E376 := E376 - 1;
      E301 := E301 - 1;
      E380 := E380 - 1;
      E398 := E398 - 1;
      E408 := E408 - 1;
      E400 := E400 - 1;
      E402 := E402 - 1;
      E410 := E410 - 1;
      E386 := E386 - 1;
      E418 := E418 - 1;
      E424 := E424 - 1;
      E426 := E426 - 1;
      E442 := E442 - 1;
      declare
         procedure F28;
         pragma Import (Ada, F28, "gtk__gentry__finalize_spec");
      begin
         F28;
      end;
      declare
         procedure F29;
         pragma Import (Ada, F29, "gtk__print_operation__finalize_spec");
      begin
         F29;
      end;
      declare
         procedure F30;
         pragma Import (Ada, F30, "gtk__dialog__finalize_spec");
      begin
         F30;
      end;
      declare
         procedure F31;
         pragma Import (Ada, F31, "gtk__window__finalize_spec");
      begin
         F31;
      end;
      declare
         procedure F32;
         pragma Import (Ada, F32, "gtk__entry_completion__finalize_spec");
      begin
         F32;
      end;
      declare
         procedure F33;
         pragma Import (Ada, F33, "gtk__cell_area__finalize_spec");
      begin
         F33;
      end;
      declare
         procedure F34;
         pragma Import (Ada, F34, "gtk__notebook__finalize_spec");
      begin
         F34;
      end;
      declare
         procedure F35;
         pragma Import (Ada, F35, "gtk__status_bar__finalize_spec");
      begin
         F35;
      end;
      E370 := E370 - 1;
      declare
         procedure F36;
         pragma Import (Ada, F36, "gtk__box__finalize_spec");
      begin
         F36;
      end;
      E384 := E384 - 1;
      declare
         procedure F37;
         pragma Import (Ada, F37, "gtk__bin__finalize_spec");
      begin
         F37;
      end;
      declare
         procedure F38;
         pragma Import (Ada, F38, "gtk__container__finalize_spec");
      begin
         F38;
      end;
      declare
         procedure F39;
         pragma Import (Ada, F39, "gtk__cell_renderer__finalize_spec");
      begin
         F39;
      end;
      E412 := E412 - 1;
      declare
         procedure F40;
         pragma Import (Ada, F40, "gtk__image__finalize_spec");
      begin
         F40;
      end;
      E414 := E414 - 1;
      declare
         procedure F41;
         pragma Import (Ada, F41, "gtk__icon_set__finalize_spec");
      begin
         F41;
      end;
      declare
         procedure F42;
         pragma Import (Ada, F42, "gtk__style_context__finalize_spec");
      begin
         F42;
      end;
      E303 := E303 - 1;
      declare
         procedure F43;
         pragma Import (Ada, F43, "gtk__settings__finalize_spec");
      begin
         F43;
      end;
      E422 := E422 - 1;
      declare
         procedure F44;
         pragma Import (Ada, F44, "gtk__misc__finalize_spec");
      begin
         F44;
      end;
      declare
         procedure F45;
         pragma Import (Ada, F45, "gtk__widget__finalize_spec");
      begin
         F45;
      end;
      E321 := E321 - 1;
      E323 := E323 - 1;
      declare
         procedure F46;
         pragma Import (Ada, F46, "gdk__drag_contexts__finalize_spec");
      begin
         F46;
      end;
      declare
         procedure F47;
         pragma Import (Ada, F47, "gdk__device__finalize_spec");
      begin
         F47;
      end;
      E337 := E337 - 1;
      declare
         procedure F48;
         pragma Import (Ada, F48, "gtk__selection_data__finalize_spec");
      begin
         F48;
      end;
      E305 := E305 - 1;
      declare
         procedure F49;
         pragma Import (Ada, F49, "gdk__screen__finalize_spec");
      begin
         F49;
      end;
      E329 := E329 - 1;
      E416 := E416 - 1;
      declare
         procedure F50;
         pragma Import (Ada, F50, "gtk__icon_source__finalize_spec");
      begin
         F50;
      end;
      declare
         procedure F51;
         pragma Import (Ada, F51, "gdk__pixbuf__finalize_spec");
      begin
         F51;
      end;
      declare
         procedure F52;
         pragma Import (Ada, F52, "gdk__frame_clock__finalize_spec");
      begin
         F52;
      end;
      declare
         procedure F53;
         pragma Import (Ada, F53, "gtk__accel_group__finalize_spec");
      begin
         F53;
      end;
      declare
         procedure F54;
         pragma Import (Ada, F54, "gtk__style__finalize_spec");
      begin
         F54;
      end;
      declare
         procedure F55;
         pragma Import (Ada, F55, "gtk__adjustment__finalize_spec");
      begin
         F55;
      end;
      declare
         procedure F56;
         pragma Import (Ada, F56, "gtk__entry_buffer__finalize_spec");
      begin
         F56;
      end;
      declare
         procedure F57;
         pragma Import (Ada, F57, "gtk__tree_model__finalize_spec");
      begin
         F57;
      end;
      declare
         procedure F58;
         pragma Import (Ada, F58, "gdk__display__finalize_spec");
      begin
         F58;
      end;
      E434 := E434 - 1;
      declare
         procedure F59;
         pragma Import (Ada, F59, "gtk__print_context__finalize_spec");
      begin
         F59;
      end;
      E364 := E364 - 1;
      declare
         procedure F60;
         pragma Import (Ada, F60, "pango__layout__finalize_spec");
      begin
         F60;
      end;
      E368 := E368 - 1;
      declare
         procedure F61;
         pragma Import (Ada, F61, "pango__tabs__finalize_spec");
      begin
         F61;
      end;
      E436 := E436 - 1;
      declare
         procedure F62;
         pragma Import (Ada, F62, "pango__font_map__finalize_spec");
      begin
         F62;
      end;
      E346 := E346 - 1;
      declare
         procedure F63;
         pragma Import (Ada, F63, "pango__context__finalize_spec");
      begin
         F63;
      end;
      E360 := E360 - 1;
      declare
         procedure F64;
         pragma Import (Ada, F64, "pango__fontset__finalize_spec");
      begin
         F64;
      end;
      E356 := E356 - 1;
      declare
         procedure F65;
         pragma Import (Ada, F65, "pango__font_family__finalize_spec");
      begin
         F65;
      end;
      E358 := E358 - 1;
      declare
         procedure F66;
         pragma Import (Ada, F66, "pango__font_face__finalize_spec");
      begin
         F66;
      end;
      E448 := E448 - 1;
      declare
         procedure F67;
         pragma Import (Ada, F67, "gtk__text_tag__finalize_spec");
      begin
         F67;
      end;
      E350 := E350 - 1;
      declare
         procedure F68;
         pragma Import (Ada, F68, "pango__font__finalize_spec");
      begin
         F68;
      end;
      E354 := E354 - 1;
      declare
         procedure F69;
         pragma Import (Ada, F69, "pango__language__finalize_spec");
      begin
         F69;
      end;
      E352 := E352 - 1;
      declare
         procedure F70;
         pragma Import (Ada, F70, "pango__font_metrics__finalize_spec");
      begin
         F70;
      end;
      E366 := E366 - 1;
      declare
         procedure F71;
         pragma Import (Ada, F71, "pango__attributes__finalize_spec");
      begin
         F71;
      end;
      E454 := E454 - 1;
      declare
         procedure F72;
         pragma Import (Ada, F72, "gtk__text_mark__finalize_spec");
      begin
         F72;
      end;
      E341 := E341 - 1;
      declare
         procedure F73;
         pragma Import (Ada, F73, "gtk__target_list__finalize_spec");
      begin
         F73;
      end;
      E440 := E440 - 1;
      declare
         procedure F74;
         pragma Import (Ada, F74, "gtk__print_settings__finalize_spec");
      begin
         F74;
      end;
      E428 := E428 - 1;
      declare
         procedure F75;
         pragma Import (Ada, F75, "gtk__page_setup__finalize_spec");
      begin
         F75;
      end;
      E432 := E432 - 1;
      declare
         procedure F76;
         pragma Import (Ada, F76, "gtk__paper_size__finalize_spec");
      begin
         F76;
      end;
      E420 := E420 - 1;
      declare
         procedure F77;
         pragma Import (Ada, F77, "gtk__css_section__finalize_spec");
      begin
         F77;
      end;
      E404 := E404 - 1;
      declare
         procedure F78;
         pragma Import (Ada, F78, "gtk__cell_area_context__finalize_spec");
      begin
         F78;
      end;
      E335 := E335 - 1;
      declare
         procedure F79;
         pragma Import (Ada, F79, "gtk__builder__finalize_spec");
      begin
         F79;
      end;
      E390 := E390 - 1;
      declare
         procedure F80;
         pragma Import (Ada, F80, "glib__variant__finalize_spec");
      begin
         F80;
      end;
      E264 := E264 - 1;
      declare
         procedure F81;
         pragma Import (Ada, F81, "glib__object__finalize_spec");
      begin
         F81;
      end;
      E327 := E327 - 1;
      declare
         procedure F82;
         pragma Import (Ada, F82, "gdk__frame_timings__finalize_spec");
      begin
         F82;
      end;
      E257 := E257 - 1;
      declare
         procedure F83;
         pragma Import (Ada, F83, "glib__finalize_spec");
      begin
         F83;
      end;
      E199 := E199 - 1;
      declare
         procedure F84;
         pragma Import (Ada, F84, "system__tasking__protected_objects__entries__finalize_spec");
      begin
         F84;
      end;
      E234 := E234 - 1;
      declare
         procedure F85;
         pragma Import (Ada, F85, "system__pool_global__finalize_spec");
      begin
         F85;
      end;
      E174 := E174 - 1;
      declare
         procedure F86;
         pragma Import (Ada, F86, "ada__text_io__finalize_spec");
      begin
         F86;
      end;
      E141 := E141 - 1;
      declare
         procedure F87;
         pragma Import (Ada, F87, "ada__strings__unbounded__finalize_spec");
      begin
         F87;
      end;
      E155 := E155 - 1;
      declare
         procedure F88;
         pragma Import (Ada, F88, "system__storage_pools__subpools__finalize_spec");
      begin
         F88;
      end;
      E157 := E157 - 1;
      declare
         procedure F89;
         pragma Import (Ada, F89, "system__finalization_masters__finalize_spec");
      begin
         F89;
      end;
      E529 := E529 - 1;
      declare
         procedure F90;
         pragma Import (Ada, F90, "ada__streams__stream_io__finalize_spec");
      begin
         F90;
      end;
      declare
         procedure F91;
         pragma Import (Ada, F91, "system__file_io__finalize_body");
      begin
         E178 := E178 - 1;
         F91;
      end;
      declare
         procedure Reraise_Library_Exception_If_Any;
            pragma Import (Ada, Reraise_Library_Exception_If_Any, "__gnat_reraise_library_exception_if_any");
      begin
         Reraise_Library_Exception_If_Any;
      end;
   end finalize_library;

   procedure adafinal is
      procedure s_stalib_adafinal;
      pragma Import (C, s_stalib_adafinal, "system__standard_library__adafinal");

      procedure Runtime_Finalize;
      pragma Import (C, Runtime_Finalize, "__gnat_runtime_finalize");

   begin
      if not Is_Elaborated then
         return;
      end if;
      Is_Elaborated := False;
      Runtime_Finalize;
      s_stalib_adafinal;
   end adafinal;

   type No_Param_Proc is access procedure;

   procedure adainit is
      Main_Priority : Integer;
      pragma Import (C, Main_Priority, "__gl_main_priority");
      Time_Slice_Value : Integer;
      pragma Import (C, Time_Slice_Value, "__gl_time_slice_val");
      WC_Encoding : Character;
      pragma Import (C, WC_Encoding, "__gl_wc_encoding");
      Locking_Policy : Character;
      pragma Import (C, Locking_Policy, "__gl_locking_policy");
      Queuing_Policy : Character;
      pragma Import (C, Queuing_Policy, "__gl_queuing_policy");
      Task_Dispatching_Policy : Character;
      pragma Import (C, Task_Dispatching_Policy, "__gl_task_dispatching_policy");
      Priority_Specific_Dispatching : System.Address;
      pragma Import (C, Priority_Specific_Dispatching, "__gl_priority_specific_dispatching");
      Num_Specific_Dispatching : Integer;
      pragma Import (C, Num_Specific_Dispatching, "__gl_num_specific_dispatching");
      Main_CPU : Integer;
      pragma Import (C, Main_CPU, "__gl_main_cpu");
      Interrupt_States : System.Address;
      pragma Import (C, Interrupt_States, "__gl_interrupt_states");
      Num_Interrupt_States : Integer;
      pragma Import (C, Num_Interrupt_States, "__gl_num_interrupt_states");
      Unreserve_All_Interrupts : Integer;
      pragma Import (C, Unreserve_All_Interrupts, "__gl_unreserve_all_interrupts");
      Detect_Blocking : Integer;
      pragma Import (C, Detect_Blocking, "__gl_detect_blocking");
      Default_Stack_Size : Integer;
      pragma Import (C, Default_Stack_Size, "__gl_default_stack_size");
      Default_Secondary_Stack_Size : System.Parameters.Size_Type;
      pragma Import (C, Default_Secondary_Stack_Size, "__gnat_default_ss_size");
      Leap_Seconds_Support : Integer;
      pragma Import (C, Leap_Seconds_Support, "__gl_leap_seconds_support");
      Bind_Env_Addr : System.Address;
      pragma Import (C, Bind_Env_Addr, "__gl_bind_env_addr");

      procedure Runtime_Initialize (Install_Handler : Integer);
      pragma Import (C, Runtime_Initialize, "__gnat_runtime_initialize");

      Finalize_Library_Objects : No_Param_Proc;
      pragma Import (C, Finalize_Library_Objects, "__gnat_finalize_library_objects");
      Binder_Sec_Stacks_Count : Natural;
      pragma Import (Ada, Binder_Sec_Stacks_Count, "__gnat_binder_ss_count");
      Default_Sized_SS_Pool : System.Address;
      pragma Import (Ada, Default_Sized_SS_Pool, "__gnat_default_ss_pool");

   begin
      if Is_Elaborated then
         return;
      end if;
      Is_Elaborated := True;
      Main_Priority := -1;
      Time_Slice_Value := -1;
      WC_Encoding := 'b';
      Locking_Policy := ' ';
      Queuing_Policy := ' ';
      Task_Dispatching_Policy := ' ';
      System.Restrictions.Run_Time_Restrictions :=
        (Set =>
          (False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, True, False, 
           False, False, False, False, False, True, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False),
         Value => (0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
         Violated =>
          (False, False, True, False, True, True, True, False, 
           True, False, False, True, True, True, True, False, 
           False, False, False, False, True, True, False, True, 
           True, False, True, True, True, True, False, True, 
           False, False, False, True, False, False, True, False, 
           True, False, True, True, False, False, False, True, 
           True, False, True, True, False, True, True, False, 
           False, False, False, True, False, True, True, True, 
           False, False, True, False, True, True, True, False, 
           True, True, False, True, True, True, True, False, 
           False, True, False, False, False, False, True, True, 
           True, False, True, False),
         Count => (0, 0, 0, 0, 3, 5, 1, 0, 5, 0),
         Unknown => (False, False, False, False, False, False, True, False, True, False));
      Priority_Specific_Dispatching :=
        Local_Priority_Specific_Dispatching'Address;
      Num_Specific_Dispatching := 0;
      Main_CPU := -1;
      Interrupt_States := Local_Interrupt_States'Address;
      Num_Interrupt_States := 0;
      Unreserve_All_Interrupts := 0;
      Detect_Blocking := 0;
      Default_Stack_Size := -1;
      Leap_Seconds_Support := 0;

      ada_main'Elab_Body;
      Default_Secondary_Stack_Size := System.Parameters.Runtime_Default_Sec_Stack_Size;
      Binder_Sec_Stacks_Count := 1;
      Default_Sized_SS_Pool := Sec_Default_Sized_Stacks'Address;

      Runtime_Initialize (1);

      Finalize_Library_Objects := finalize_library'access;

      System.Soft_Links'Elab_Spec;
      System.Exception_Table'Elab_Body;
      E008 := E008 + 1;
      Ada.Io_Exceptions'Elab_Spec;
      E063 := E063 + 1;
      Ada.Strings'Elab_Spec;
      E047 := E047 + 1;
      Ada.Containers'Elab_Spec;
      E033 := E033 + 1;
      System.Exceptions'Elab_Spec;
      E018 := E018 + 1;
      Interfaces.C'Elab_Spec;
      System.Os_Lib'Elab_Body;
      E067 := E067 + 1;
      Ada.Strings.Maps'Elab_Spec;
      Ada.Strings.Maps.Constants'Elab_Spec;
      E053 := E053 + 1;
      System.Soft_Links.Initialize'Elab_Body;
      E094 := E094 + 1;
      E010 := E010 + 1;
      System.Object_Reader'Elab_Spec;
      System.Dwarf_Lines'Elab_Spec;
      E042 := E042 + 1;
      E073 := E073 + 1;
      E049 := E049 + 1;
      System.Traceback.Symbolic'Elab_Body;
      E032 := E032 + 1;
      E075 := E075 + 1;
      Ada.Numerics'Elab_Spec;
      E005 := E005 + 1;
      Ada.Tags'Elab_Spec;
      Ada.Tags'Elab_Body;
      E145 := E145 + 1;
      Ada.Streams'Elab_Spec;
      E162 := E162 + 1;
      Interfaces.C.Strings'Elab_Spec;
      E119 := E119 + 1;
      System.File_Control_Block'Elab_Spec;
      E179 := E179 + 1;
      System.Finalization_Root'Elab_Spec;
      E164 := E164 + 1;
      Ada.Finalization'Elab_Spec;
      E160 := E160 + 1;
      System.File_Io'Elab_Body;
      E178 := E178 + 1;
      Ada.Streams.Stream_Io'Elab_Spec;
      E529 := E529 + 1;
      System.Storage_Pools'Elab_Spec;
      E166 := E166 + 1;
      System.Finalization_Masters'Elab_Spec;
      System.Finalization_Masters'Elab_Body;
      E157 := E157 + 1;
      System.Storage_Pools.Subpools'Elab_Spec;
      E155 := E155 + 1;
      Ada.Strings.Unbounded'Elab_Spec;
      E141 := E141 + 1;
      System.Task_Info'Elab_Spec;
      E129 := E129 + 1;
      Ada.Calendar'Elab_Spec;
      Ada.Calendar'Elab_Body;
      E104 := E104 + 1;
      Ada.Real_Time'Elab_Spec;
      Ada.Real_Time'Elab_Body;
      E113 := E113 + 1;
      Ada.Text_Io'Elab_Spec;
      Ada.Text_Io'Elab_Body;
      E174 := E174 + 1;
      System.Assertions'Elab_Spec;
      E513 := E513 + 1;
      System.Pool_Global'Elab_Spec;
      E234 := E234 + 1;
      System.Random_Seed'Elab_Body;
      E102 := E102 + 1;
      System.Tasking.Initialization'Elab_Body;
      E189 := E189 + 1;
      System.Tasking.Protected_Objects'Elab_Body;
      E197 := E197 + 1;
      System.Tasking.Protected_Objects.Entries'Elab_Spec;
      E199 := E199 + 1;
      System.Tasking.Queuing'Elab_Body;
      E203 := E203 + 1;
      System.Tasking.Stages'Elab_Body;
      E209 := E209 + 1;
      Glib'Elab_Spec;
      Gtkada.Types'Elab_Spec;
      E260 := E260 + 1;
      E257 := E257 + 1;
      E248 := E248 + 1;
      Gdk.Frame_Timings'Elab_Spec;
      Gdk.Frame_Timings'Elab_Body;
      E327 := E327 + 1;
      E280 := E280 + 1;
      Gdk.Visual'Elab_Body;
      E309 := E309 + 1;
      E282 := E282 + 1;
      E274 := E274 + 1;
      Glib.Object'Elab_Spec;
      Glib.Values'Elab_Body;
      E278 := E278 + 1;
      E266 := E266 + 1;
      E276 := E276 + 1;
      E268 := E268 + 1;
      Glib.Object'Elab_Body;
      E264 := E264 + 1;
      E288 := E288 + 1;
      E290 := E290 + 1;
      E296 := E296 + 1;
      Glib.Generic_Properties'Elab_Spec;
      Glib.Generic_Properties'Elab_Body;
      E294 := E294 + 1;
      Gdk.Color'Elab_Spec;
      E319 := E319 + 1;
      E299 := E299 + 1;
      E292 := E292 + 1;
      E430 := E430 + 1;
      E311 := E311 + 1;
      E392 := E392 + 1;
      Glib.Variant'Elab_Spec;
      Glib.Variant'Elab_Body;
      E390 := E390 + 1;
      E388 := E388 + 1;
      Gtk.Actionable'Elab_Spec;
      E483 := E483 + 1;
      Gtk.Builder'Elab_Spec;
      Gtk.Builder'Elab_Body;
      E335 := E335 + 1;
      E372 := E372 + 1;
      Gtk.Cell_Area_Context'Elab_Spec;
      Gtk.Cell_Area_Context'Elab_Body;
      E404 := E404 + 1;
      Gtk.Css_Section'Elab_Spec;
      Gtk.Css_Section'Elab_Body;
      E420 := E420 + 1;
      E313 := E313 + 1;
      Gtk.Orientable'Elab_Spec;
      E378 := E378 + 1;
      Gtk.Paper_Size'Elab_Spec;
      Gtk.Paper_Size'Elab_Body;
      E432 := E432 + 1;
      Gtk.Page_Setup'Elab_Spec;
      Gtk.Page_Setup'Elab_Body;
      E428 := E428 + 1;
      Gtk.Print_Settings'Elab_Spec;
      Gtk.Print_Settings'Elab_Body;
      E440 := E440 + 1;
      E343 := E343 + 1;
      Gtk.Target_List'Elab_Spec;
      Gtk.Target_List'Elab_Body;
      E341 := E341 + 1;
      Gtk.Text_Mark'Elab_Spec;
      Gtk.Text_Mark'Elab_Body;
      E454 := E454 + 1;
      E348 := E348 + 1;
      Pango.Attributes'Elab_Spec;
      Pango.Attributes'Elab_Body;
      E366 := E366 + 1;
      Pango.Font_Metrics'Elab_Spec;
      Pango.Font_Metrics'Elab_Body;
      E352 := E352 + 1;
      Pango.Language'Elab_Spec;
      Pango.Language'Elab_Body;
      E354 := E354 + 1;
      Pango.Font'Elab_Spec;
      Pango.Font'Elab_Body;
      E350 := E350 + 1;
      E446 := E446 + 1;
      Gtk.Text_Tag'Elab_Spec;
      Gtk.Text_Tag'Elab_Body;
      E448 := E448 + 1;
      Pango.Font_Face'Elab_Spec;
      Pango.Font_Face'Elab_Body;
      E358 := E358 + 1;
      Pango.Font_Family'Elab_Spec;
      Pango.Font_Family'Elab_Body;
      E356 := E356 + 1;
      Pango.Fontset'Elab_Spec;
      Pango.Fontset'Elab_Body;
      E360 := E360 + 1;
      E362 := E362 + 1;
      Pango.Context'Elab_Spec;
      Pango.Context'Elab_Body;
      E346 := E346 + 1;
      Pango.Font_Map'Elab_Spec;
      Pango.Font_Map'Elab_Body;
      E436 := E436 + 1;
      Pango.Tabs'Elab_Spec;
      Pango.Tabs'Elab_Body;
      E368 := E368 + 1;
      Pango.Layout'Elab_Spec;
      Pango.Layout'Elab_Body;
      E364 := E364 + 1;
      Gtk.Print_Context'Elab_Spec;
      Gtk.Print_Context'Elab_Body;
      E434 := E434 + 1;
      Gdk.Display'Elab_Spec;
      Gtk.Tree_Model'Elab_Spec;
      Gtk.Entry_Buffer'Elab_Spec;
      Gtk.Cell_Editable'Elab_Spec;
      Gtk.Adjustment'Elab_Spec;
      Gtk.Style'Elab_Spec;
      Gtk.Accel_Group'Elab_Spec;
      Gdk.Frame_Clock'Elab_Spec;
      Gdk.Pixbuf'Elab_Spec;
      Gtk.Icon_Source'Elab_Spec;
      Gtk.Icon_Source'Elab_Body;
      E416 := E416 + 1;
      E329 := E329 + 1;
      Gdk.Screen'Elab_Spec;
      Gdk.Screen'Elab_Body;
      E305 := E305 + 1;
      E444 := E444 + 1;
      Gtk.Selection_Data'Elab_Spec;
      Gtk.Selection_Data'Elab_Body;
      E337 := E337 + 1;
      Gdk.Device'Elab_Spec;
      Gdk.Drag_Contexts'Elab_Spec;
      Gdk.Drag_Contexts'Elab_Body;
      E323 := E323 + 1;
      Gdk.Device'Elab_Body;
      E321 := E321 + 1;
      Gtk.Widget'Elab_Spec;
      Gtk.Misc'Elab_Spec;
      Gtk.Misc'Elab_Body;
      E422 := E422 + 1;
      E315 := E315 + 1;
      Gtk.Settings'Elab_Spec;
      Gtk.Settings'Elab_Body;
      E303 := E303 + 1;
      Gdk.Window'Elab_Spec;
      E382 := E382 + 1;
      Gtk.Style_Context'Elab_Spec;
      Gtk.Icon_Set'Elab_Spec;
      Gtk.Icon_Set'Elab_Body;
      E414 := E414 + 1;
      Gtk.Image'Elab_Spec;
      Gtk.Image'Elab_Body;
      E412 := E412 + 1;
      Gtk.Cell_Renderer'Elab_Spec;
      Gtk.Container'Elab_Spec;
      Gtk.Bin'Elab_Spec;
      Gtk.Bin'Elab_Body;
      E384 := E384 + 1;
      Gtk.Box'Elab_Spec;
      Gtk.Box'Elab_Body;
      E370 := E370 + 1;
      Gtk.Status_Bar'Elab_Spec;
      Gtk.Notebook'Elab_Spec;
      E406 := E406 + 1;
      Gtk.Cell_Area'Elab_Spec;
      Gtk.Entry_Completion'Elab_Spec;
      Gtk.Window'Elab_Spec;
      Gtk.Dialog'Elab_Spec;
      Gtk.Print_Operation'Elab_Spec;
      Gtk.Gentry'Elab_Spec;
      Gtk.Status_Bar'Elab_Body;
      E442 := E442 + 1;
      E438 := E438 + 1;
      Gtk.Print_Operation'Elab_Body;
      E426 := E426 + 1;
      Gtk.Notebook'Elab_Body;
      E424 := E424 + 1;
      Gtk.Style_Context'Elab_Body;
      E418 := E418 + 1;
      Gtk.Gentry'Elab_Body;
      E386 := E386 + 1;
      Gtk.Tree_Model'Elab_Body;
      E410 := E410 + 1;
      Gtk.Cell_Area'Elab_Body;
      E402 := E402 + 1;
      Gtk.Entry_Completion'Elab_Body;
      E400 := E400 + 1;
      Gtk.Cell_Renderer'Elab_Body;
      E408 := E408 + 1;
      Gtk.Entry_Buffer'Elab_Body;
      E398 := E398 + 1;
      E396 := E396 + 1;
      E394 := E394 + 1;
      Gtk.Window'Elab_Body;
      E380 := E380 + 1;
      Gtk.Dialog'Elab_Body;
      E301 := E301 + 1;
      Gtk.Adjustment'Elab_Body;
      E376 := E376 + 1;
      Gtk.Container'Elab_Body;
      E374 := E374 + 1;
      Gtk.Style'Elab_Body;
      E339 := E339 + 1;
      Gtk.Widget'Elab_Body;
      E317 := E317 + 1;
      Gtk.Accel_Group'Elab_Body;
      E333 := E333 + 1;
      Gdk.Frame_Clock'Elab_Body;
      E325 := E325 + 1;
      Gdk.Display'Elab_Body;
      E307 := E307 + 1;
      E286 := E286 + 1;
      Glib.Menu_Model'Elab_Spec;
      Glib.Menu_Model'Elab_Body;
      E503 := E503 + 1;
      Gtk.Action'Elab_Spec;
      Gtk.Action'Elab_Body;
      E481 := E481 + 1;
      Gtk.Activatable'Elab_Spec;
      E485 := E485 + 1;
      Gtk.Button'Elab_Spec;
      Gtk.Button'Elab_Body;
      E479 := E479 + 1;
      Gtk.Clipboard'Elab_Spec;
      Gtk.Clipboard'Elab_Body;
      E450 := E450 + 1;
      Gtk.Grange'Elab_Spec;
      Gtk.Grange'Elab_Body;
      E465 := E465 + 1;
      E487 := E487 + 1;
      E515 := E515 + 1;
      Gtk.Menu_Item'Elab_Spec;
      Gtk.Menu_Item'Elab_Body;
      E505 := E505 + 1;
      Gtk.Menu_Shell'Elab_Spec;
      Gtk.Menu_Shell'Elab_Body;
      E507 := E507 + 1;
      Gtk.Menu'Elab_Spec;
      Gtk.Menu'Elab_Body;
      E501 := E501 + 1;
      Gtk.Label'Elab_Spec;
      Gtk.Label'Elab_Body;
      E499 := E499 + 1;
      Gtk.Scale'Elab_Spec;
      Gtk.Scale'Elab_Body;
      E463 := E463 + 1;
      Gtk.Scrollable'Elab_Spec;
      E460 := E460 + 1;
      Gtk.Switch'Elab_Spec;
      Gtk.Switch'Elab_Body;
      E489 := E489 + 1;
      Gtk.Text_Child_Anchor'Elab_Spec;
      Gtk.Text_Child_Anchor'Elab_Body;
      E452 := E452 + 1;
      Gtk.Text_Tag_Table'Elab_Spec;
      Gtk.Text_Tag_Table'Elab_Body;
      E456 := E456 + 1;
      Gtk.Text_Buffer'Elab_Spec;
      Gtk.Text_Buffer'Elab_Body;
      E284 := E284 + 1;
      Gtk.Text_View'Elab_Spec;
      Gtk.Text_View'Elab_Body;
      E458 := E458 + 1;
      Gtk.Tree_View_Column'Elab_Spec;
      Gtk.Tree_View_Column'Elab_Body;
      E517 := E517 + 1;
      Gtkada.Handlers'Elab_Spec;
      E518 := E518 + 1;
      Gtkada.Builder'Elab_Spec;
      Gtkada.Builder'Elab_Body;
      E509 := E509 + 1;
      logger'elab_spec;
      E254 := E254 + 1;
      E211 := E211 + 1;
      E491 := E491 + 1;
      actor_package'elab_spec;
      actor_package'elab_body;
      E240 := E240 + 1;
      controller_package'elab_spec;
      controller_package'elab_body;
      E238 := E238 + 1;
      pump_package'elab_spec;
      pump_package'elab_body;
      E473 := E473 + 1;
      sensor_core_package'elab_spec;
      sensor_core_package'elab_body;
      E469 := E469 + 1;
      interrupt_sensor_package'elab_spec;
      interrupt_sensor_package'elab_body;
      E467 := E467 + 1;
      polling_sensor_package'elab_spec;
      polling_sensor_package'elab_body;
      E471 := E471 + 1;
      water'elab_spec;
      E475 := E475 + 1;
      water_flow_sensor_package'elab_spec;
      water_flow_sensor_package'elab_body;
      E477 := E477 + 1;
      processes'elab_spec;
      processes'elab_body;
      E228 := E228 + 1;
      E207 := E207 + 1;
      builder_cb'elab_spec;
      E181 := E181 + 1;
   end adainit;

   procedure Ada_Main_Program;
   pragma Import (Ada, Ada_Main_Program, "_ada_prv");

   function main
     (argc : Integer;
      argv : System.Address;
      envp : System.Address)
      return Integer
   is
      procedure Initialize (Addr : System.Address);
      pragma Import (C, Initialize, "__gnat_initialize");

      procedure Finalize;
      pragma Import (C, Finalize, "__gnat_finalize");
      SEH : aliased array (1 .. 2) of Integer;

      Ensure_Reference : aliased System.Address := Ada_Main_Program_Name'Address;
      pragma Volatile (Ensure_Reference);

   begin
      gnat_argc := argc;
      gnat_argv := argv;
      gnat_envp := envp;

      Initialize (SEH'Address);
      adainit;
      Ada_Main_Program;
      adafinal;
      Finalize;
      return (gnat_exit_status);
   end;

--  BEGIN Object file/option list
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\formatter.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\logger.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\parameters.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\guisensor.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\guidata.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\types.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\actor_package.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\controller_package.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\pump_package.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\sensor_core_package.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\interrupt_sensor_package.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\polling_sensor_package.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\water.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\water_flow_sensor_package.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\processes.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\control_package.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\builder_cb.o
   --   C:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\prv.o
   --   -LC:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\
   --   -LC:\Users\dv_work\Desktop\Ada_Projects\PRV_Projekat\obj\
   --   -LC:\GtkAda\lib\gtkada\gtkada.static\gtkada\
   --   -LC:/gnat/2018/lib/gcc/x86_64-pc-mingw32/7.3.1/adalib/
   --   -static
   --   -shared-libgcc
   --   -shared-libgcc
   --   -lgthread-2.0
   --   -shared-libgcc
   --   -lgnarl
   --   -lgnat
   --   -Xlinker
   --   --stack=0x200000,0x1000
   --   -mthreads
   --   -Wl,--stack=0x2000000
--  END Object file/option list   

end ada_main;
