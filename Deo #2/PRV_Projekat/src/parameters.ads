with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings; use Ada.Strings;


package parameters is
   
   type Sensor_Parameters is record
      PError: Float;
      TimeToMeasure: Integer;
      Threshold: Float;
      Low: Float;
      High: Float;        
   end record;
   
   type Project_Parameters is record
      PumpErrorRate: Float;
      PumpWaitTime:  Integer;
         
      MaxTimeToAlarmAfterCH4: Integer;
      MaxTimeToClosePumpAfterCH4: Integer;
         
      ControllerT: Integer;
      ControllerD: Integer;
      ActivationsBeforePump: Integer;  
      
      WaterHigh, WaterLow, WaterIncrease, WaterDecrease, WaterLevel: Float;
      
      ErrorWaterFlow, ErrorWaterHigh, ErrorWaterLow: Float;
           
      CH4Parameters: Sensor_Parameters;
      COParameters:  Sensor_Parameters;
      AirParameters: Sensor_Parameters;
   end record;
     
   function InitFromFile(filename: in String) return Project_Parameters;

end parameters;
