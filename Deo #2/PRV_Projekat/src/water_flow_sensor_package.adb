package body water_flow_sensor_package is

   overriding procedure Initialize(Self: in out WaterFlowSensor; ip: in InitializationPackage) is
   begin
      Initialize(Sensor(Self), ip);
      Self.PreviousWaterLevel := WaterLevel.GetWaterLevel;
      Self.Status := NO_WATER_FLOW;
   end;  
      
   function CalculateValueAndUpdate(Self: in out WaterFlowSensor) return WaterFlowStatus is
      CurrentWaterLevel: Float;
      CurrentWaterFlow: WaterFlowStatus;            
   begin
      
      CurrentWaterLevel := WaterLevel.GetWaterLevel;
      if CurrentWaterLevel > Self.PreviousWaterLevel then
         CurrentWaterFlow := WATER_INCREASING;
      elsif CurrentWaterLevel < Self.PreviousWaterLevel then
         CurrentWaterFlow := WATER_DECREASING;
      else
         CurrentWaterFlow := NO_WATER_FLOW;
      end if;
      
      Self.PreviousWaterLevel := CurrentWaterLevel;
      
      return CurrentWaterFlow;
   end;
   
      
   overriding function CreateResult(Self: in out WaterFlowSensor) return ResultPacket is
      res: ResultPacket;
   begin
      res.Result := 0.0;
      res.ID := Self.ID;
      res.SensorName := Self.actorName;
      res.waterStatus := Self.CalculateValueAndUpdate;      
      
      return res;
   end;
         
     
end water_flow_sensor_package;
