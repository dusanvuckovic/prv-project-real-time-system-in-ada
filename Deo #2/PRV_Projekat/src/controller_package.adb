with Ada.Text_IO;
package body controller_package is

   overriding procedure Initialize(Self: in out Controller; ip: InitializationPackage) is
   begin
      Self.systemStartTime := ip.SystemStartTime;
      Self.actorName := To_Unbounded_String("C");
      Self.ActivationsBeforePump := ip.ActivationsBeforePump;
   end Initialize;


   procedure ResetError(Self: in out Controller; ID: in SensorID) is
   begin
      Self.errors(ID) := False;
   end;
   procedure SetError(Self: in out Controller; ID: in SensorID) is
   begin
      Self.errors(ID) := True;
   end;
   function IsError(Self: in Controller; ID: in SensorID) return Boolean is
   begin
      return Self.errors(ID);
   end;


   function AreQueriesRemaining(Self: in Controller) return boolean is
   begin
      return Self.RemainingQueries /= 0;
   end AreQueriesRemaining;

   procedure ResetQueries(Self: in out Controller) is
   begin
      Self.RemainingQueries := TotalQueries;
   end;
   function QueriesRemaining(Self: in Controller) return NumOfQueries is
   begin
      return Self.RemainingQueries;
   end;

   procedure ProcessedQuery(Self: in out Controller) is
   begin
      Self.RemainingQueries := Self.RemainingQueries - 1;
   end;

   procedure NoQueriesRemaining(Self: in out Controller) is
   begin
      Self.RemainingQueries := 0;
   end;

end controller_package;
