package body water is
   
   protected body WaterLevel is

      function GetWaterLevel return Float is
      begin
         return CurrentWaterLevel;
      end;
   
      procedure IncreaseWater is
      begin
         CurrentWaterLevel := CurrentWaterLevel + WaterIncrease;
      end;
      procedure DecreaseWater is 
      begin 
         CurrentWaterLevel := CurrentWaterLevel - WaterDecrease;   
      end;
      procedure SetWaterParameters(ParWaterLevel, ParWaterIncrease, ParWaterDecrease: in Float) is
      begin
         StartWaterLevel := ParWaterLevel;
         CurrentWaterLevel := ParWaterLevel;
         WaterIncrease := ParWaterIncrease;
         WaterDecrease := ParWaterDecrease;
      end;
      procedure Reset is
      begin
         CurrentWaterLevel := StartWaterLevel;
      end;
         
       
      
   end WaterLevel;

end water;
