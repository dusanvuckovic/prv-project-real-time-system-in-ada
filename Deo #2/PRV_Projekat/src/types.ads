with Ada.Real_Time; use Ada.Real_Time;
with Ada.Strings.Unbounded;   use Ada.Strings.Unbounded;
with parameters; use parameters;
with Gtk.Scale; use Gtk.Scale;


package types is

   subtype ActorString is Unbounded_String;
      
   type SensorID is (CH4_SENSOR, CO_SENSOR, AIR_SENSOR, WATER_HIGH_SENSOR, WATER_LOW_SENSOR, WATER_FLOW_SENSOR);
   type SensorStatus is (OK, MISMEASURED, TOO_SOON);
   type SensorState is (UNINITIALIZED, IDLE);
   type PumpState is (UNINITIALIZED, INACTIVE, WAIT_FOR_ACTIVE, ACTIVE, WAIT_FOR_INACTIVE);
   type WaterFlowStatus is (NO_WATER_FLOW, WATER_INCREASING, WATER_DECREASING);

   
   type StatusPacket is record
      Status: SensorStatus;
      StatusDescription: ActorString;
      ID: SensorID;
      sensorName: ActorString;
   end record;
   
   type ResultPacket is record
      Result: Float;
      ID: SensorID;
      SensorName: ActorString;
      waterStatus: WaterFlowStatus;
   end record;
   
   type InitializationPackage is record
      
      SystemStartTime: Time;
      SensorPars: Sensor_Parameters;
      ActorName: ActorString;      
      SensID: SensorID;
      
      PumpErrorRate: Float;
      PumpWaitTime: Integer;
      
      ControllerT, ControllerD, ActivationsBeforePump: Integer;
      
      CH4Threshold, COThreshold, AirThreshold: Float;
      
      CH4Scale, COScale, AirScale: access Gtk_Scale;
      
    
           
      --Attribute systemStartTime: int64
                        
      --Attribute sensorID: SensorID
      --Attribute sensorPError: float32
      --Attribute sensorTimeToMeasure: int32
      --Attribute sensorThreshold: float32
      --Attribute sensorLowerBound: float32
      --Attribute sensorUpperBound: float32
      --Attribute sensorName: string

      --Attribute maxTimeToAlarmAfterCH4: int64
      --Attribute maxTimeToClosePumpAfterCH4: int64

      --Attribute COThreshold: float32
      --Attribute CH4Threshold: float32
      --Attribute airThreshold: float32
      
   end record;
   

end types;
