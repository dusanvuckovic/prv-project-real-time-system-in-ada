package body guidata is

   function Link_GUI_Objects_With_XML (filename : in String) return GUI_Data is

      Builder : Gtkada_Builder;

      CH4Sensor, COSensor, AirSensor : GUI_Sensor;
      GUI_Objects                    : GUI_Data;

      Win   : Gtk_Window;
      Error : aliased GError;
      Value : Guint;
   begin
      Gtk_New (Builder);
      Value := Builder.Add_From_File (filename, Error'Access);
      Win   := Gtk_Window (Builder.Get_Object ("myWindow"));

      CH4Sensor.Critical := Gtk_Label (Builder.Get_Object ("CH4Critical"));
      CH4Sensor.Low      := Gtk_Label (Builder.Get_Object ("CH4Low"));
      CH4Sensor.High     := Gtk_Label (Builder.Get_Object ("CH4High"));
      CH4Sensor.Error    := Gtk_Label (Builder.Get_Object ("CH4Error"));
      CH4Sensor.Scale    := Gtk_Scale (Builder.Get_Object ("CH4Scale"));

      COSensor.Critical := Gtk_Label (Builder.Get_Object ("COCritical"));
      COSensor.Low      := Gtk_Label (Builder.Get_Object ("COLow"));
      COSensor.High     := Gtk_Label (Builder.Get_Object ("COHigh"));
      COSensor.Error    := Gtk_Label (Builder.Get_Object ("COError"));
      COSensor.Scale    := Gtk_Scale (Builder.Get_Object ("COScale"));

      AirSensor.Critical := Gtk_Label (Builder.Get_Object ("AirCritical"));
      AirSensor.Low      := Gtk_Label (Builder.Get_Object ("AirLow"));
      AirSensor.High     := Gtk_Label (Builder.Get_Object ("AirHigh"));
      AirSensor.Error    := Gtk_Label (Builder.Get_Object ("AirError"));
      AirSensor.Scale    := Gtk_Scale (Builder.Get_Object ("AirScale"));

      GUI_Objects.CH4_Sensor := CH4Sensor;
      GUI_Objects.CO_Sensor  := COSensor;
      GUI_Objects.Air_Sensor := AirSensor;

      GUI_Objects.StartButton :=
        Gtk_Button (Builder.Get_Object ("StartButton"));
      GUI_Objects.ResetButton :=
        Gtk_Button (Builder.Get_Object ("ResetButton"));
      GUI_Objects.StopButton := Gtk_Button (Builder.Get_Object ("StopButton"));
      GUI_Objects.LoadButton := Gtk_Button (Builder.Get_Object ("LoadButton"));

      GUI_Objects.WaterLevel := Gtk_Label (Builder.Get_Object ("WaterLevel"));
      GUI_Objects.WaterIncreaseDecreaseRange :=
        Gtk_Label (Builder.Get_Object ("WaterIncreaseDecreaseRange"));
      GUI_Objects.ControllerT :=
        Gtk_Label (Builder.Get_Object ("ControllerT"));
      GUI_Objects.ControllerN :=
        Gtk_Label (Builder.Get_Object ("ControllerN"));

      GUI_Objects.WaterLowCritical :=
        Gtk_Label (Builder.Get_Object ("WaterLowCritical"));
      GUI_Objects.WaterHighCritical :=
        Gtk_Label (Builder.Get_Object ("WaterHighCritical"));
      GUI_Objects.WaterLowSwitch :=
        Gtk_Switch (Builder.Get_Object ("WaterLowSwitch"));
      GUI_Objects.WaterHighSwitch :=
        Gtk_Switch (Builder.Get_Object ("WaterHighSwitch"));
      GUI_Objects.WaterLowHighError :=
        Gtk_Label (Builder.Get_Object ("WaterLowHighError"));

      GUI_Objects.PumpTimeToStart :=
        Gtk_Label (Builder.Get_Object ("PumpTimeToStart"));
      GUI_Objects.PumpErrorRate :=
        Gtk_Label (Builder.Get_Object ("PumpErrorRate"));
      GUI_Objects.PumpSwitch := Gtk_Switch (Builder.Get_Object ("PumpSwitch"));

      GUI_Objects.Log := Gtk_Text_View (Builder.Get_Object ("log"));

      GUI_Objects.MainWindow := Gtk_Window(Builder.Get_Object("myWindow"));

      Win.Show_All;

      return GUI_Objects;
   end Link_GUI_Objects_With_XML;

   procedure Update_GUI_Sensor (sensor : out GUI_Sensor;
      pars :     parameters.Sensor_Parameters; element, unit : in String)
   is
   begin
      sensor.Low.Set_Text (Format (pars.Low));
      sensor.High.Set_Text (Format (pars.High));
      sensor.Critical.Set_Text
        (element & " (critical = " & Format (pars.Threshold) & " " & unit &
         ")");
      sensor.Error.Set_Text ("Error rate: " & FormatError (pars.PError) & "%");
      sensor.Scale.Set_Range
        (Min => Gdouble (pars.Low), Max => Gdouble (pars.High));
      sensor.Scale.Set_Fill_Level (Fill_Level => Gdouble (pars.High));
   end Update_GUI_Sensor;

   procedure Update_GUI_Objects (GUIdata : out GUI_Data;
      pars                               :     parameters.Project_Parameters)
   is
   begin
      Update_GUI_Sensor
        (sensor  => GUIdata.CH4_Sensor, pars => pars.CH4Parameters,
         element => "CH4", unit => "ppm");
      Update_GUI_Sensor
        (sensor  => GUIdata.CO_Sensor, pars => pars.COParameters,
         element => "CO", unit => "ppm");
      Update_GUI_Sensor
        (sensor  => GUIdata.Air_Sensor, pars => pars.AirParameters,
         element => "Air", unit => "ppm");

      GUIdata.WaterLevel.Set_Text
        ("Water level = " & Format (pars.WaterLevel));
      GUIdata.WaterIncreaseDecreaseRange.Set_Text
        ("Increase rate = " & Format (pars.WaterIncrease) &
         ", decrease rate = " & Format (pars.WaterDecrease) & ", range = [" &
         Format (pars.WaterLow) & ", " & Format (pars.WaterHigh) & "]");

      GUIdata.ControllerT.Set_Text
        ("Controller T = " & Format (pars.ControllerT) & ", Controller D = " & Format(pars.ControllerD));
      GUIdata.ControllerN.Set_Text
        ("Controller N = " & Format (pars.ActivationsBeforePump));

      GUIdata.WaterLowCritical.Set_Text
        ("Low (" & Format (pars.WaterLow) & " ppm)");
      GUIdata.WaterHighCritical.Set_Text
        ("High (" & Format (pars.WaterHigh) & " ppm)");
      GUIdata.WaterLowHighError.Set_Text
        ("Error rates: low = " & FormatError (pars.ErrorWaterLow) & ", " &
           "high = " & FormatError (pars.ErrorWaterHigh));
   end Update_GUI_Objects;

end guidata;
