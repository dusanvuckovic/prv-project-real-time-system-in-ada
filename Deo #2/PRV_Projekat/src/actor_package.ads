with Ada.Real_Time; use Ada.Real_Time;
with Ada.Strings.Unbounded.Text_IO; use Ada.Strings.Unbounded.Text_IO;
with Ada.Strings.Unbounded;   use Ada.Strings.Unbounded;
with logger;        use logger;
with formatter; use formatter;
with types; use types;


package actor_package is
      
   type Actor is abstract tagged limited record
      actorName: ActorString;
      systemStartTime: Time;
   end record;
   
   procedure Initialize(Self: in out Actor; ip: InitializationPackage) is abstract;
   procedure Log(Self: Actor; str: String; pri: Priority);
      
end actor_package;
