with Gtk.Label; use Gtk.Label;
with Gtk.Scale; use Gtk.Scale;
with parameters; use parameters;

package GUISensor is

   type GUI_Sensor is
      record
         Critical, Error:  Gtk_Label;
         Low, High: Gtk_Label;
         Scale:     aliased Gtk_Scale;
         
      
      end record;
   

end GUISensor;
