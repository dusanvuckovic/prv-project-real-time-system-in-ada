with Ada.Real_Time; use Ada.Real_Time;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Float_Random; use Ada.Numerics.Float_Random;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with guidata; use guidata;
with Gtk.Main;
with Gdk.Threads;
with Gtk.Window; use Gtk.Window;

with parameters; use parameters;
with logger; use logger;
with types; use types;

with builder_cb; use builder_cb;

with processes; use processes;
with control_package; use control_package;



procedure prv is
   --ip: InitializationPackage;


begin
   Gdk.Threads.G_Init;
   Gdk.Threads.Init;
   Gtk.Main.Init;

   pars := InitFromFile("config.txt");
   gdata := Link_GUI_Objects_With_XML("PRV.glade");
   guidata.Update_GUI_Objects(GUIdata => gdata,
                              pars    => pars);
   LogType.Init(gdata.Log);

   gdata.PumpSwitch.On_State_Set(Call  => builder_cb.Pump_Switched'Access,
                                 After => False);

   gdata.StartButton.On_Clicked(Call => builder_cb.Start_Clicked'Access);
   gdata.StopButton.On_Clicked(Call => builder_cb.Stop_Clicked'Access);
   gdata.LoadButton.On_Clicked(Call => builder_cb.Load_Clicked'Access);
   gdata.ResetButton.On_Clicked(Call => builder_cb.Reset_Clicked'Access);

   gdata.WaterHighSwitch.On_State_Set(Call  => builder_cb.WaterHigh_Switched'Access,
                                      After => False);
   gdata.WaterLowSwitch.On_State_Set(Call  => builder_cb.WaterLow_Switched'Access,
                                      After => False);


   --ip.CH4Scale := gdata.CH4_Sensor.Scale'Unchecked_Access;
   -- ip.COScale := guidata.GUI_Data.CO_Sensor.Scale;
   --   ip.AirScale := guidata.GUI_Data.Air_Sensor.Scale;

   --tp := Create_Tasks;
   --Initialize_Tasks(tp, pars);

   Gdk.Threads.Enter;
   Gtk.Main.Main;
   Gdk.Threads.Leave;

end Prv;
