package body logger is

   protected body LogType is  
      
      procedure Init(log: Gtk_Text_View) is begin
         Ada.Text_IO.Open(Name => "log-all.txt",
                          Mode => Ada.Text_IO.Out_File,
                          File => log_file);
         Ada.Text_IO.Open(Name => "log-significant.txt",
                          Mode => Ada.Text_IO.Out_File,
                          File => significant_file);
         Ada.Text_IO.Open(Name => "log-severe.txt",
                          Mode => Ada.Text_IO.Out_File,
                          File => severe_file);    
         myLog := log;
      end Init;
      
      --entry Log(text: in String; pri: in Priority) when True is
      --begin
      -- Log(To_Unbounded_String(text), pri);              
      --end Log;
      
      procedure Log(text: in Unbounded_String; pri: in Priority) is
      begin
         LogToFile(text, log_file);
         if (pri >= 2) then
            LogToFile(text, significant_file);
         end if;
         if (pri = 3) then
            LogToFile(text, severe_file);
         end if;
         if (pri >= GUILogPriority) then
            LogToGUI(text);              
         end if;
      end Log;
      
      procedure Finish is
      begin
         Ada.Text_IO.Close(log_file);
         Ada.Text_IO.Close(significant_file);
         Ada.Text_IO.Close(severe_file);
         null;
      end Finish;     
      
      procedure LogToFile(text: in Unbounded_String; file: File_Type) is
      begin
         Ada.Text_IO.Put_Line(File => file,
                              Item => To_String(text));
      end LogToFile;
   
      procedure LogToGUI(text: in Unbounded_String) is
         buffer: Gtk_Text_Buffer;
         it:     Gtk_Text_Iter;
      begin
         Gdk.Threads.Enter;   
         buffer := myLog.Get_Buffer;
         buffer.Get_Start_Iter(it);
         buffer.Insert(Iter => it,
                       Text => To_String(text) & ASCII.LF);
         Gdk.Threads.Leave;
      end LogToGUI;
      
      --severe_file, significant_file, log_file: File_Type;
      --myLog: Gtk_Text_View;
   end LogType;

end logger;
