with Gtk.Widget;  use Gtk.Widget;
with Gtk.Button; use Gtk.Button;
with Gdk.Threads;
with Ada.Text_IO; use Ada.Text_IO;
with Gtk.Main;
with Gtk.Switch; use Gtk.Switch;
with Gtk.Window; use Gtk.Window;
with processes; use processes;
with parameters; use parameters;
with control_package; use control_package;
with guidata; use guidata;
with types; use types;
with Glib; use Glib;

package builder_cb is

   
   
   tp: TaskPackage;
   pars: Project_Parameters;  
   gdata: aliased GUI_Data;

      
   function Pump_Switched(Self: access Gtk_Switch_Record'Class; State : Boolean) return Boolean;
   function WaterHigh_Switched(Self: access Gtk_Switch_Record'Class; State : Boolean) return Boolean;
   function WaterLow_Switched(Self: access Gtk_Switch_Record'Class; State : Boolean) return Boolean;
   
   procedure Start_Clicked(Self: access Gtk_Button_Record'Class);
   procedure Stop_Clicked(Self: access Gtk_Button_Record'Class);
   procedure Reset_Clicked(Self: access Gtk_Button_Record'Class);
   procedure Load_Clicked(Self: access Gtk_Button_Record'Class);
   
end builder_cb; 
