import os

top = os.getcwd()
lines = 0
for root, dirs, files in os.walk(top):
	for f in files:
		filepath = os.path.join(root, f)
		if filepath.endswith('.adb') or filepath.endswith('ads'):
			print(filepath)
			file = open(filepath, encoding='utf-8')
			lines += len(file.readlines())
			file.close()
print("lines = {}".format(lines))
