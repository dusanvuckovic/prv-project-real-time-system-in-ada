with Ada.Real_Time; use Ada.Real_Time;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with types;      use types;
with processes;  use processes;
with parameters; use parameters;
with water;      use water;


package control_package is
   
   function Create_Tasks return TaskPackage;
   procedure Initialize_Tasks(tp: in TaskPackage; pars: in Project_Parameters);     
   procedure Stop_Tasks(tp: in TaskPackage);
   

end control_package;
