with polling_sensor_package; use polling_sensor_package;
with sensor_core_package; use sensor_core_package;
with types; use types;
with Ada.Numerics.Float_Random;
with water; use water;


package water_flow_sensor_package is
   
   FunctionNotSupported: exception;

   type WaterFlowSensor is new Sensor with record
      Status: WaterFlowStatus;
      PreviousWaterLevel: Float;
   end record;
   
   
   overriding procedure Initialize(Self: in out WaterFlowSensor; ip: in InitializationPackage);   
   function CalculateValueAndUpdate(Self: in out WaterFlowSensor) return WaterFlowStatus;
   overriding function CreateResult(Self: in out WaterFlowSensor) return ResultPacket;
   
   
end water_flow_sensor_package;
