package body builder_cb is
  
   
   function Pump_Switched(Self: access Gtk_Switch_Record'Class; State : Boolean) return Boolean is
   begin
      
      Gdk.Threads.Enter;
      if Self.Get_Active then
         tp.Pump.ActivatePump;
      else
         tp.Pump.DeactivatePump;
      end if;
      Gdk.Threads.Leave;
      
      return True;     
   end;
   
   function WaterHigh_Switched(Self: access Gtk_Switch_Record'Class; State : Boolean) return Boolean is
   begin
      
      --Gdk.Threads.Enter;
      --if Self.Get_Active then
         tp.interrupts(WATER_HIGH_SENSOR).SetActive;
      --else
     --    tp.interrupts(WATER_HIGH_SENSOR).SetInactive;
     -- end if; 
      --Gdk.Threads.Leave;

      
      return True;     
   end;
   
   function WaterLow_Switched(Self: access Gtk_Switch_Record'Class; State : Boolean) return Boolean is
   begin
      
      if Self.Get_Active then
         Put_Line ("LOW ON");
      else
         Put_Line("LOW OFF");
      end if;
      
      return True;     
   end;
   
   procedure Start_Clicked(Self: access Gtk_Button_Record'Class) is
   begin
      gdata.StartButton.Set_Sensitive(False);
      gdata.ResetButton.Set_Sensitive(False);
      gdata.LoadButton.Set_Sensitive(False);
      tp := Create_Tasks;
      Initialize_Tasks(tp, pars);
      gdata.StopButton.Set_Label("STOP");      
      gdata.StopButton.Set_Sensitive(True);     
   end;
   
   procedure Stop_Clicked(Self: access Gtk_Button_Record'Class) is
   begin      
      if (Self.Get_Label = "STOP") then         
         Self.Set_Label("EXIT");
         
         Stop_Tasks(tp);
            
         gdata.StartButton.Set_Sensitive(True);
         gdata.ResetButton.Set_Sensitive(True);
         gdata.LoadButton.Set_Sensitive(True);         
      else
         gdata.MainWindow.Destroy;
         Gtk.Main.Main_Quit;
      end if;
   end;
   
   procedure Load_Clicked(Self: access Gtk_Button_Record'Class) is
   begin
      pars := parameters.InitFromFile("config.txt");
      guidata.Update_GUI_Objects(GUIdata => gdata,
                                 pars    => pars);
   end;
   
   procedure Reset_Clicked(Self: access Gtk_Button_Record'Class) is
   begin
      pars := parameters.InitFromFile("config_default.txt");
      guidata.Update_GUI_Objects(GUIdata => gdata,
                                 pars    => pars);
   end;

end builder_cb; 
