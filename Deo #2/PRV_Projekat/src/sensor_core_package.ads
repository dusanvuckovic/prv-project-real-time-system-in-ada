with actor_package; use actor_package;
with Ada.Numerics.Float_Random; use Ada.Numerics.Float_Random;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with types; use types;

package sensor_core_package is
      
   type Sensor is abstract new Actor with record
      PError: Float;
      Gen: Ada.Numerics.Float_Random.Generator;
      TimeToMeasure: Duration;
      ID: SensorID;
      
   end record;      
      

   overriding procedure Initialize(Self: in out Sensor; ip: in InitializationPackage);
   function CreateResult(Self: in out Sensor) return ResultPacket is abstract;
   function CreateStatus(Self: in Sensor; Status: in SensorStatus) return StatusPacket;
   function HasErrorHappened(Self: in Sensor) return Boolean; 

end sensor_core_package;
