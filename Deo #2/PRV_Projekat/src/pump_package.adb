package body pump_package is

   overriding procedure Initialize(Self: in out Pump; ip: InitializationPackage) is
   begin
      Ada.Numerics.Float_Random.Reset(Gen => Self.Gen);           
      Self.systemStartTime := ip.systemStartTime;      
      Self.actorName := To_Unbounded_String("PUMP");
      Self.PumpErrorRate := ip.PumpErrorRate;
      Self.PumpWaitTime := Milliseconds(ip.PumpWaitTime);
   end Initialize;
   
   function HasErrorHappened(Self: in Pump) return Boolean is
      RandomNum: Float;
   begin
      RandomNum := Ada.Numerics.Float_Random.Random(Self.Gen);
      return Self.PumpErrorRate > RandomNum;   
   end HasErrorHappened;

end pump_package;
