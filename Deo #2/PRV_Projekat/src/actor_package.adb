package body actor_package is
   
   procedure Log(Self: Actor; str: String; pri: Priority) is 
      elapsedTime: Time_Span;
      elapsedTimeD: Integer;
   
   begin
      elapsedTime := Ada.Real_Time.Clock - Self.systemStartTime;
      elapsedTimeD := Integer(Ada.Real_Time.To_Duration(elapsedTime) * 1000);
      LogType.Log(text => To_Unbounded_String("[" 
                  & Format(elapsedTimeD) & " ms][" 
                  & To_String(Self.actorName) & "] - " & str),
                  pri  => pri);           
   end Log;
   
end actor_package;
