package body control_package is

   
   function Create_Tasks return TaskPackage is
      tp: TaskPackage;
   begin     
      tp.sensors(CH4_SENSOR) := new SensorProcess;
      tp.sensors(CO_SENSOR) := new SensorProcess;
      tp.sensors(AIR_SENSOR) := new SensorProcess;
      tp.sensors(WATER_FLOW_SENSOR) := new SensorProcess;
      tp.interrupts(WATER_LOW_SENSOR) := new InterruptProcess;
      tp.interrupts(WATER_HIGH_SENSOR) := new InterruptProcess;

      tp.Pump := new PumpProcess;
      tp.Controller := new ControllerProcess;      
      return tp;
            
   end;
   
   procedure Initialize_Tasks(tp: in TaskPackage; pars: in Project_Parameters) is
      ip: InitializationPackage;      
   begin      
      WaterLevel.SetWaterParameters(ParWaterLevel    => pars.WaterLevel,
                                    ParWaterIncrease => pars.WaterIncrease,
                                    ParWaterDecrease => pars.WaterDecrease);
      
      ip.systemStartTime := Ada.Real_Time.Clock;
      ip.controllerT := pars.ControllerT;
      ip.controllerD := pars.ControllerD;
      ip.activationsBeforePump := pars.ActivationsBeforePump;

      ip.sensID := CH4_SENSOR;
      ip.actorName := To_Unbounded_String("CH4");
      ip.sensorPars := pars.CH4Parameters;
      tp.sensors(CH4_SENSOR).Initialize(ip, tp);

      ip.sensID := CO_SENSOR;
      ip.actorName := To_Unbounded_String("CO");
      ip.sensorPars := pars.COParameters;
      tp.sensors(CO_SENSOR).Initialize(ip, tp);

      ip.sensID := AIR_SENSOR;
      ip.actorName := To_Unbounded_String("AIR");
      ip.sensorPars := pars.AirParameters;
      tp.sensors(AIR_SENSOR).Initialize(ip, tp);

      ip.sensID := WATER_FLOW_SENSOR;
      ip.actorName := To_Unbounded_String("WaterFlow");
      ip.sensorPars.PError := pars.ErrorWaterFlow;
      ip.sensorPars.Low := 0.0;
      ip.sensorPars.High := 1.0;
      ip.sensorPars.Threshold := 2.0;
      ip.sensorPars.TimeToMeasure := 150;
      tp.sensors(WATER_FLOW_SENSOR).Initialize(ip, tp);

      ip.sensID := WATER_LOW_SENSOR;
      ip.actorName := To_Unbounded_String("WaterLow");
      ip.sensorPars.Threshold := pars.WaterLow;
      ip.sensorPars.TimeToMeasure := 150;
      tp.interrupts(WATER_LOW_SENSOR).Initialize(ip, tp);

      ip.sensID := WATER_HIGH_SENSOR;
      ip.actorName := To_Unbounded_String("WaterHigh");
      ip.sensorPars.Threshold := pars.WaterHigh;
      ip.sensorPars.TimeToMeasure := 150;
      tp.interrupts(WATER_HIGH_SENSOR).Initialize(ip, tp);

      ip.actorName := To_Unbounded_String("Pump");
      ip.PumpErrorRate := pars.PumpErrorRate;
      ip.PumpWaitTime := pars.PumpWaitTime;
      tp.Pump.Initialize(ip, tp);

      ip.CH4Threshold := pars.CH4Parameters.Threshold;
      ip.COThreshold := pars.COParameters.Threshold;
      ip.AirThreshold := pars.AirParameters.Threshold;
      tp.Controller.Initialize(ip, tp);      
   end;
   
   procedure Stop_Tasks(tp: in TaskPackage) is
      
   begin
      abort tp.Controller.all;   
      abort tp.Pump.all;
      abort tp.sensors(CH4_SENSOR).all;
      abort tp.sensors(CO_SENSOR).all;  
      abort tp.sensors(AIR_SENSOR).all;
      abort tp.sensors(WATER_FLOW_SENSOR).all;
      abort tp.interrupts(WATER_HIGH_SENSOR).all;
      abort tp.interrupts(WATER_LOW_SENSOR).all;
      WaterLevel.Reset;
   end;
      

end control_package;
