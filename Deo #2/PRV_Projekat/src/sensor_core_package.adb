with Ada.Text_IO; use Ada.Text_IO;
with formatter; use formatter;
package body sensor_core_package is

   overriding procedure Initialize(Self: in out Sensor; ip: in InitializationPackage) is
   begin
      Ada.Numerics.Float_Random.Reset(Gen => Self.Gen);      
      Self.PError := ip.sensorPars.PError;
      Self.actorName := ip.actorName;
      Self.systemStartTime := ip.systemStartTime;
      Self.ID := ip.sensID;
   end;
   
   function HasErrorHappened(Self: in Sensor) return Boolean is
      RandomNum: Float;
   begin
      RandomNum := Ada.Numerics.Float_Random.Random(Self.Gen);
      return Self.PError > RandomNum;   
   end;
      
   function CreateStatus(Self: in Sensor; Status: in SensorStatus) return StatusPacket is
      stat: StatusPacket;
   begin
      stat.status := Status;
      stat.ID := Self.ID;
      stat.sensorName := Self.actorName;
      case Status is
         when OK => stat.statusDescription := To_Unbounded_String("OK");
         when MISMEASURED => stat.statusDescription := To_Unbounded_String("MISMEASURED");
         when TOO_SOON => stat.statusDescription := To_Unbounded_String("TOO_SOON");
      end case;
      return stat;              
   end;

end sensor_core_package;
