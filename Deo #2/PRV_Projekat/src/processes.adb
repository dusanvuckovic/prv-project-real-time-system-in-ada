with Ada.Text_IO;
package body processes is
   
   task body ControllerProcess is
      cont: Controller;
      pump: access PumpProcess;
      sensors: SensorArray;
      state: Controller_State := Uninitialized;
      NextRelease, NextDeadline: Time;
      stp: StatusPacket;
      WaterStatus : WaterFlowStatus;
      CH4Threshold, COThreshold, AirThreshold: Float;
      SensResult: Float := 0.0;
      CH4Result: Float := 0.0;
      sensID: SensorID;
      ShouldWork: Boolean := True;
      DeadlineExceeded: Boolean := False;    
      
      PumpInactive: Boolean := True;
      PumpActive : Boolean := False;
      
      --CH4Scale, COScale, AirScale: access Gtk_Scale;
      CH4Scale: access Gtk_Scale;
      
      ReleaseInterval: Time_Span;
      DeadlineInterval: Time_Span;
   begin
      NextRelease := Clock;
      NextDeadline := Clock;
      Main_Loop:
      while ShouldWork loop
         case state is
            when Uninitialized =>
               accept Initialize(ip: InitializationPackage; tp: TaskPackage) do
                  cont.Initialize(ip);
                  CH4Threshold := ip.CH4Threshold;
                  COThreshold := ip.COThreshold;
                  AirThreshold := ip.AirThreshold;
                  
                  ReleaseInterval := Milliseconds(ip.ControllerT);
                  DeadlineInterval := Milliseconds(ip.ControllerD);
                  NextRelease := NextRelease + ReleaseInterval;
                  NextDeadline := NextDeadline + DeadlineInterval;
                  
                  CH4Scale := ip.CH4Scale;
                  --COScale := ip.COScale;
                  --AirScale := ip.AirScale;
                  sensors := tp.sensors;
                  pump := tp.pump;
                  cont.Log("Initialized!", 1);
                  cont.ResetQueries;
                  state := Initialized;

               end Initialize;              
   
            when Initialized =>               
               cont.Log("Sent measurements!", 1);
               
               sensors(CH4_SENSOR).RequestStatus;
               sensors(CO_SENSOR).RequestStatus;
               sensors(AIR_SENSOR).RequestStatus;
               sensors(WATER_FLOW_SENSOR).RequestStatus;
               pump.PollPump;
               
               select
                  accept Stop  do
                     ShouldWork := False;
                  end Stop;
                  exit Main_Loop;
               or
                  accept SensorInterrupt (id : in SensorID) do
                     case id is
                        when WATER_HIGH_SENSOR =>                           
                           cont.Log("WATER IS HIGH!", 3);
                           if PumpInactive and cont.PumpStatusKnown then
                              cont.Log("Activating pump!", 3);
                              pump.ActivatePump;
                              cont.TriedActivatingPump := True;
                              cont.PumpRequestStartTime := Clock;                                
                              cont.PumpStatusKnown := False;
                           end if;
                        when WATER_LOW_SENSOR =>
                           cont.Log("WATER IS LOW!", 3);
                           if PumpActive and cont.PumpStatusKnown then
                              cont.Log("Deactivating pump!", 3);
                              pump.DeactivatePump;
                              cont.TriedDeactivatingPump := True;
                              cont.PumpRequestStartTime := Clock;                                
                              cont.PumpStatusKnown := False;                                
                           end if;
                        when others => null;
                     end case;
                  end SensorInterrupt;
               
               or
                  delay To_Duration(Milliseconds(10));
               end select;
               
               while (cont.AreQueriesRemaining) loop
                  select               
                     accept ReturnStatus (statp : in StatusPacket) do
                        cont.Log("Accepted returnStatus from " & To_String(statp.sensorName), 1);                        
                        stp := statp;                      
                     end ReturnStatus; 
                     cont.Log("Out of returnStatus from " & To_String(stp.sensorName), 1);
                     case stp.status is
                        when OK => 
                           cont.ResetError(stp.ID);
                           sensors(stp.ID).RequestResult;
                        when MISMEASURED => 
                           cont.Log("Mismeasured " & To_String(stp.sensorName) & "!", 3);
                           if cont.IsError(stp.ID) then
                              cont.Log("DOUBLE MISMEASURE!", 3);
                              cont.Log("ALARM ALARM ALARM", 3);
                              exit Main_Loop;
                           else 
                              cont.SetError(stp.ID);
                           end if;
                           cont.ProcessedQuery;                              
                        when TOO_SOON => 
                           cont.Log("TOO SOON " & To_String(stp.sensorName) & "!", 3);
                           cont.ProcessedQuery;
                     end case;
                  or 
                     accept ReturnResult (resp : in ResultPacket) do
                        cont.Log("Accepted returnResult from " & To_String(resp.sensorName), 2);
                        if (resp.id = WATER_FLOW_SENSOR) then
                           cont.Log(To_String(resp.sensorName) & " result is " & resp.waterStatus'Image, 2);
                           WaterStatus := resp.WaterStatus;
                        else  
                           cont.Log(To_String(resp.sensorName) & " result is " & Format(resp.result), 2);
                           sensID := resp.id;
                           sensResult := resp.Result;
                        end if;                      
                     end ReturnResult;
                     cont.Log("Out of ReturnResult", 1);
                     cont.ProcessedQuery;                     
                     case sensID is
                        when CH4_SENSOR => 
                           cont.Log("In CH4Sensor", 1);                           
                           CH4Result := SensResult;
                           --                         Gtk.Scale.
                           --                           Set_Value(CH4Scale, CH4Result, Get_Range(CH4Scale));
                           if sensResult >= CH4Threshold and cont.PumpStatusKnown and WaterStatus = WATER_DECREASING then
                              cont.Log("CH4 critical and pump active, shutting down!", 3);
                              cont.Log("ALARM ALARM ALARM", 3);
                              exit Main_Loop;
                           end if;
                        when CO_SENSOR => 
                           cont.Log("In COSensor", 1);
                           if sensResult >= COThreshold then
                              cont.Log("CO critical, shutting down!", 3);
                              cont.Log("ALARM ALARM ALARM", 3);
                              exit Main_Loop;
                           end if;
                        when AIR_SENSOR => 
                           cont.Log("In AirSensor", 1);                           
                           if sensResult >= AirThreshold then
                              cont.Log("Air critical, shutting down!", 3);
                              cont.Log("ALARM ALARM ALARM", 3);
                              exit Main_Loop;
                           end if;
                        when others => null;
                     end case;               
                     cont.Log("Out of case", 2);
                     cont.Log("Val is " & cont.RemainingQueries'Image, 2);
                    
                  end select;                  
               end loop;

         end case;
         cont.Log("Water level is " & Format(WaterLevel.GetWaterLevel), 3);         
         sensors(CH4_SENSOR).StartMeasurement;
         sensors(CO_SENSOR).StartMeasurement;
         sensors(AIR_SENSOR).StartMeasurement;
         sensors(WATER_FLOW_SENSOR).StartMeasurement;
         
         if cont.TriedDeactivatingPump then
            cont.CurrentActivationsBeforePump := cont.CurrentActivationsBeforePump + 1;
            if (cont.CurrentActivationsBeforePump = cont.ActivationsBeforePump + 1) then
               cont.CurrentActivationsBeforePump := 0;
               if WaterStatus = WATER_INCREASING then
                  cont.Log("Pump checked - safely deactivated!", 3);
                  cont.Log("It took " & Format(Clock - cont.PumpRequestStartTime) & "ms!", 3);
                  cont.PumpStatusKnown := True;
                  PumpActive := False;
                  PumpInactive := True;
                  cont.TriedDeactivatingPump := False;
               else
                  cont.Log("Pump checked - not deactivated!", 3);
                  cont.Log("ALARM ALARM ALARM!", 3);
                  exit Main_Loop;
               end if;
            else
               cont.Log("Currently deactivating on " & 
                          format(cont.CurrentActivationsBeforePump) & "/" & 
                          format(cont.ActivationsBeforePump), 3);
            end if;
         end if;    
         
         if cont.TriedActivatingPump then
            cont.CurrentActivationsBeforePump := cont.CurrentActivationsBeforePump + 1;
            if CH4Result >= CH4Threshold then 
               cont.Log("Cannot activate pump, methane level too high!", 3);
               cont.Log("ALARM ALARM ALARM", 3);
               exit Main_Loop;
            end if;
            if (cont.CurrentActivationsBeforePump = cont.ActivationsBeforePump + 1) then
               cont.CurrentActivationsBeforePump := 0;
               if WaterStatus = WATER_DECREASING then
                  cont.Log("Pump checked - safely activated!", 3);
                  cont.Log("It took " & Format(Clock - cont.PumpRequestStartTime) & "ms!", 3);
                  cont.PumpStatusKnown := True;
                  PumpActive := True;
                  PumpInactive := False;
                  cont.TriedActivatingPump := False;
               else
                  cont.Log("Pump checked - not activated!", 3);
                  cont.Log("ALARM ALARM ALARM!", 3);
                  exit Main_Loop;
               end if;
            else
               cont.Log("Currently activating on " & 
                          Format(cont.CurrentActivationsBeforePump) & "/" & 
                          Format(cont.ActivationsBeforePump), 3);
            end if;
         end if;  
        
         if (Clock > NextDeadline) then
            cont.Log("Deadline overran!", 3);
         end if;

         cont.Log("Delayed!", 1);    
         delay until NextRelease;
         cont.Log("Restarted", 1);
         cont.ResetQueries;
         WaterLevel.IncreaseWater;
         NextRelease := NextRelease + ReleaseInterval;
         NextDeadline := NextRelease + DeadlineInterval;

      end loop Main_Loop;
   end ControllerProcess;
   
   task body SensorProcess is
      sens: access Sensor'Class;
      startedMeasuring: Time := Clock;
      elapsedTime: Time_Span := Milliseconds(0);
      timeToMeasure: Time_Span := Milliseconds(50);
      state: SensorState := Uninitialized;
      controller: access ControllerProcess;
      NotDone: Boolean := True;
      SentMeasurement : Boolean := False;
      ShouldWork: Boolean := True;
      
      
      NextRelease, NextDeadline: Time;
      ReleaseInterval: constant Time_Span := Milliseconds(50);
      DeadlineInterval: constant Time_Span := Milliseconds(40);
      
   begin
      NextRelease := Clock + ReleaseInterval;
      NextDeadline := Clock + DeadlineInterval;
      Main_Loop:
      while ShouldWork loop
         case state is
            when UNINITIALIZED =>
               accept Initialize (ip : in InitializationPackage; tp : in TaskPackage) do
                  case ip.sensID is
                     when CH4_SENSOR => sens := new PollingSensor;
                     when CO_SENSOR  => sens := new PollingSensor;
                     when AIR_SENSOR => sens := new PollingSensor;
                     when WATER_FLOW_SENSOR => sens := new WaterFlowSensor;                    
                     when WATER_HIGH_SENSOR => null;
                     when WATER_LOW_SENSOR  => null;
                  end case;
                  sens.Initialize(ip);                  
                  state := IDLE;
                  controller := tp.Controller;           
                  
                  sentMeasurement := False;
                  sens.Log("Initialized!", 2);
               end Initialize;
               accept StartMeasurement  do
                  startedMeasuring := Clock;
               end StartMeasurement;
               sens.Log("Accepted start measurement!", 3);

 
            when IDLE => 
               sens.Log("Entered idle!", 1);
                                                        
               while (notDone) loop  
                  select 
                     accept Stop  do
                        ShouldWork := False;
                     end Stop;
                     exit Main_Loop;
                  or
                     accept RequestStatus  do
                        null;
                     end RequestStatus;
                     elapsedTime := Clock - startedMeasuring;
                     sens.Log("Time elapsed: " & Format(Float(1000*To_Duration(elapsedTime))), 2);                     
                     if elapsedTime > timeToMeasure then
                        if sens.HasErrorHappened then
                           controller.ReturnStatus(sens.CreateStatus(MISMEASURED));
                           notDone := False;
                        else
                           controller.ReturnStatus(sens.CreateStatus(OK));
                        end if;
                     else
                        controller.ReturnStatus(sens.CreateStatus(TOO_SOON));
                        notDone := False;
                     end if;
                        
                  or 
                     accept RequestResult  do
                        null;
                     end RequestResult;   
                     sens.Log("Returned result!", 1);
                     --if (sens.ID = WATER_FLOW_SENSOR) then
                     -- controller.ReturnResult(sens.
                     controller.ReturnResult(sens.CreateResult);
                     notDone := False;
                  end select;
               end loop;
               
               accept StartMeasurement do
                  sens.Log("Accepted StartMeasurement!", 1);
                  --RequestedMeasurement := True;
                  startedMeasuring := Clock;
                  NotDone := True;
               end StartMeasurement; 

         end case;
         sens.Log("Delayed!", 1);
         delay until NextRelease;
         sens.Log("Restarted", 1);
         NextRelease := NextRelease + ReleaseInterval;
         NextDeadline := NextRelease + DeadlineInterval;
      end loop Main_Loop;      
   end SensorProcess;
   
   
   task body InterruptProcess is
      
      state: SensorState := Uninitialized;
      sens: access InterruptSensor'Class;
      controller: access ControllerProcess;
      
      ShouldWork: Boolean := True;
      IsActive: Boolean := False;
      
      NextRelease, NextDeadline: Time;
      ReleaseInterval: constant Time_Span := Milliseconds(50);
      DeadlineInterval: constant Time_Span := Milliseconds(40);
      
      CurrentWaterLevel: Float;
      
   begin
      NextRelease := Clock + ReleaseInterval;
      NextDeadline := Clock + DeadlineInterval;
      Main_Loop:
      while ShouldWork loop
         case state is
            when UNINITIALIZED =>
               accept Initialize (ip : in InitializationPackage; tp : in TaskPackage) do
                  case ip.sensID is
                     when CH4_SENSOR => sens := null;
                     when CO_SENSOR  => sens := null;
                     when AIR_SENSOR => sens := null;
                     when WATER_FLOW_SENSOR => sens := null;                    
                     when WATER_HIGH_SENSOR => sens := new WaterHighSensor;
                     when WATER_LOW_SENSOR  => sens := new WaterLowSensor;
                  end case;
                  sens.Initialize(ip);                  
                  controller := tp.Controller;           
                  
                  sens.Log("Initialized!", 2);
                  state := IDLE;   
               end Initialize;
                                              
            when IDLE =>
               
               CurrentWaterLevel := WaterLevel.GetWaterLevel;
               if sens.isPastThreshold(CurrentWaterLevel) or IsActive then
                  controller.SensorInterrupt(sens.ID);  
               end if;  
               select                  
                  accept Stop  do
                     ShouldWork := False;
                  end Stop;
                  exit Main_Loop;
               or
                  accept SetActive  do
                     --sens.Log("SET ACTIVE!", 3);
                     IsActive := True;
                  end SetActive;
               or 
                  accept SetInactive  do
                     --sens.Log("SET INACTIVE!", 3);
                     IsActive := False;
                  end SetInactive;                  
               else
                  null;
               end select;
         end case;
                       
         sens.Log("Delayed!", 1);
         delay until NextRelease;
         sens.Log("Restarted", 1);
            
         NextRelease := NextRelease + ReleaseInterval;
         NextDeadline := NextRelease + DeadlineInterval;
      end loop Main_Loop;      
   end InterruptProcess;
   
   task body PumpProcess is
      myPump: Pump;
      state: PumpState := UNINITIALIZED;
      StopLoop: Boolean := False;
      ShouldWork: Boolean := True;
      NextRelease, NextDeadline: Time;
      TimeToEnd: Time;
      ReleaseInterval: constant Time_Span := Milliseconds(50);
      DeadlineInterval: constant Time_Span := Milliseconds(40);
      
      PumpActivationTime, PumpDeactivationTime: Time;
      
   begin
      NextRelease := Clock + ReleaseInterval;
      NextDeadline := Clock + DeadlineInterval;   
      PumpActivationTime := Clock;
      PumpDeactivationTime := Clock;
      Main_Loop: 
      while ShouldWork loop
         
         if state = UNINITIALIZED then
            accept Initialize (ip : in InitializationPackage; tp : in TaskPackage) do
               myPump.Initialize(ip);
            end Initialize;
            state := INACTIVE;
         else
            TimeToEnd := Clock + Milliseconds(30);
            while not StopLoop loop
               select 
                  accept PollPump  do     
                     case state is
                                                      
                        when INACTIVE => 
                           myPump.log("Polling pump", 2);
                              
                              
                        when WAIT_FOR_ACTIVE =>                               
                           if (Clock - PumpActivationTime) > myPump.PumpWaitTime then
                              if myPump.HasErrorHappened then
                                 myPump.Log("Error: fault at activation!", 3);
                              else
                                 myPump.Log("Now active!", 3);
                                 WaterLevel.DecreaseWater;
                                 WaterLevel.DecreaseWater;
                                 state := ACTIVE;
                              end if;                                    
                           else
                              myPump.Log("Pump activating", 3);
                           end if;
                              
                              
                        when ACTIVE => 
                           WaterLevel.DecreaseWater;
                              
                              
                        when WAIT_FOR_INACTIVE => 
                           if (Clock - PumpDeactivationTime) > myPump.PumpWaitTime then
                              if myPump.HasErrorHappened then
                                 myPump.Log("Error: fault at deactivation!", 3);
                              else
                                 myPump.Log("Now inactive!", 3);
                                 WaterLevel.IncreaseWater;
                                 WaterLevel.IncreaseWater;
                                 state := INACTIVE;
                              end if;
                           else
                              myPump.Log("Pump deactivating", 3);
                           end if;
                           WaterLevel.DecreaseWater;
                              
                              
                        when UNINITIALIZED => null;
                     end case;
                  end PollPump;
               or 
                  accept ActivatePump  do
                     myPump.Log("Pump set to activate!", 3);
                     PumpActivationTime := Clock;
                     state := WAIT_FOR_ACTIVE;
                  end ActivatePump;

               or 
                  accept DeactivatePump  do
                     myPump.Log("Pump set to deactivate!", 3);
                     PumpDeactivationTime := Clock;
                     state := WAIT_FOR_INACTIVE;
                  end DeactivatePump;
               or
                  accept Stop  do
                     ShouldWork := False;
                  end Stop;
                  exit Main_Loop;
               or
                  delay until TimeToEnd;
                  StopLoop := True;
               end select;
            end loop;   
            StopLoop := False;
         end if;
         delay until NextRelease;
         NextRelease := NextRelease + ReleaseInterval;
         NextDeadline := NextRelease + DeadlineInterval;
      end loop Main_Loop;       
      
      
   end PumpProcess;

end processes;
