with Gtkada.Builder; use Gtkada.Builder;
with Gtk.Label;      use Gtk.Label;
with Gtk.Window;     use Gtk.Window;
with Gtk.Switch;     use Gtk.Switch;
with Gtk.Button;     use Gtk.Button;
with Gtk.Scale;      use Gtk.Scale;
with Gtk.Text_View;  use Gtk.Text_View;
with Glib.Error;     use Glib.Error;
with Glib;           use Glib;

with GUISensor; use GUISensor;
with parameters; use parameters;
with formatter;      use formatter;

package guidata is

   type GUI_Data is 
      record         
         CH4_Sensor: aliased GUI_Sensor;
         CO_Sensor: GUI_Sensor;
         Air_Sensor: GUI_Sensor;
         
         StartButton, StopButton, LoadButton, ResetButton: Gtk_Button;

         WaterLevel: Gtk_Label;
         WaterIncreaseDecreaseRange: Gtk_Label;
         
         ControllerT: Gtk_Label;
         ControllerN: Gtk_Label;
                           
         WaterLowCritical, WaterHighCritical: Gtk_Label;
         WaterLowSwitch,   WaterHighSwitch  : Gtk_Switch;
         WaterLowHighError: Gtk_Label;
         
         PumpTimeToStart, PumpErrorRate: Gtk_Label;
         PumpSwitch: Gtk_Switch;
         
         Log: Gtk_Text_View;
                          
      end record;  
   
   function Link_GUI_Objects_With_XML(filename: in String) return GUI_Data;
   procedure Update_GUI_Objects(GUIdata: out GUI_Data; pars: parameters.Project_Parameters);
   
private
   procedure Update_GUI_Sensor(sensor: out GUI_Sensor;  pars: parameters.Sensor_Parameters; 
                               element, unit: in String);

end guidata;
