with types; use types;
with sensor_core_package; use sensor_core_package;

package interrupt_sensor_package is
   
   BadOperation: exception;

   type InterruptSensor is abstract new Sensor with record
      Threshold: Float;
   end record;
   
   overriding procedure Initialize(Self: in out InterruptSensor; ip: in InitializationPackage);   
   function IsPastThreshold(Self: InterruptSensor; CurrentWaterLevel: Float) return Boolean is abstract;    
   overriding function CreateResult(Self: in out InterruptSensor) return ResultPacket;
   
   type WaterHighSensor is new InterruptSensor with null record;
   overriding function IsPastThreshold(Self: WaterHighSensor; CurrentWaterLevel: Float) return Boolean;    
   
   type WaterLowSensor is new InterruptSensor with null record;   
   overriding function IsPastThreshold(Self: WaterLowSensor; CurrentWaterLevel: Float) return Boolean;    
   


end interrupt_sensor_package;
