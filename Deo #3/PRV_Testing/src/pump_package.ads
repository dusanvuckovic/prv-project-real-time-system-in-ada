with actor_package; use actor_package;
with types; use types;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Numerics.Float_Random; use Ada.Numerics.Float_Random;


package pump_package is

   type Pump is new Actor with record      
      PumpErrorRate: Float;
      PumpWaitTime: Time_Span;     
      Gen: Ada.Numerics.Float_Random.Generator;    
   end record;
   
   overriding procedure Initialize(Self: in out Pump; ip: InitializationPackage);     
   function HasErrorHappened(Self: in Pump) return Boolean;


end pump_package;
