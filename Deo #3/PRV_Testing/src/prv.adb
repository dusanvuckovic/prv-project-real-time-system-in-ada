with Ada.Real_Time; use Ada.Real_Time;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Float_Random; use Ada.Numerics.Float_Random;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with guidata; use guidata;
with Gtk.Main;
with Gdk.Threads;

with parameters; use parameters;
with logger; use logger;
with types; use types;

with builder_cb; use builder_cb;

with processes; use processes;

procedure prv is

   gdata: aliased GUI_Data;
   pars: Project_Parameters;
   ip: InitializationPackage;
   tp: TaskPackage;


begin
   Gdk.Threads.G_Init;
   Gdk.Threads.Init;
   Gtk.Main.Init;

   pars := InitFromFile("config.txt");
   gdata := Link_GUI_Objects_With_XML("PRV.glade");
   guidata.Update_GUI_Objects(GUIdata => gdata,
                              pars    => pars);
   LogType.Init(gdata.Log);

   gdata.PumpSwitch.On_State_Set(Call  => builder_cb.Pump_Switched'Access,
                                 After => False);

   gdata.StartButton.On_Clicked(Call => builder_cb.Start_Clicked'Access);
   gdata.StopButton.On_Clicked(Call => builder_cb.Stop_Clicked'Access);
   gdata.LoadButton.On_Clicked(Call => builder_cb.Load_Clicked'Access);
   gdata.ResetButton.On_Clicked(Call => builder_cb.Reset_Clicked'Access);



   ip.CH4Scale := gdata.CH4_Sensor.Scale'Unchecked_Access;
   -- ip.COScale := guidata.GUI_Data.CO_Sensor.Scale;
   --   ip.AirScale := guidata.GUI_Data.Air_Sensor.Scale;

   tp.sensors(CH4_SENSOR) := new SensorProcess;
   tp.sensors(CO_SENSOR) := new SensorProcess;
   tp.sensors(AIR_SENSOR) := new SensorProcess;
   tp.sensors(WATER_FLOW_SENSOR) := new SensorProcess;
   tp.interrupts(WATER_LOW_SENSOR) := new InterruptProcess;
   tp.interrupts(WATER_HIGH_SENSOR) := new InterruptProcess;

   tp.Pump := new PumpProcess;
   tp.Controller := new ControllerProcess;
   ip.systemStartTime := Ada.Real_Time.Clock;

   ip.sensID := CH4_SENSOR;
   ip.actorName := To_Unbounded_String("CH4");
   ip.sensorPars := pars.CH4Parameters;
   tp.sensors(CH4_SENSOR).Initialize(ip, tp);

   ip.sensID := CO_SENSOR;
   ip.actorName := To_Unbounded_String("CO");
   ip.sensorPars := pars.COParameters;
   tp.sensors(CO_SENSOR).Initialize(ip, tp);

   ip.sensID := AIR_SENSOR;
   ip.actorName := To_Unbounded_String("AIR");
   ip.sensorPars := pars.AirParameters;
   tp.sensors(AIR_SENSOR).Initialize(ip, tp);

   ip.sensID := WATER_FLOW_SENSOR;
   ip.actorName := To_Unbounded_String("WaterFlow");
   ip.sensorPars.PError := pars.ErrorWaterFlow;
   ip.sensorPars.Low := 0.0;
   ip.sensorPars.High := 1.0;
   ip.sensorPars.Threshold := 2.0;
   ip.sensorPars.TimeToMeasure := 150;
   tp.sensors(WATER_FLOW_SENSOR).Initialize(ip, tp);

   ip.sensID := WATER_LOW_SENSOR;
   ip.actorName := To_Unbounded_String("WaterLow");
   ip.sensorPars.Threshold := pars.WaterLow;
   ip.sensorPars.TimeToMeasure := 150;
   tp.interrupts(WATER_LOW_SENSOR).Initialize(ip, tp);

   ip.sensID := WATER_HIGH_SENSOR;
   ip.actorName := To_Unbounded_String("WaterHigh");
   ip.sensorPars.Threshold := pars.WaterHigh;
   ip.sensorPars.TimeToMeasure := 150;
   tp.interrupts(WATER_HIGH_SENSOR).Initialize(ip, tp);

   ip.actorName := To_Unbounded_String("Pump");
   ip.PumpErrorRate := pars.PumpErrorRate;
   ip.PumpWaitTime := pars.PumpWaitTime;
   tp.Pump.Initialize(ip, tp);

   ip.CH4Threshold := pars.CH4Parameters.Threshold;
   ip.COThreshold := pars.COParameters.Threshold;
   ip.AirThreshold := pars.AirParameters.Threshold;
   tp.Controller.Initialize(ip, tp);

   Gdk.Threads.Enter;
   Gtk.Main.Main;
   Gdk.Threads.Leave;

end Prv;
