with Ada.Real_Time; use Ada.Real_Time;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;
with actor_package; use actor_package;
with types; use types;


package controller_package is
    
   type Controller_State is (Uninitialized, Initialized);
   
   subtype NumOfQueries is Natural range 0..4;
   type SensorArray is array (SensorID) of Boolean;
   
   TotalQueries: constant NumOfQueries := 4;
   
   type Controller is new Actor with
      record
         controllerT: Duration;
         RemainingQueries: Integer := TotalQueries;
         errors: SensorArray := (others => False);    
         
         TriedActivatingPump, TriedDeactivatingPump: Boolean := False;
         PumpStatusKnown: Boolean := True;
         ActivationsBeforePump: Integer := 7;
         CurrentActivationsBeforePump: Integer := 0;
         PumpRequestStartTime: Time;
         
      end record;
   
   overriding procedure Initialize(Self: in out Controller; ip: InitializationPackage);     
   
   procedure ResetError(Self: in out Controller; ID: in SensorID);
   procedure SetError(Self: in out Controller; ID: in SensorID);
   function IsError(Self: in Controller; ID: in SensorID) return Boolean;      
   
   function AreQueriesRemaining(Self: in Controller) return boolean;
   procedure ResetQueries(Self: in out Controller);
   function QueriesRemaining(Self: in Controller) return NumOfQueries;         
   procedure ProcessedQuery(Self: in out Controller);
   procedure NoQueriesRemaining(Self: in out Controller);     
   

   
end controller_package;
