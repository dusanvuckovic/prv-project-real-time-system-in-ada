package body polling_sensor_package is

   overriding procedure Initialize(Self: in out PollingSensor; ip: in InitializationPackage) is
   begin
      Initialize(Sensor(Self), ip);
      Self.HighValue := ip.sensorPars.High;
      Self.LowValue := ip.sensorPars.Low;
   end;
   
   function CalculateValue(Self: in PollingSensor) return Float is
   begin
      return Self.LowValue + Ada.Numerics.Float_Random.Random(Self.Gen) * (Self.HighValue - Self.LowValue);    
   end;
   
   overriding function CreateResult(Self: in out PollingSensor) return ResultPacket is
      res: ResultPacket;
   begin
      res.Result := Self.CalculateValue;
      res.ID := Self.ID;
      res.SensorName := Self.actorName;
      res.waterStatus := NO_WATER_FLOW;
      return res;
   end;

end polling_sensor_package;
