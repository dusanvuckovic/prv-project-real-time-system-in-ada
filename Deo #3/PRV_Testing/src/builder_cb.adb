package body builder_cb is

   procedure main_quit (Self : access Gtk_Widget_Record'Class) is
   begin
      Gtk.Main.Main_Quit;
   end main_quit;

   procedure button_clicked (Self : access Gtk_Button_Record'Class) is
   begin
      Put_Line ("Hello World!");
   end button_clicked;

   procedure button_quit (Self : access Gtk_Widget_Record'Class) is
   begin
      Put_Line ("button_quit is called");
      Destroy (Self);
   end button_quit;
   
   function Pump_Switched(Self: access Gtk_Switch_Record'Class; State : Boolean) return Boolean is
   begin
      if Self.Get_Active then
         Put_Line ("ACTIVE");
      else
         Put_Line("INACTIVE");
      end if;
      
      return True;
     
   end;
   
   procedure Start_Clicked(Self: access Gtk_Button_Record'Class) is
   begin
      Put_Line("START CLICKED!");
   end;
   
   procedure Stop_Clicked(Self: access Gtk_Button_Record'Class) is
   begin
      Put_Line("STOP CLICKED!");
   end;
   
   procedure Load_Clicked(Self: access Gtk_Button_Record'Class) is
   begin
      Put_Line("LOAD CLICKED!");
   end;
   
   procedure Reset_Clicked(Self: access Gtk_Button_Record'Class) is
   begin
      Put_Line("RESET CLICKED!");
   end;

end builder_cb; 
