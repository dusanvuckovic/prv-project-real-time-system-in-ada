with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Gtk.Text_View; use Gtk.Text_View;
with Gtk.Text_Buffer; use Gtk.Text_Buffer;
with Gtk.Text_Iter;   use Gtk.Text_Iter;
with Gdk.Threads;

package logger is
   
   subtype Priority is Positive range 1..3;
   
   GUILogPriority: Priority := 3;

   protected LogType is
      procedure Init(log: Gtk_Text_View);
      procedure Log(text: in Unbounded_String; pri: in Priority);
      procedure Finish;
      
   private
      procedure LogToFile(text: in Unbounded_String; file: File_Type);
      procedure LogToGUI(text: in Unbounded_String);
      
      severe_file, significant_file, log_file: File_Type;
      myLog: Gtk_Text_View;
      buffering: Boolean := False;
   end LogType;
      
end logger;
