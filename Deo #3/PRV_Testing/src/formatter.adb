package body formatter is
   
   function Format(flt: in Float) return String is
      i: Integer;      
      f: Float;      
   begin
      i := Integer(flt);
      f := Float(i);
      if f = flt then
         return Ada.Strings.Fixed.Trim(Source => i'Image,
                                       Side   => Ada.Strings.Both);
      else
         return Ada.Strings.Fixed.Trim(Source => Float_Two_Digits(flt),
                                       Side   => Both);      
      end if;
   end;
   
   function Format(i: in Integer) return String is
   begin
      return Trim(Source => i'Image, Side => Both);
   end;


   function Float_Two_Digits(flt: in Float) return String is
      str: String(1..100);
   begin
      Ada.Float_Text_IO.Put(To => str,
                            Item => flt,
                            Aft  => 2,
                            Exp  => 0);
      return Trim(Source => str, Side => Both);
   end;
   
   function Format(ts: in Time_Span) return String is
      ft: Float;
   begin
      ft := Float(To_Duration(ts*1000));
      return Format(ft);
   end;


end formatter;
