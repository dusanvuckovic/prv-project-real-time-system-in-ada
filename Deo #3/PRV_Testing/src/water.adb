package body water is
   
   protected body WaterLevel is

      function GetWaterLevel return Float is
      begin
         return CurrentWaterLevel;
      end;
   
      procedure IncreaseWater is
      begin
         CurrentWaterLevel := CurrentWaterLevel + WaterIncrease;
      end;
      procedure DecreaseWater is 
      begin 
         CurrentWaterLevel := CurrentWaterLevel - WaterDecrease;   
      end;
      
   end WaterLevel;

end water;
