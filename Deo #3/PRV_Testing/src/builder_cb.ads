with Gtk.Widget;  use Gtk.Widget;
with Gtk.Button; use Gtk.Button;
with Ada.Text_IO; use Ada.Text_IO;
with Gtk.Main;
with Gtk.Switch; use Gtk.Switch;

package builder_cb is
   procedure main_quit (Self : access Gtk_Widget_Record'Class);

   procedure button_clicked (Self : access Gtk_Button_Record'Class);
   procedure button_quit (Self : access Gtk_Widget_Record'Class);
   
   
   
   function Pump_Switched(Self: access Gtk_Switch_Record'Class; State : Boolean) return Boolean;
   
   procedure Start_Clicked(Self: access Gtk_Button_Record'Class);
   procedure Stop_Clicked(Self: access Gtk_Button_Record'Class);
   procedure Reset_Clicked(Self: access Gtk_Button_Record'Class);
   procedure Load_Clicked(Self: access Gtk_Button_Record'Class);
   
end builder_cb; 
