with Ada.Float_Text_IO;       use Ada.Float_Text_IO;
with Ada.Strings;             use Ada.Strings;
with Ada.Strings.Fixed;       use Ada.Strings.Fixed;
with Ada.Real_Time;           use Ada.Real_Time;


package formatter is   
   
   function Format(flt: in Float) return String;
   function Format(ts: in Time_Span) return String;   
   function Format(i: in Integer) return String;
   
private
  
   function Float_Two_Digits(flt: in Float) return String;
   
   


end formatter;
