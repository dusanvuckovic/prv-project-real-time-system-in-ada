with sensor_core_package; use sensor_core_package;
with types; use types;
with Ada.Numerics.Float_Random;
package polling_sensor_package is

   type PollingSensor is new Sensor with record
      HighValue, LowValue: Float;
   end record;
   
   overriding procedure Initialize(Self: in out PollingSensor; ip: in InitializationPackage);
   function CalculateValue(Self: in PollingSensor) return Float;
   overriding function CreateResult(Self: in out PollingSensor) return ResultPacket;
   

end polling_sensor_package;
