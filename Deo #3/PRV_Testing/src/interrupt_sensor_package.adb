package body interrupt_sensor_package is

   overriding procedure Initialize(Self: in out InterruptSensor; ip: in InitializationPackage) is
   begin
      Initialize(Sensor(Self), ip);
      Self.Threshold := ip.sensorPars.Threshold;
   end;
   
   overriding function CreateResult(Self: in out InterruptSensor) return ResultPacket is
      rp: ResultPacket;
   begin
      rp.Result := 1.0;
      raise BadOperation;
      return rp;
   end;
   
   overriding function IsPastThreshold(Self: WaterHighSensor; CurrentWaterLevel: Float) return Boolean is
   begin
      return CurrentWaterLevel >= Self.Threshold;
   end;   
   
   overriding function IsPastThreshold(Self: WaterLowSensor; CurrentWaterLevel: Float) return Boolean is
   begin
      return CurrentWaterLevel <= Self.Threshold;
   end; 

end interrupt_sensor_package;
