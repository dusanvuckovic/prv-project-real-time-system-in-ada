package water is
   
   protected WaterLevel is         
      
      function GetWaterLevel return Float;
      procedure IncreaseWater;
      procedure DecreaseWater;
      
   private
      CurrentWaterLevel: Float := 200.0;
   
      WaterIncrease: Float := 0.1;
      WaterDecrease: Float := 0.2;
      
   end WaterLevel;   

end water;
