with Ada.Real_Time; use Ada.Real_Time;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with types; use types;
with formatter; use formatter;
with sensor_core_package; use sensor_core_package;
with polling_sensor_package; use polling_sensor_package;
with controller_package; use controller_package;
with water_flow_sensor_package; use water_flow_sensor_package;
with interrupt_sensor_package; use interrupt_sensor_package;
with pump_package; use pump_package;
with water; use water;
with Gtk.Scale; use Gtk.Scale;


package processes is  
   
   type SensorProcess;
   type ControllerProcess;
   type InterruptProcess;
   type PumpProcess;
   
   type SensorArray is array (SensorID) of access SensorProcess;
   type InterruptArray is array (SensorID) of access InterruptProcess;
   
   type TaskPackage is record   
      --CH4, CO, AIR: access SensorProcess;
      sensors: SensorArray;
      interrupts: InterruptArray;
      Controller: access ControllerProcess;
      Pump: access PumpProcess;
   end record;
   
   task type SensorProcess is

      entry Initialize(ip: InitializationPackage; tp: TaskPackage);
      entry StartMeasurement;
      entry RequestStatus;
      entry RequestResult;
      --procedure run(Self: in out Controller);
     
   end SensorProcess;
   
   task type ControllerProcess is

      entry Initialize(ip: InitializationPackage; tp: TaskPackage);
      entry SensorInterrupt(id: SensorID);
      entry ReturnStatus(statp : in StatusPacket);
      entry ReturnResult(resp: ResultPacket);
      --procedure run(Self: in out Controller);
     
   end ControllerProcess;
   
   task type InterruptProcess is
      entry Initialize(ip: InitializationPackage; tp: TaskPackage);
   end InterruptProcess;
   
   task type PumpProcess is
      entry Initialize(ip: InitializationPackage; tp: TaskPackage);
      entry PollPump;
      entry ActivatePump;
      entry DeactivatePump;
   end PumpProcess;
    

end processes;
