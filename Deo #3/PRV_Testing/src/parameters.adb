package body parameters is
   
   package Real_IO is new Float_IO(Float); use Real_IO;
   
   function GetNumberFloat(number: in String; last: in Integer) return Float is
      startCharacter: Integer := 1;
      
   begin
      while number(startCharacter) /= '=' loop 
         startCharacter := startCharacter + 1;
      end loop;      
      startCharacter := startCharacter + 2;
      return Float'Value(number(startCharacter..last));
                    
   end;

   function InitFromFile(filename: in String) return Project_Parameters is
      file: File_Type;
      chars: String (1..1000);
      last: Natural;
      pars : Project_Parameters;
      cnt: Integer := 0;
      val: Float;
   begin
      Ada.Text_IO.Open(Name => filename,
                       Mode => Ada.Text_IO.In_File,
                       File => file);
      
      while not End_Of_File (file) loop
         Ada.Text_IO.Get_Line(File => file,
                              Item => chars,
                              Last => last);
         if last /= 0 then 
            val := GetNumberFloat(chars(1..last), last);
            if cnt = 0 then
               pars.CH4Parameters.Threshold := val;
            elsif cnt = 1 then
               pars.COParameters.Threshold := val;
            elsif cnt = 2 then
               pars.AirParameters.Threshold := val;
            elsif cnt = 3 then
               pars.CH4Parameters.High := val;
            elsif cnt = 4 then
               pars.CH4Parameters.Low := val;
            elsif cnt = 5 then
               pars.COParameters.High := val;
            elsif cnt = 6 then
               pars.COParameters.Low := val;
            elsif cnt = 7 then
               pars.AirParameters.High := val;
            elsif cnt = 8 then
               pars.AirParameters.Low := val;   
            elsif cnt = 9 then
               pars.WaterHigh := val;
            elsif cnt = 10 then
               pars.WaterLow := val; 
            elsif cnt = 11 then
               pars.WaterIncrease := val;
            elsif cnt = 12 then
               pars.WaterDecrease := val;
            elsif cnt = 13 then
               pars.WaterLevel := val;                
            elsif cnt = 14 then
               pars.CH4Parameters.PError := val; 
            elsif cnt = 15 then
               pars.COParameters.PError := val; 
            elsif cnt = 16 then
               pars.AirParameters.PError := val; 
            elsif cnt = 17 then
               pars.ErrorWaterFlow := val; 
            elsif cnt = 18 then
               pars.ErrorWaterHigh := val; 
            elsif cnt = 19 then      
               pars.ErrorWaterLow := val; 
            elsif cnt = 20 then
               pars.PumpErrorRate := val;
            elsif cnt = 21 then
               pars.PumpWaitTime := Integer(val);
            end if;
            
            cnt := cnt + 1;

         end if;
      end loop;
      
      Ada.Text_IO.Close(File => file);
      return pars;
   end InitFromFile;

end parameters;
