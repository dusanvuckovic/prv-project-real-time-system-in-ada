pragma Warnings (Off);
pragma Ada_95;
pragma Source_File_Name (ada_main, Spec_File_Name => "b__prv.ads");
pragma Source_File_Name (ada_main, Body_File_Name => "b__prv.adb");
pragma Suppress (Overflow_Check);

with System.Restrictions;
with Ada.Exceptions;

package body ada_main is

   E070 : Short_Integer; pragma Import (Ada, E070, "system__os_lib_E");
   E011 : Short_Integer; pragma Import (Ada, E011, "system__soft_links_E");
   E023 : Short_Integer; pragma Import (Ada, E023, "system__exception_table_E");
   E066 : Short_Integer; pragma Import (Ada, E066, "ada__io_exceptions_E");
   E050 : Short_Integer; pragma Import (Ada, E050, "ada__strings_E");
   E038 : Short_Integer; pragma Import (Ada, E038, "ada__containers_E");
   E025 : Short_Integer; pragma Import (Ada, E025, "system__exceptions_E");
   E076 : Short_Integer; pragma Import (Ada, E076, "interfaces__c_E");
   E052 : Short_Integer; pragma Import (Ada, E052, "ada__strings__maps_E");
   E056 : Short_Integer; pragma Import (Ada, E056, "ada__strings__maps__constants_E");
   E019 : Short_Integer; pragma Import (Ada, E019, "system__soft_links__initialize_E");
   E078 : Short_Integer; pragma Import (Ada, E078, "system__object_reader_E");
   E045 : Short_Integer; pragma Import (Ada, E045, "system__dwarf_lines_E");
   E037 : Short_Integer; pragma Import (Ada, E037, "system__traceback__symbolic_E");
   E096 : Short_Integer; pragma Import (Ada, E096, "ada__numerics_E");
   E145 : Short_Integer; pragma Import (Ada, E145, "ada__tags_E");
   E162 : Short_Integer; pragma Import (Ada, E162, "ada__streams_E");
   E119 : Short_Integer; pragma Import (Ada, E119, "interfaces__c__strings_E");
   E179 : Short_Integer; pragma Import (Ada, E179, "system__file_control_block_E");
   E164 : Short_Integer; pragma Import (Ada, E164, "system__finalization_root_E");
   E160 : Short_Integer; pragma Import (Ada, E160, "ada__finalization_E");
   E178 : Short_Integer; pragma Import (Ada, E178, "system__file_io_E");
   E474 : Short_Integer; pragma Import (Ada, E474, "ada__streams__stream_io_E");
   E166 : Short_Integer; pragma Import (Ada, E166, "system__storage_pools_E");
   E157 : Short_Integer; pragma Import (Ada, E157, "system__finalization_masters_E");
   E155 : Short_Integer; pragma Import (Ada, E155, "system__storage_pools__subpools_E");
   E141 : Short_Integer; pragma Import (Ada, E141, "ada__strings__unbounded_E");
   E129 : Short_Integer; pragma Import (Ada, E129, "system__task_info_E");
   E104 : Short_Integer; pragma Import (Ada, E104, "ada__calendar_E");
   E113 : Short_Integer; pragma Import (Ada, E113, "ada__real_time_E");
   E174 : Short_Integer; pragma Import (Ada, E174, "ada__text_io_E");
   E458 : Short_Integer; pragma Import (Ada, E458, "system__assertions_E");
   E189 : Short_Integer; pragma Import (Ada, E189, "system__pool_global_E");
   E102 : Short_Integer; pragma Import (Ada, E102, "system__random_seed_E");
   E499 : Short_Integer; pragma Import (Ada, E499, "system__tasking__initialization_E");
   E483 : Short_Integer; pragma Import (Ada, E483, "system__tasking__protected_objects_E");
   E503 : Short_Integer; pragma Import (Ada, E503, "system__tasking__protected_objects__entries_E");
   E507 : Short_Integer; pragma Import (Ada, E507, "system__tasking__queuing_E");
   E511 : Short_Integer; pragma Import (Ada, E511, "system__tasking__stages_E");
   E184 : Short_Integer; pragma Import (Ada, E184, "glib_E");
   E187 : Short_Integer; pragma Import (Ada, E187, "gtkada__types_E");
   E405 : Short_Integer; pragma Import (Ada, E405, "formatter_E");
   E257 : Short_Integer; pragma Import (Ada, E257, "gdk__frame_timings_E");
   E209 : Short_Integer; pragma Import (Ada, E209, "glib__glist_E");
   E239 : Short_Integer; pragma Import (Ada, E239, "gdk__visual_E");
   E211 : Short_Integer; pragma Import (Ada, E211, "glib__gslist_E");
   E203 : Short_Integer; pragma Import (Ada, E203, "gtkada__c_E");
   E193 : Short_Integer; pragma Import (Ada, E193, "glib__object_E");
   E207 : Short_Integer; pragma Import (Ada, E207, "glib__values_E");
   E205 : Short_Integer; pragma Import (Ada, E205, "glib__types_E");
   E195 : Short_Integer; pragma Import (Ada, E195, "glib__type_conversion_hooks_E");
   E197 : Short_Integer; pragma Import (Ada, E197, "gtkada__bindings_E");
   E217 : Short_Integer; pragma Import (Ada, E217, "cairo_E");
   E219 : Short_Integer; pragma Import (Ada, E219, "cairo__region_E");
   E226 : Short_Integer; pragma Import (Ada, E226, "gdk__rectangle_E");
   E224 : Short_Integer; pragma Import (Ada, E224, "glib__generic_properties_E");
   E249 : Short_Integer; pragma Import (Ada, E249, "gdk__color_E");
   E229 : Short_Integer; pragma Import (Ada, E229, "gdk__rgba_E");
   E222 : Short_Integer; pragma Import (Ada, E222, "gdk__event_E");
   E360 : Short_Integer; pragma Import (Ada, E360, "glib__key_file_E");
   E241 : Short_Integer; pragma Import (Ada, E241, "glib__properties_E");
   E322 : Short_Integer; pragma Import (Ada, E322, "glib__string_E");
   E320 : Short_Integer; pragma Import (Ada, E320, "glib__variant_E");
   E318 : Short_Integer; pragma Import (Ada, E318, "glib__g_icon_E");
   E382 : Short_Integer; pragma Import (Ada, E382, "gtk__actionable_E");
   E265 : Short_Integer; pragma Import (Ada, E265, "gtk__builder_E");
   E302 : Short_Integer; pragma Import (Ada, E302, "gtk__buildable_E");
   E334 : Short_Integer; pragma Import (Ada, E334, "gtk__cell_area_context_E");
   E350 : Short_Integer; pragma Import (Ada, E350, "gtk__css_section_E");
   E243 : Short_Integer; pragma Import (Ada, E243, "gtk__enums_E");
   E308 : Short_Integer; pragma Import (Ada, E308, "gtk__orientable_E");
   E362 : Short_Integer; pragma Import (Ada, E362, "gtk__paper_size_E");
   E358 : Short_Integer; pragma Import (Ada, E358, "gtk__page_setup_E");
   E370 : Short_Integer; pragma Import (Ada, E370, "gtk__print_settings_E");
   E273 : Short_Integer; pragma Import (Ada, E273, "gtk__target_entry_E");
   E271 : Short_Integer; pragma Import (Ada, E271, "gtk__target_list_E");
   E450 : Short_Integer; pragma Import (Ada, E450, "gtk__text_mark_E");
   E278 : Short_Integer; pragma Import (Ada, E278, "pango__enums_E");
   E296 : Short_Integer; pragma Import (Ada, E296, "pango__attributes_E");
   E282 : Short_Integer; pragma Import (Ada, E282, "pango__font_metrics_E");
   E284 : Short_Integer; pragma Import (Ada, E284, "pango__language_E");
   E280 : Short_Integer; pragma Import (Ada, E280, "pango__font_E");
   E376 : Short_Integer; pragma Import (Ada, E376, "gtk__text_attributes_E");
   E378 : Short_Integer; pragma Import (Ada, E378, "gtk__text_tag_E");
   E288 : Short_Integer; pragma Import (Ada, E288, "pango__font_face_E");
   E286 : Short_Integer; pragma Import (Ada, E286, "pango__font_family_E");
   E290 : Short_Integer; pragma Import (Ada, E290, "pango__fontset_E");
   E292 : Short_Integer; pragma Import (Ada, E292, "pango__matrix_E");
   E276 : Short_Integer; pragma Import (Ada, E276, "pango__context_E");
   E366 : Short_Integer; pragma Import (Ada, E366, "pango__font_map_E");
   E298 : Short_Integer; pragma Import (Ada, E298, "pango__tabs_E");
   E294 : Short_Integer; pragma Import (Ada, E294, "pango__layout_E");
   E364 : Short_Integer; pragma Import (Ada, E364, "gtk__print_context_E");
   E237 : Short_Integer; pragma Import (Ada, E237, "gdk__display_E");
   E368 : Short_Integer; pragma Import (Ada, E368, "gtk__print_operation_preview_E");
   E340 : Short_Integer; pragma Import (Ada, E340, "gtk__tree_model_E");
   E328 : Short_Integer; pragma Import (Ada, E328, "gtk__entry_buffer_E");
   E326 : Short_Integer; pragma Import (Ada, E326, "gtk__editable_E");
   E324 : Short_Integer; pragma Import (Ada, E324, "gtk__cell_editable_E");
   E306 : Short_Integer; pragma Import (Ada, E306, "gtk__adjustment_E");
   E269 : Short_Integer; pragma Import (Ada, E269, "gtk__style_E");
   E263 : Short_Integer; pragma Import (Ada, E263, "gtk__accel_group_E");
   E255 : Short_Integer; pragma Import (Ada, E255, "gdk__frame_clock_E");
   E259 : Short_Integer; pragma Import (Ada, E259, "gdk__pixbuf_E");
   E346 : Short_Integer; pragma Import (Ada, E346, "gtk__icon_source_E");
   E235 : Short_Integer; pragma Import (Ada, E235, "gdk__screen_E");
   E374 : Short_Integer; pragma Import (Ada, E374, "gtk__text_iter_E");
   E267 : Short_Integer; pragma Import (Ada, E267, "gtk__selection_data_E");
   E251 : Short_Integer; pragma Import (Ada, E251, "gdk__device_E");
   E253 : Short_Integer; pragma Import (Ada, E253, "gdk__drag_contexts_E");
   E247 : Short_Integer; pragma Import (Ada, E247, "gtk__widget_E");
   E352 : Short_Integer; pragma Import (Ada, E352, "gtk__misc_E");
   E245 : Short_Integer; pragma Import (Ada, E245, "gtk__style_provider_E");
   E233 : Short_Integer; pragma Import (Ada, E233, "gtk__settings_E");
   E312 : Short_Integer; pragma Import (Ada, E312, "gdk__window_E");
   E348 : Short_Integer; pragma Import (Ada, E348, "gtk__style_context_E");
   E344 : Short_Integer; pragma Import (Ada, E344, "gtk__icon_set_E");
   E342 : Short_Integer; pragma Import (Ada, E342, "gtk__image_E");
   E338 : Short_Integer; pragma Import (Ada, E338, "gtk__cell_renderer_E");
   E304 : Short_Integer; pragma Import (Ada, E304, "gtk__container_E");
   E314 : Short_Integer; pragma Import (Ada, E314, "gtk__bin_E");
   E300 : Short_Integer; pragma Import (Ada, E300, "gtk__box_E");
   E372 : Short_Integer; pragma Import (Ada, E372, "gtk__status_bar_E");
   E354 : Short_Integer; pragma Import (Ada, E354, "gtk__notebook_E");
   E336 : Short_Integer; pragma Import (Ada, E336, "gtk__cell_layout_E");
   E332 : Short_Integer; pragma Import (Ada, E332, "gtk__cell_area_E");
   E330 : Short_Integer; pragma Import (Ada, E330, "gtk__entry_completion_E");
   E310 : Short_Integer; pragma Import (Ada, E310, "gtk__window_E");
   E231 : Short_Integer; pragma Import (Ada, E231, "gtk__dialog_E");
   E356 : Short_Integer; pragma Import (Ada, E356, "gtk__print_operation_E");
   E316 : Short_Integer; pragma Import (Ada, E316, "gtk__gentry_E");
   E215 : Short_Integer; pragma Import (Ada, E215, "gtk__arguments_E");
   E430 : Short_Integer; pragma Import (Ada, E430, "glib__menu_model_E");
   E380 : Short_Integer; pragma Import (Ada, E380, "gtk__action_E");
   E384 : Short_Integer; pragma Import (Ada, E384, "gtk__activatable_E");
   E213 : Short_Integer; pragma Import (Ada, E213, "gtk__button_E");
   E446 : Short_Integer; pragma Import (Ada, E446, "gtk__clipboard_E");
   E438 : Short_Integer; pragma Import (Ada, E438, "gtk__grange_E");
   E386 : Short_Integer; pragma Import (Ada, E386, "gtk__main_E");
   E460 : Short_Integer; pragma Import (Ada, E460, "gtk__marshallers_E");
   E432 : Short_Integer; pragma Import (Ada, E432, "gtk__menu_item_E");
   E434 : Short_Integer; pragma Import (Ada, E434, "gtk__menu_shell_E");
   E428 : Short_Integer; pragma Import (Ada, E428, "gtk__menu_E");
   E426 : Short_Integer; pragma Import (Ada, E426, "gtk__label_E");
   E436 : Short_Integer; pragma Import (Ada, E436, "gtk__scale_E");
   E442 : Short_Integer; pragma Import (Ada, E442, "gtk__scrollable_E");
   E388 : Short_Integer; pragma Import (Ada, E388, "gtk__switch_E");
   E181 : Short_Integer; pragma Import (Ada, E181, "builder_cb_E");
   E448 : Short_Integer; pragma Import (Ada, E448, "gtk__text_child_anchor_E");
   E452 : Short_Integer; pragma Import (Ada, E452, "gtk__text_tag_table_E");
   E444 : Short_Integer; pragma Import (Ada, E444, "gtk__text_buffer_E");
   E440 : Short_Integer; pragma Import (Ada, E440, "gtk__text_view_E");
   E462 : Short_Integer; pragma Import (Ada, E462, "gtk__tree_view_column_E");
   E463 : Short_Integer; pragma Import (Ada, E463, "gtkada__handlers_E");
   E454 : Short_Integer; pragma Import (Ada, E454, "gtkada__builder_E");
   E481 : Short_Integer; pragma Import (Ada, E481, "logger_E");
   E479 : Short_Integer; pragma Import (Ada, E479, "parameters_E");
   E391 : Short_Integer; pragma Import (Ada, E391, "guidata_E");
   E515 : Short_Integer; pragma Import (Ada, E515, "actor_package_E");
   E513 : Short_Integer; pragma Import (Ada, E513, "controller_package_E");
   E526 : Short_Integer; pragma Import (Ada, E526, "pump_package_E");
   E522 : Short_Integer; pragma Import (Ada, E522, "sensor_core_package_E");
   E520 : Short_Integer; pragma Import (Ada, E520, "interrupt_sensor_package_E");
   E524 : Short_Integer; pragma Import (Ada, E524, "polling_sensor_package_E");
   E528 : Short_Integer; pragma Import (Ada, E528, "water_E");
   E530 : Short_Integer; pragma Import (Ada, E530, "water_flow_sensor_package_E");
   E489 : Short_Integer; pragma Import (Ada, E489, "processes_E");

   Sec_Default_Sized_Stacks : array (1 .. 1) of aliased System.Secondary_Stack.SS_Stack (System.Parameters.Runtime_Default_Sec_Stack_Size);

   Local_Priority_Specific_Dispatching : constant String := "";
   Local_Interrupt_States : constant String := "";

   Is_Elaborated : Boolean := False;

   procedure finalize_library is
   begin
      declare
         procedure F1;
         pragma Import (Ada, F1, "processes__finalize_body");
      begin
         E489 := E489 - 1;
         F1;
      end;
      E530 := E530 - 1;
      declare
         procedure F2;
         pragma Import (Ada, F2, "water_flow_sensor_package__finalize_spec");
      begin
         F2;
      end;
      E528 := E528 - 1;
      declare
         procedure F3;
         pragma Import (Ada, F3, "water__finalize_spec");
      begin
         F3;
      end;
      E524 := E524 - 1;
      declare
         procedure F4;
         pragma Import (Ada, F4, "polling_sensor_package__finalize_spec");
      begin
         F4;
      end;
      E520 := E520 - 1;
      declare
         procedure F5;
         pragma Import (Ada, F5, "interrupt_sensor_package__finalize_spec");
      begin
         F5;
      end;
      E526 := E526 - 1;
      declare
         procedure F6;
         pragma Import (Ada, F6, "pump_package__finalize_spec");
      begin
         F6;
      end;
      E513 := E513 - 1;
      declare
         procedure F7;
         pragma Import (Ada, F7, "controller_package__finalize_spec");
      begin
         F7;
      end;
      E481 := E481 - 1;
      declare
         procedure F8;
         pragma Import (Ada, F8, "logger__finalize_spec");
      begin
         F8;
      end;
      declare
         procedure F9;
         pragma Import (Ada, F9, "gtkada__builder__finalize_body");
      begin
         E454 := E454 - 1;
         F9;
      end;
      declare
         procedure F10;
         pragma Import (Ada, F10, "gtkada__builder__finalize_spec");
      begin
         F10;
      end;
      declare
         procedure F11;
         pragma Import (Ada, F11, "gtkada__handlers__finalize_spec");
      begin
         E463 := E463 - 1;
         F11;
      end;
      E462 := E462 - 1;
      declare
         procedure F12;
         pragma Import (Ada, F12, "gtk__tree_view_column__finalize_spec");
      begin
         F12;
      end;
      E440 := E440 - 1;
      declare
         procedure F13;
         pragma Import (Ada, F13, "gtk__text_view__finalize_spec");
      begin
         F13;
      end;
      E444 := E444 - 1;
      declare
         procedure F14;
         pragma Import (Ada, F14, "gtk__text_buffer__finalize_spec");
      begin
         F14;
      end;
      E452 := E452 - 1;
      declare
         procedure F15;
         pragma Import (Ada, F15, "gtk__text_tag_table__finalize_spec");
      begin
         F15;
      end;
      E448 := E448 - 1;
      declare
         procedure F16;
         pragma Import (Ada, F16, "gtk__text_child_anchor__finalize_spec");
      begin
         F16;
      end;
      E388 := E388 - 1;
      declare
         procedure F17;
         pragma Import (Ada, F17, "gtk__switch__finalize_spec");
      begin
         F17;
      end;
      E436 := E436 - 1;
      declare
         procedure F18;
         pragma Import (Ada, F18, "gtk__scale__finalize_spec");
      begin
         F18;
      end;
      E426 := E426 - 1;
      declare
         procedure F19;
         pragma Import (Ada, F19, "gtk__label__finalize_spec");
      begin
         F19;
      end;
      E428 := E428 - 1;
      declare
         procedure F20;
         pragma Import (Ada, F20, "gtk__menu__finalize_spec");
      begin
         F20;
      end;
      E434 := E434 - 1;
      declare
         procedure F21;
         pragma Import (Ada, F21, "gtk__menu_shell__finalize_spec");
      begin
         F21;
      end;
      E432 := E432 - 1;
      declare
         procedure F22;
         pragma Import (Ada, F22, "gtk__menu_item__finalize_spec");
      begin
         F22;
      end;
      E438 := E438 - 1;
      declare
         procedure F23;
         pragma Import (Ada, F23, "gtk__grange__finalize_spec");
      begin
         F23;
      end;
      E446 := E446 - 1;
      declare
         procedure F24;
         pragma Import (Ada, F24, "gtk__clipboard__finalize_spec");
      begin
         F24;
      end;
      E213 := E213 - 1;
      declare
         procedure F25;
         pragma Import (Ada, F25, "gtk__button__finalize_spec");
      begin
         F25;
      end;
      E380 := E380 - 1;
      declare
         procedure F26;
         pragma Import (Ada, F26, "gtk__action__finalize_spec");
      begin
         F26;
      end;
      E430 := E430 - 1;
      declare
         procedure F27;
         pragma Import (Ada, F27, "glib__menu_model__finalize_spec");
      begin
         F27;
      end;
      E237 := E237 - 1;
      E255 := E255 - 1;
      E263 := E263 - 1;
      E247 := E247 - 1;
      E269 := E269 - 1;
      E304 := E304 - 1;
      E306 := E306 - 1;
      E231 := E231 - 1;
      E310 := E310 - 1;
      E328 := E328 - 1;
      E338 := E338 - 1;
      E330 := E330 - 1;
      E332 := E332 - 1;
      E340 := E340 - 1;
      E316 := E316 - 1;
      E348 := E348 - 1;
      E354 := E354 - 1;
      E356 := E356 - 1;
      E372 := E372 - 1;
      declare
         procedure F28;
         pragma Import (Ada, F28, "gtk__gentry__finalize_spec");
      begin
         F28;
      end;
      declare
         procedure F29;
         pragma Import (Ada, F29, "gtk__print_operation__finalize_spec");
      begin
         F29;
      end;
      declare
         procedure F30;
         pragma Import (Ada, F30, "gtk__dialog__finalize_spec");
      begin
         F30;
      end;
      declare
         procedure F31;
         pragma Import (Ada, F31, "gtk__window__finalize_spec");
      begin
         F31;
      end;
      declare
         procedure F32;
         pragma Import (Ada, F32, "gtk__entry_completion__finalize_spec");
      begin
         F32;
      end;
      declare
         procedure F33;
         pragma Import (Ada, F33, "gtk__cell_area__finalize_spec");
      begin
         F33;
      end;
      declare
         procedure F34;
         pragma Import (Ada, F34, "gtk__notebook__finalize_spec");
      begin
         F34;
      end;
      declare
         procedure F35;
         pragma Import (Ada, F35, "gtk__status_bar__finalize_spec");
      begin
         F35;
      end;
      E300 := E300 - 1;
      declare
         procedure F36;
         pragma Import (Ada, F36, "gtk__box__finalize_spec");
      begin
         F36;
      end;
      E314 := E314 - 1;
      declare
         procedure F37;
         pragma Import (Ada, F37, "gtk__bin__finalize_spec");
      begin
         F37;
      end;
      declare
         procedure F38;
         pragma Import (Ada, F38, "gtk__container__finalize_spec");
      begin
         F38;
      end;
      declare
         procedure F39;
         pragma Import (Ada, F39, "gtk__cell_renderer__finalize_spec");
      begin
         F39;
      end;
      E342 := E342 - 1;
      declare
         procedure F40;
         pragma Import (Ada, F40, "gtk__image__finalize_spec");
      begin
         F40;
      end;
      E344 := E344 - 1;
      declare
         procedure F41;
         pragma Import (Ada, F41, "gtk__icon_set__finalize_spec");
      begin
         F41;
      end;
      declare
         procedure F42;
         pragma Import (Ada, F42, "gtk__style_context__finalize_spec");
      begin
         F42;
      end;
      E233 := E233 - 1;
      declare
         procedure F43;
         pragma Import (Ada, F43, "gtk__settings__finalize_spec");
      begin
         F43;
      end;
      E352 := E352 - 1;
      declare
         procedure F44;
         pragma Import (Ada, F44, "gtk__misc__finalize_spec");
      begin
         F44;
      end;
      declare
         procedure F45;
         pragma Import (Ada, F45, "gtk__widget__finalize_spec");
      begin
         F45;
      end;
      E251 := E251 - 1;
      E253 := E253 - 1;
      declare
         procedure F46;
         pragma Import (Ada, F46, "gdk__drag_contexts__finalize_spec");
      begin
         F46;
      end;
      declare
         procedure F47;
         pragma Import (Ada, F47, "gdk__device__finalize_spec");
      begin
         F47;
      end;
      E267 := E267 - 1;
      declare
         procedure F48;
         pragma Import (Ada, F48, "gtk__selection_data__finalize_spec");
      begin
         F48;
      end;
      E235 := E235 - 1;
      declare
         procedure F49;
         pragma Import (Ada, F49, "gdk__screen__finalize_spec");
      begin
         F49;
      end;
      E259 := E259 - 1;
      E346 := E346 - 1;
      declare
         procedure F50;
         pragma Import (Ada, F50, "gtk__icon_source__finalize_spec");
      begin
         F50;
      end;
      declare
         procedure F51;
         pragma Import (Ada, F51, "gdk__pixbuf__finalize_spec");
      begin
         F51;
      end;
      declare
         procedure F52;
         pragma Import (Ada, F52, "gdk__frame_clock__finalize_spec");
      begin
         F52;
      end;
      declare
         procedure F53;
         pragma Import (Ada, F53, "gtk__accel_group__finalize_spec");
      begin
         F53;
      end;
      declare
         procedure F54;
         pragma Import (Ada, F54, "gtk__style__finalize_spec");
      begin
         F54;
      end;
      declare
         procedure F55;
         pragma Import (Ada, F55, "gtk__adjustment__finalize_spec");
      begin
         F55;
      end;
      declare
         procedure F56;
         pragma Import (Ada, F56, "gtk__entry_buffer__finalize_spec");
      begin
         F56;
      end;
      declare
         procedure F57;
         pragma Import (Ada, F57, "gtk__tree_model__finalize_spec");
      begin
         F57;
      end;
      declare
         procedure F58;
         pragma Import (Ada, F58, "gdk__display__finalize_spec");
      begin
         F58;
      end;
      E364 := E364 - 1;
      declare
         procedure F59;
         pragma Import (Ada, F59, "gtk__print_context__finalize_spec");
      begin
         F59;
      end;
      E294 := E294 - 1;
      declare
         procedure F60;
         pragma Import (Ada, F60, "pango__layout__finalize_spec");
      begin
         F60;
      end;
      E298 := E298 - 1;
      declare
         procedure F61;
         pragma Import (Ada, F61, "pango__tabs__finalize_spec");
      begin
         F61;
      end;
      E366 := E366 - 1;
      declare
         procedure F62;
         pragma Import (Ada, F62, "pango__font_map__finalize_spec");
      begin
         F62;
      end;
      E276 := E276 - 1;
      declare
         procedure F63;
         pragma Import (Ada, F63, "pango__context__finalize_spec");
      begin
         F63;
      end;
      E290 := E290 - 1;
      declare
         procedure F64;
         pragma Import (Ada, F64, "pango__fontset__finalize_spec");
      begin
         F64;
      end;
      E286 := E286 - 1;
      declare
         procedure F65;
         pragma Import (Ada, F65, "pango__font_family__finalize_spec");
      begin
         F65;
      end;
      E288 := E288 - 1;
      declare
         procedure F66;
         pragma Import (Ada, F66, "pango__font_face__finalize_spec");
      begin
         F66;
      end;
      E378 := E378 - 1;
      declare
         procedure F67;
         pragma Import (Ada, F67, "gtk__text_tag__finalize_spec");
      begin
         F67;
      end;
      E280 := E280 - 1;
      declare
         procedure F68;
         pragma Import (Ada, F68, "pango__font__finalize_spec");
      begin
         F68;
      end;
      E284 := E284 - 1;
      declare
         procedure F69;
         pragma Import (Ada, F69, "pango__language__finalize_spec");
      begin
         F69;
      end;
      E282 := E282 - 1;
      declare
         procedure F70;
         pragma Import (Ada, F70, "pango__font_metrics__finalize_spec");
      begin
         F70;
      end;
      E296 := E296 - 1;
      declare
         procedure F71;
         pragma Import (Ada, F71, "pango__attributes__finalize_spec");
      begin
         F71;
      end;
      E450 := E450 - 1;
      declare
         procedure F72;
         pragma Import (Ada, F72, "gtk__text_mark__finalize_spec");
      begin
         F72;
      end;
      E271 := E271 - 1;
      declare
         procedure F73;
         pragma Import (Ada, F73, "gtk__target_list__finalize_spec");
      begin
         F73;
      end;
      E370 := E370 - 1;
      declare
         procedure F74;
         pragma Import (Ada, F74, "gtk__print_settings__finalize_spec");
      begin
         F74;
      end;
      E358 := E358 - 1;
      declare
         procedure F75;
         pragma Import (Ada, F75, "gtk__page_setup__finalize_spec");
      begin
         F75;
      end;
      E362 := E362 - 1;
      declare
         procedure F76;
         pragma Import (Ada, F76, "gtk__paper_size__finalize_spec");
      begin
         F76;
      end;
      E350 := E350 - 1;
      declare
         procedure F77;
         pragma Import (Ada, F77, "gtk__css_section__finalize_spec");
      begin
         F77;
      end;
      E334 := E334 - 1;
      declare
         procedure F78;
         pragma Import (Ada, F78, "gtk__cell_area_context__finalize_spec");
      begin
         F78;
      end;
      E265 := E265 - 1;
      declare
         procedure F79;
         pragma Import (Ada, F79, "gtk__builder__finalize_spec");
      begin
         F79;
      end;
      E320 := E320 - 1;
      declare
         procedure F80;
         pragma Import (Ada, F80, "glib__variant__finalize_spec");
      begin
         F80;
      end;
      E193 := E193 - 1;
      declare
         procedure F81;
         pragma Import (Ada, F81, "glib__object__finalize_spec");
      begin
         F81;
      end;
      E257 := E257 - 1;
      declare
         procedure F82;
         pragma Import (Ada, F82, "gdk__frame_timings__finalize_spec");
      begin
         F82;
      end;
      E184 := E184 - 1;
      declare
         procedure F83;
         pragma Import (Ada, F83, "glib__finalize_spec");
      begin
         F83;
      end;
      E503 := E503 - 1;
      declare
         procedure F84;
         pragma Import (Ada, F84, "system__tasking__protected_objects__entries__finalize_spec");
      begin
         F84;
      end;
      E189 := E189 - 1;
      declare
         procedure F85;
         pragma Import (Ada, F85, "system__pool_global__finalize_spec");
      begin
         F85;
      end;
      E174 := E174 - 1;
      declare
         procedure F86;
         pragma Import (Ada, F86, "ada__text_io__finalize_spec");
      begin
         F86;
      end;
      E141 := E141 - 1;
      declare
         procedure F87;
         pragma Import (Ada, F87, "ada__strings__unbounded__finalize_spec");
      begin
         F87;
      end;
      E155 := E155 - 1;
      declare
         procedure F88;
         pragma Import (Ada, F88, "system__storage_pools__subpools__finalize_spec");
      begin
         F88;
      end;
      E157 := E157 - 1;
      declare
         procedure F89;
         pragma Import (Ada, F89, "system__finalization_masters__finalize_spec");
      begin
         F89;
      end;
      E474 := E474 - 1;
      declare
         procedure F90;
         pragma Import (Ada, F90, "ada__streams__stream_io__finalize_spec");
      begin
         F90;
      end;
      declare
         procedure F91;
         pragma Import (Ada, F91, "system__file_io__finalize_body");
      begin
         E178 := E178 - 1;
         F91;
      end;
      declare
         procedure Reraise_Library_Exception_If_Any;
            pragma Import (Ada, Reraise_Library_Exception_If_Any, "__gnat_reraise_library_exception_if_any");
      begin
         Reraise_Library_Exception_If_Any;
      end;
   end finalize_library;

   procedure adafinal is
      procedure s_stalib_adafinal;
      pragma Import (C, s_stalib_adafinal, "system__standard_library__adafinal");

      procedure Runtime_Finalize;
      pragma Import (C, Runtime_Finalize, "__gnat_runtime_finalize");

   begin
      if not Is_Elaborated then
         return;
      end if;
      Is_Elaborated := False;
      Runtime_Finalize;
      s_stalib_adafinal;
   end adafinal;

   type No_Param_Proc is access procedure;

   procedure adainit is
      Main_Priority : Integer;
      pragma Import (C, Main_Priority, "__gl_main_priority");
      Time_Slice_Value : Integer;
      pragma Import (C, Time_Slice_Value, "__gl_time_slice_val");
      WC_Encoding : Character;
      pragma Import (C, WC_Encoding, "__gl_wc_encoding");
      Locking_Policy : Character;
      pragma Import (C, Locking_Policy, "__gl_locking_policy");
      Queuing_Policy : Character;
      pragma Import (C, Queuing_Policy, "__gl_queuing_policy");
      Task_Dispatching_Policy : Character;
      pragma Import (C, Task_Dispatching_Policy, "__gl_task_dispatching_policy");
      Priority_Specific_Dispatching : System.Address;
      pragma Import (C, Priority_Specific_Dispatching, "__gl_priority_specific_dispatching");
      Num_Specific_Dispatching : Integer;
      pragma Import (C, Num_Specific_Dispatching, "__gl_num_specific_dispatching");
      Main_CPU : Integer;
      pragma Import (C, Main_CPU, "__gl_main_cpu");
      Interrupt_States : System.Address;
      pragma Import (C, Interrupt_States, "__gl_interrupt_states");
      Num_Interrupt_States : Integer;
      pragma Import (C, Num_Interrupt_States, "__gl_num_interrupt_states");
      Unreserve_All_Interrupts : Integer;
      pragma Import (C, Unreserve_All_Interrupts, "__gl_unreserve_all_interrupts");
      Detect_Blocking : Integer;
      pragma Import (C, Detect_Blocking, "__gl_detect_blocking");
      Default_Stack_Size : Integer;
      pragma Import (C, Default_Stack_Size, "__gl_default_stack_size");
      Default_Secondary_Stack_Size : System.Parameters.Size_Type;
      pragma Import (C, Default_Secondary_Stack_Size, "__gnat_default_ss_size");
      Leap_Seconds_Support : Integer;
      pragma Import (C, Leap_Seconds_Support, "__gl_leap_seconds_support");
      Bind_Env_Addr : System.Address;
      pragma Import (C, Bind_Env_Addr, "__gl_bind_env_addr");

      procedure Runtime_Initialize (Install_Handler : Integer);
      pragma Import (C, Runtime_Initialize, "__gnat_runtime_initialize");

      Finalize_Library_Objects : No_Param_Proc;
      pragma Import (C, Finalize_Library_Objects, "__gnat_finalize_library_objects");
      Binder_Sec_Stacks_Count : Natural;
      pragma Import (Ada, Binder_Sec_Stacks_Count, "__gnat_binder_ss_count");
      Default_Sized_SS_Pool : System.Address;
      pragma Import (Ada, Default_Sized_SS_Pool, "__gnat_default_ss_pool");

   begin
      if Is_Elaborated then
         return;
      end if;
      Is_Elaborated := True;
      Main_Priority := -1;
      Time_Slice_Value := -1;
      WC_Encoding := 'b';
      Locking_Policy := ' ';
      Queuing_Policy := ' ';
      Task_Dispatching_Policy := ' ';
      System.Restrictions.Run_Time_Restrictions :=
        (Set =>
          (False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False, False, False, True, False, 
           False, False, False, False, False, True, False, False, 
           False, False, False, False, False, False, False, False, 
           False, False, False, False),
         Value => (0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
         Violated =>
          (False, False, False, False, True, True, True, False, 
           True, False, False, True, True, True, True, False, 
           False, False, False, False, True, True, False, True, 
           True, False, True, True, True, True, False, True, 
           False, False, False, True, False, False, True, False, 
           True, False, True, True, False, False, False, True, 
           True, False, True, True, False, True, True, False, 
           False, False, False, True, False, True, True, True, 
           False, False, True, False, True, True, True, False, 
           True, True, False, True, True, True, True, False, 
           False, True, False, False, False, False, True, True, 
           True, False, True, False),
         Count => (0, 0, 0, 0, 2, 4, 1, 0, 4, 0),
         Unknown => (False, False, False, False, False, False, True, False, True, False));
      Priority_Specific_Dispatching :=
        Local_Priority_Specific_Dispatching'Address;
      Num_Specific_Dispatching := 0;
      Main_CPU := -1;
      Interrupt_States := Local_Interrupt_States'Address;
      Num_Interrupt_States := 0;
      Unreserve_All_Interrupts := 0;
      Detect_Blocking := 0;
      Default_Stack_Size := -1;
      Leap_Seconds_Support := 0;

      ada_main'Elab_Body;
      Default_Secondary_Stack_Size := System.Parameters.Runtime_Default_Sec_Stack_Size;
      Binder_Sec_Stacks_Count := 1;
      Default_Sized_SS_Pool := Sec_Default_Sized_Stacks'Address;

      Runtime_Initialize (1);

      Finalize_Library_Objects := finalize_library'access;

      System.Soft_Links'Elab_Spec;
      System.Exception_Table'Elab_Body;
      E023 := E023 + 1;
      Ada.Io_Exceptions'Elab_Spec;
      E066 := E066 + 1;
      Ada.Strings'Elab_Spec;
      E050 := E050 + 1;
      Ada.Containers'Elab_Spec;
      E038 := E038 + 1;
      System.Exceptions'Elab_Spec;
      E025 := E025 + 1;
      Interfaces.C'Elab_Spec;
      System.Os_Lib'Elab_Body;
      E070 := E070 + 1;
      Ada.Strings.Maps'Elab_Spec;
      Ada.Strings.Maps.Constants'Elab_Spec;
      E056 := E056 + 1;
      System.Soft_Links.Initialize'Elab_Body;
      E019 := E019 + 1;
      E011 := E011 + 1;
      System.Object_Reader'Elab_Spec;
      System.Dwarf_Lines'Elab_Spec;
      E045 := E045 + 1;
      E076 := E076 + 1;
      E052 := E052 + 1;
      System.Traceback.Symbolic'Elab_Body;
      E037 := E037 + 1;
      E078 := E078 + 1;
      Ada.Numerics'Elab_Spec;
      E096 := E096 + 1;
      Ada.Tags'Elab_Spec;
      Ada.Tags'Elab_Body;
      E145 := E145 + 1;
      Ada.Streams'Elab_Spec;
      E162 := E162 + 1;
      Interfaces.C.Strings'Elab_Spec;
      E119 := E119 + 1;
      System.File_Control_Block'Elab_Spec;
      E179 := E179 + 1;
      System.Finalization_Root'Elab_Spec;
      E164 := E164 + 1;
      Ada.Finalization'Elab_Spec;
      E160 := E160 + 1;
      System.File_Io'Elab_Body;
      E178 := E178 + 1;
      Ada.Streams.Stream_Io'Elab_Spec;
      E474 := E474 + 1;
      System.Storage_Pools'Elab_Spec;
      E166 := E166 + 1;
      System.Finalization_Masters'Elab_Spec;
      System.Finalization_Masters'Elab_Body;
      E157 := E157 + 1;
      System.Storage_Pools.Subpools'Elab_Spec;
      E155 := E155 + 1;
      Ada.Strings.Unbounded'Elab_Spec;
      E141 := E141 + 1;
      System.Task_Info'Elab_Spec;
      E129 := E129 + 1;
      Ada.Calendar'Elab_Spec;
      Ada.Calendar'Elab_Body;
      E104 := E104 + 1;
      Ada.Real_Time'Elab_Spec;
      Ada.Real_Time'Elab_Body;
      E113 := E113 + 1;
      Ada.Text_Io'Elab_Spec;
      Ada.Text_Io'Elab_Body;
      E174 := E174 + 1;
      System.Assertions'Elab_Spec;
      E458 := E458 + 1;
      System.Pool_Global'Elab_Spec;
      E189 := E189 + 1;
      System.Random_Seed'Elab_Body;
      E102 := E102 + 1;
      System.Tasking.Initialization'Elab_Body;
      E499 := E499 + 1;
      System.Tasking.Protected_Objects'Elab_Body;
      E483 := E483 + 1;
      System.Tasking.Protected_Objects.Entries'Elab_Spec;
      E503 := E503 + 1;
      System.Tasking.Queuing'Elab_Body;
      E507 := E507 + 1;
      System.Tasking.Stages'Elab_Body;
      E511 := E511 + 1;
      Glib'Elab_Spec;
      Gtkada.Types'Elab_Spec;
      E184 := E184 + 1;
      E187 := E187 + 1;
      E405 := E405 + 1;
      Gdk.Frame_Timings'Elab_Spec;
      Gdk.Frame_Timings'Elab_Body;
      E257 := E257 + 1;
      E209 := E209 + 1;
      Gdk.Visual'Elab_Body;
      E239 := E239 + 1;
      E211 := E211 + 1;
      E203 := E203 + 1;
      Glib.Object'Elab_Spec;
      Glib.Values'Elab_Body;
      E207 := E207 + 1;
      E195 := E195 + 1;
      E205 := E205 + 1;
      E197 := E197 + 1;
      Glib.Object'Elab_Body;
      E193 := E193 + 1;
      E217 := E217 + 1;
      E219 := E219 + 1;
      E226 := E226 + 1;
      Glib.Generic_Properties'Elab_Spec;
      Glib.Generic_Properties'Elab_Body;
      E224 := E224 + 1;
      Gdk.Color'Elab_Spec;
      E249 := E249 + 1;
      E229 := E229 + 1;
      E222 := E222 + 1;
      E360 := E360 + 1;
      E241 := E241 + 1;
      E322 := E322 + 1;
      Glib.Variant'Elab_Spec;
      Glib.Variant'Elab_Body;
      E320 := E320 + 1;
      E318 := E318 + 1;
      Gtk.Actionable'Elab_Spec;
      E382 := E382 + 1;
      Gtk.Builder'Elab_Spec;
      Gtk.Builder'Elab_Body;
      E265 := E265 + 1;
      E302 := E302 + 1;
      Gtk.Cell_Area_Context'Elab_Spec;
      Gtk.Cell_Area_Context'Elab_Body;
      E334 := E334 + 1;
      Gtk.Css_Section'Elab_Spec;
      Gtk.Css_Section'Elab_Body;
      E350 := E350 + 1;
      E243 := E243 + 1;
      Gtk.Orientable'Elab_Spec;
      E308 := E308 + 1;
      Gtk.Paper_Size'Elab_Spec;
      Gtk.Paper_Size'Elab_Body;
      E362 := E362 + 1;
      Gtk.Page_Setup'Elab_Spec;
      Gtk.Page_Setup'Elab_Body;
      E358 := E358 + 1;
      Gtk.Print_Settings'Elab_Spec;
      Gtk.Print_Settings'Elab_Body;
      E370 := E370 + 1;
      E273 := E273 + 1;
      Gtk.Target_List'Elab_Spec;
      Gtk.Target_List'Elab_Body;
      E271 := E271 + 1;
      Gtk.Text_Mark'Elab_Spec;
      Gtk.Text_Mark'Elab_Body;
      E450 := E450 + 1;
      E278 := E278 + 1;
      Pango.Attributes'Elab_Spec;
      Pango.Attributes'Elab_Body;
      E296 := E296 + 1;
      Pango.Font_Metrics'Elab_Spec;
      Pango.Font_Metrics'Elab_Body;
      E282 := E282 + 1;
      Pango.Language'Elab_Spec;
      Pango.Language'Elab_Body;
      E284 := E284 + 1;
      Pango.Font'Elab_Spec;
      Pango.Font'Elab_Body;
      E280 := E280 + 1;
      E376 := E376 + 1;
      Gtk.Text_Tag'Elab_Spec;
      Gtk.Text_Tag'Elab_Body;
      E378 := E378 + 1;
      Pango.Font_Face'Elab_Spec;
      Pango.Font_Face'Elab_Body;
      E288 := E288 + 1;
      Pango.Font_Family'Elab_Spec;
      Pango.Font_Family'Elab_Body;
      E286 := E286 + 1;
      Pango.Fontset'Elab_Spec;
      Pango.Fontset'Elab_Body;
      E290 := E290 + 1;
      E292 := E292 + 1;
      Pango.Context'Elab_Spec;
      Pango.Context'Elab_Body;
      E276 := E276 + 1;
      Pango.Font_Map'Elab_Spec;
      Pango.Font_Map'Elab_Body;
      E366 := E366 + 1;
      Pango.Tabs'Elab_Spec;
      Pango.Tabs'Elab_Body;
      E298 := E298 + 1;
      Pango.Layout'Elab_Spec;
      Pango.Layout'Elab_Body;
      E294 := E294 + 1;
      Gtk.Print_Context'Elab_Spec;
      Gtk.Print_Context'Elab_Body;
      E364 := E364 + 1;
      Gdk.Display'Elab_Spec;
      Gtk.Tree_Model'Elab_Spec;
      Gtk.Entry_Buffer'Elab_Spec;
      Gtk.Cell_Editable'Elab_Spec;
      Gtk.Adjustment'Elab_Spec;
      Gtk.Style'Elab_Spec;
      Gtk.Accel_Group'Elab_Spec;
      Gdk.Frame_Clock'Elab_Spec;
      Gdk.Pixbuf'Elab_Spec;
      Gtk.Icon_Source'Elab_Spec;
      Gtk.Icon_Source'Elab_Body;
      E346 := E346 + 1;
      E259 := E259 + 1;
      Gdk.Screen'Elab_Spec;
      Gdk.Screen'Elab_Body;
      E235 := E235 + 1;
      E374 := E374 + 1;
      Gtk.Selection_Data'Elab_Spec;
      Gtk.Selection_Data'Elab_Body;
      E267 := E267 + 1;
      Gdk.Device'Elab_Spec;
      Gdk.Drag_Contexts'Elab_Spec;
      Gdk.Drag_Contexts'Elab_Body;
      E253 := E253 + 1;
      Gdk.Device'Elab_Body;
      E251 := E251 + 1;
      Gtk.Widget'Elab_Spec;
      Gtk.Misc'Elab_Spec;
      Gtk.Misc'Elab_Body;
      E352 := E352 + 1;
      E245 := E245 + 1;
      Gtk.Settings'Elab_Spec;
      Gtk.Settings'Elab_Body;
      E233 := E233 + 1;
      Gdk.Window'Elab_Spec;
      E312 := E312 + 1;
      Gtk.Style_Context'Elab_Spec;
      Gtk.Icon_Set'Elab_Spec;
      Gtk.Icon_Set'Elab_Body;
      E344 := E344 + 1;
      Gtk.Image'Elab_Spec;
      Gtk.Image'Elab_Body;
      E342 := E342 + 1;
      Gtk.Cell_Renderer'Elab_Spec;
      Gtk.Container'Elab_Spec;
      Gtk.Bin'Elab_Spec;
      Gtk.Bin'Elab_Body;
      E314 := E314 + 1;
      Gtk.Box'Elab_Spec;
      Gtk.Box'Elab_Body;
      E300 := E300 + 1;
      Gtk.Status_Bar'Elab_Spec;
      Gtk.Notebook'Elab_Spec;
      E336 := E336 + 1;
      Gtk.Cell_Area'Elab_Spec;
      Gtk.Entry_Completion'Elab_Spec;
      Gtk.Window'Elab_Spec;
      Gtk.Dialog'Elab_Spec;
      Gtk.Print_Operation'Elab_Spec;
      Gtk.Gentry'Elab_Spec;
      Gtk.Status_Bar'Elab_Body;
      E372 := E372 + 1;
      E368 := E368 + 1;
      Gtk.Print_Operation'Elab_Body;
      E356 := E356 + 1;
      Gtk.Notebook'Elab_Body;
      E354 := E354 + 1;
      Gtk.Style_Context'Elab_Body;
      E348 := E348 + 1;
      Gtk.Gentry'Elab_Body;
      E316 := E316 + 1;
      Gtk.Tree_Model'Elab_Body;
      E340 := E340 + 1;
      Gtk.Cell_Area'Elab_Body;
      E332 := E332 + 1;
      Gtk.Entry_Completion'Elab_Body;
      E330 := E330 + 1;
      Gtk.Cell_Renderer'Elab_Body;
      E338 := E338 + 1;
      Gtk.Entry_Buffer'Elab_Body;
      E328 := E328 + 1;
      E326 := E326 + 1;
      E324 := E324 + 1;
      Gtk.Window'Elab_Body;
      E310 := E310 + 1;
      Gtk.Dialog'Elab_Body;
      E231 := E231 + 1;
      Gtk.Adjustment'Elab_Body;
      E306 := E306 + 1;
      Gtk.Container'Elab_Body;
      E304 := E304 + 1;
      Gtk.Style'Elab_Body;
      E269 := E269 + 1;
      Gtk.Widget'Elab_Body;
      E247 := E247 + 1;
      Gtk.Accel_Group'Elab_Body;
      E263 := E263 + 1;
      Gdk.Frame_Clock'Elab_Body;
      E255 := E255 + 1;
      Gdk.Display'Elab_Body;
      E237 := E237 + 1;
      E215 := E215 + 1;
      Glib.Menu_Model'Elab_Spec;
      Glib.Menu_Model'Elab_Body;
      E430 := E430 + 1;
      Gtk.Action'Elab_Spec;
      Gtk.Action'Elab_Body;
      E380 := E380 + 1;
      Gtk.Activatable'Elab_Spec;
      E384 := E384 + 1;
      Gtk.Button'Elab_Spec;
      Gtk.Button'Elab_Body;
      E213 := E213 + 1;
      Gtk.Clipboard'Elab_Spec;
      Gtk.Clipboard'Elab_Body;
      E446 := E446 + 1;
      Gtk.Grange'Elab_Spec;
      Gtk.Grange'Elab_Body;
      E438 := E438 + 1;
      E386 := E386 + 1;
      E460 := E460 + 1;
      Gtk.Menu_Item'Elab_Spec;
      Gtk.Menu_Item'Elab_Body;
      E432 := E432 + 1;
      Gtk.Menu_Shell'Elab_Spec;
      Gtk.Menu_Shell'Elab_Body;
      E434 := E434 + 1;
      Gtk.Menu'Elab_Spec;
      Gtk.Menu'Elab_Body;
      E428 := E428 + 1;
      Gtk.Label'Elab_Spec;
      Gtk.Label'Elab_Body;
      E426 := E426 + 1;
      Gtk.Scale'Elab_Spec;
      Gtk.Scale'Elab_Body;
      E436 := E436 + 1;
      Gtk.Scrollable'Elab_Spec;
      E442 := E442 + 1;
      Gtk.Switch'Elab_Spec;
      Gtk.Switch'Elab_Body;
      E388 := E388 + 1;
      E181 := E181 + 1;
      Gtk.Text_Child_Anchor'Elab_Spec;
      Gtk.Text_Child_Anchor'Elab_Body;
      E448 := E448 + 1;
      Gtk.Text_Tag_Table'Elab_Spec;
      Gtk.Text_Tag_Table'Elab_Body;
      E452 := E452 + 1;
      Gtk.Text_Buffer'Elab_Spec;
      Gtk.Text_Buffer'Elab_Body;
      E444 := E444 + 1;
      Gtk.Text_View'Elab_Spec;
      Gtk.Text_View'Elab_Body;
      E440 := E440 + 1;
      Gtk.Tree_View_Column'Elab_Spec;
      Gtk.Tree_View_Column'Elab_Body;
      E462 := E462 + 1;
      Gtkada.Handlers'Elab_Spec;
      E463 := E463 + 1;
      Gtkada.Builder'Elab_Spec;
      Gtkada.Builder'Elab_Body;
      E454 := E454 + 1;
      logger'elab_spec;
      E481 := E481 + 1;
      E479 := E479 + 1;
      E391 := E391 + 1;
      actor_package'elab_spec;
      actor_package'elab_body;
      E515 := E515 + 1;
      controller_package'elab_spec;
      controller_package'elab_body;
      E513 := E513 + 1;
      pump_package'elab_spec;
      pump_package'elab_body;
      E526 := E526 + 1;
      sensor_core_package'elab_spec;
      sensor_core_package'elab_body;
      E522 := E522 + 1;
      interrupt_sensor_package'elab_spec;
      interrupt_sensor_package'elab_body;
      E520 := E520 + 1;
      polling_sensor_package'elab_spec;
      polling_sensor_package'elab_body;
      E524 := E524 + 1;
      water'elab_spec;
      E528 := E528 + 1;
      water_flow_sensor_package'elab_spec;
      water_flow_sensor_package'elab_body;
      E530 := E530 + 1;
      processes'elab_spec;
      processes'elab_body;
      E489 := E489 + 1;
   end adainit;

   procedure Ada_Main_Program;
   pragma Import (Ada, Ada_Main_Program, "_ada_prv");

   function main
     (argc : Integer;
      argv : System.Address;
      envp : System.Address)
      return Integer
   is
      procedure Initialize (Addr : System.Address);
      pragma Import (C, Initialize, "__gnat_initialize");

      procedure Finalize;
      pragma Import (C, Finalize, "__gnat_finalize");
      SEH : aliased array (1 .. 2) of Integer;

      Ensure_Reference : aliased System.Address := Ada_Main_Program_Name'Address;
      pragma Volatile (Ensure_Reference);

   begin
      gnat_argc := argc;
      gnat_argv := argv;
      gnat_envp := envp;

      Initialize (SEH'Address);
      adainit;
      Ada_Main_Program;
      adafinal;
      Finalize;
      return (gnat_exit_status);
   end;

--  BEGIN Object file/option list
   --   C:\PRV_Testing\obj\formatter.o
   --   C:\PRV_Testing\obj\builder_cb.o
   --   C:\PRV_Testing\obj\logger.o
   --   C:\PRV_Testing\obj\parameters.o
   --   C:\PRV_Testing\obj\guisensor.o
   --   C:\PRV_Testing\obj\guidata.o
   --   C:\PRV_Testing\obj\types.o
   --   C:\PRV_Testing\obj\actor_package.o
   --   C:\PRV_Testing\obj\controller_package.o
   --   C:\PRV_Testing\obj\pump_package.o
   --   C:\PRV_Testing\obj\sensor_core_package.o
   --   C:\PRV_Testing\obj\interrupt_sensor_package.o
   --   C:\PRV_Testing\obj\polling_sensor_package.o
   --   C:\PRV_Testing\obj\water.o
   --   C:\PRV_Testing\obj\water_flow_sensor_package.o
   --   C:\PRV_Testing\obj\processes.o
   --   C:\PRV_Testing\obj\prv.o
   --   -LC:\PRV_Testing\obj\
   --   -LC:\PRV_Testing\obj\
   --   -LC:\GtkAda\lib\gtkada\gtkada.static\gtkada\
   --   -LC:/gnat/2018/lib/gcc/x86_64-pc-mingw32/7.3.1/adalib/
   --   -static
   --   -shared-libgcc
   --   -shared-libgcc
   --   -lgthread-2.0
   --   -shared-libgcc
   --   -lgnarl
   --   -lgnat
   --   -Xlinker
   --   --stack=0x200000,0x1000
   --   -mthreads
   --   -Wl,--stack=0x2000000
--  END Object file/option list   

end ada_main;
